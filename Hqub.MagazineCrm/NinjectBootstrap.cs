﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ninject;
using Ninject.Modules;

namespace Hqub.MagazineCrm
{
    internal class NinjectBootstrap : NinjectModule
    {
        private static IKernel _self;

        public static IKernel Kernel
        {
            get
            {
                _self = _self ?? new StandardKernel(new NinjectBootstrap());
                return _self;
            }
        }

        public override void Load()
        {
            Bind<Service.Interface.ICustomerService>().To<Service.Implementation.CustomerService>();
            Bind<Service.Interface.IOrderService>().To<Service.Implementation.OrderService>();
            Bind<Service.Interface.IJournalService>().To<Service.Implementation.JournalService>();
            Bind<Service.Interface.IPaymentService>().To<Service.Implementation.PaymentService>();
            Bind<Service.Interface.IDatabaseService>().To<Service.Implementation.DatabaseService>();
            Bind<Service.Interface.IHistoryService>().To<Service.Implementation.HistoryService>();
            Bind<Service.Interface.IBillService>().To<Service.Implementation.BillService>();
        }

        private static Service.Interface.ICustomerService _customerService;
        public static Service.Interface.ICustomerService GetCustomerService(bool reinstance=false, bool saveInstance = true)
        {
            if (reinstance)
                if (saveInstance)
                    return _customerService = Kernel.Get<Service.Interface.ICustomerService>();

            return _customerService = _customerService ?? Kernel.Get<Service.Interface.ICustomerService>();
        }

        private static Service.Interface.IOrderService _orderService;
        public static Service.Interface.IOrderService GetOrderService(bool reinstance = false, bool saveInstance = true)
        {
            if (reinstance)
                if (saveInstance)
                    return _orderService = Kernel.Get<Service.Interface.IOrderService>();
                else
                    return Kernel.Get<Service.Interface.IOrderService>();
            

            return _orderService = _orderService ?? Kernel.Get<Service.Interface.IOrderService>();
        }

        private static Service.Interface.IJournalService _journalService;
        public static Service.Interface.IJournalService GetJournalService(bool reinstance=false, bool saveInstance=true)
        {
            if (reinstance)
                if (saveInstance)
                    return _journalService = Kernel.Get<Service.Interface.IJournalService>();
                else
                    return Kernel.Get<Service.Interface.IJournalService>();


            return _journalService = _journalService ?? Kernel.Get<Service.Interface.IJournalService>();
        }

        private static Service.Interface.IPaymentService _paymentService;
        public static Service.Interface.IPaymentService GetPaymentService(bool reinstance=false, bool saveInstance=true)
        {
            if (reinstance)
                if (saveInstance)
                    return _paymentService = Kernel.Get<Service.Interface.IPaymentService>();
                else
                    return Kernel.Get<Service.Interface.IPaymentService>();


            return _paymentService = _paymentService ?? Kernel.Get<Service.Interface.IPaymentService>();
        }

        public static Service.Interface.IDatabaseService GetDatabaseService()
        {
            return Kernel.Get<Service.Interface.IDatabaseService>();
        }

        public static Service.Interface.IHistoryService GetHistoryService()
        {
            return Kernel.Get<Service.Interface.IHistoryService>();
        }

        public static Service.Interface.IBillService GetBillService()
        {
            return Kernel.Get<Service.Interface.IBillService>();
        }
    }
}
