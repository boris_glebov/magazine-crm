﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hqub.MagazineCrm.Core.Import
{
    public enum CustomerColumns
    {
        Num = 1,
        Kunde, //Name
        Adresse, 
        PlzOrt, //ZIP Code,
        Telefon, //Phone
        Fax,
        Handy,
        Ansprechpartner,
        Bemerkungen,
        Email,
        Internetseite
    }
}
