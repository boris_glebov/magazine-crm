﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hqub.MagazineCrm.Core.Import
{
    public enum OrderColumns
    {
        Num = 1,
        KundeNum,
        DatumRechnung,
        DatumZahlung,
        Vorzahlung,
        Beschreibung,
        EinnahmeNetto,
        EinnahmeBrutto,
        Kunde,
        VersandArt,
        AbWerbungMonat,
        AbJahr,
        BisWerbungMonat,
        BisJahr,
        Bemerkungen,
        EMail,
        Anschrift,
        Plz,
        SpezieleBemerkungen,
        Monat,
        Storno
    }
}
