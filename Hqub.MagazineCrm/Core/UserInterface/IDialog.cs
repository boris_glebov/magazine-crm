﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hqub.MagazineCrm.Core.UserInterface
{
    public interface IDialog
    {
        Action Close { get; set; }
    }
}
