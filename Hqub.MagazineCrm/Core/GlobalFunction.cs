﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hqub.MagazineCrm.Core
{
    public static class GlobalFunction
    {
        public static Action<bool> SetBusyIndicator { get; set; }

        public delegate void FilterHandle(string text, bool sensivity);

        public static event FilterHandle OnFiltering;
        public static void InvokeFiltering(string text, bool sensivity)
        {
            if (OnFiltering != null)
                OnFiltering(text, sensivity);
        }
    }
}

