﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hqub.MagazineCrm.Core
{
    public class JournalManager
    {
        public class JournalInfo
        {
            public JournalInfo(int month, int year)
            {
                Month = month;
                Year = year;

                Duplicate = 0;
            }

            public int Month { get; set; }

            public int Year { get; set; }

            public int Duplicate { get; set; }
        }


        private JournalManager()
        {
            
        }

        private static JournalManager _instance;
        public static JournalManager Instance
        {
            get { return _instance = _instance ?? new JournalManager(); }
        }

        private JournalInfo _registerJournal;

        /// <summary>
        /// Резервируем месяц журнала. 
        /// </summary>
        /// <param name="month"></param>
        /// <param name="year"></param>
        public void RegisterJournal(int month, int year, bool isDuplicate = false)
        {
            if (!isDuplicate)
            {
                _registerJournal = new JournalInfo(month, year);
                return;
            }

            ++_registerJournal.Duplicate;
        }

        public void CloseJournal(int month, int year)
        {
            _registerJournal = null;
        }

        /// <summary>
        /// Проверяем существует ли журнал с таким же месяцем
        /// </summary>
        /// <param name="month"></param>
        /// <param name="year"></param>
        public bool ExistsJournal(int month, int year)
        {
            var journalService = NinjectBootstrap.GetJournalService();

            var date = journalService.GetLastJournalDate();

            if (date == null)
                return false;

            return date.Value.Month == month && date.Value.Year == year;
        }

        /// <summary>
        /// Проверяем создан ли в текущем сеансе журнал с тем же годом и месяцем
        /// </summary>
        public bool CheckJournal(int month, int  year)
        {
            if(_registerJournal == null)
                return false;

            return month == _registerJournal.Month && year == _registerJournal.Year;
        }
    }
}
