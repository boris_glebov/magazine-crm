﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Data.Linq;

namespace Hqub.MagazineCrm.Core
{
    public static class GuiManager
    {
        #region Manage TabControl GUI Elem

        private static Dictionary<string, TabControl> TabControlCollection = new Dictionary<string, TabControl>();

        /// <summary>
        /// Register new tab control
        /// </summary>
        public static bool RegisterTabControl(string tabName, TabControl tabControl)
        {
            var uniqueTabControl = string.Format("{0}_{1}", tabName, tabControl.GetHashCode());
            if (TabControlCollection.ContainsKey(uniqueTabControl))
                return false;

            TabControlCollection.Add(uniqueTabControl, tabControl);

            return true;
        }

        /// <summary>
        /// Add TabItem in TabControl with name = tabName
        /// </summary>
        public static void AddPage(string tabName, string pageName, string title, UIElement element)
        {
            var tabControl = TabControlCollection[tabName];
            if (tabControl == null)
                throw new ArgumentException(string.Format("TabControl with the name '{0}' not found", tabName),
                                            "tabName");

            var uniqPageName = string.Format("{0}_{1}", pageName, element.GetHashCode());
            var tab = tabControl.Items.Cast<TabItem>().FirstOrDefault(x => x.Name == uniqPageName);
            if (tab != null)
            {
                tabControl.SelectedItem = tab;
                return;
            }

            tab = new TabItem {Header = title, Content = element, Name = uniqPageName};
            tabControl.Items.Add(tab);
            tabControl.SelectedItem = tab;
        }

        /// <summary>
        /// Remove TabItem from TabControl with name = tabName
        /// </summary>
        /// <param name="tabName"></param>
        /// <param name="pageName"></param>
        public static void RemovePage(string tabName)
        {
            var tabControl = TabControlCollection[tabName];
            if (tabControl == null)
                throw new ArgumentException(string.Format("TabControl with the name '{0}' not found", tabName),
                                            "tabName");

            var page = tabControl.Items.Cast<TabItem>().Where(tab => tab.Name == tabName).FirstOrDefault();
            if (page == null)
                return;

            tabControl.Items.Remove(page);
        }

        #endregion

        #region Managing the main TabControl.

        private static TabControl MainContent { get; set; }

        public static void RegisterMainContent(TabControl mainContent)
        {
            MainContent = mainContent;
        }

        public static bool IsRegisterMainContent()
        {
            return MainContent != null;
        }

        public static bool IsEmpty()
        {
            if (!IsRegisterMainContent())
                return true;

            return MainContent.Items.Count == 0;
        }

        public static void AddMainContent(UIElement element, string title)
        {
            if (!IsRegisterMainContent())
                throw new NullReferenceException("You mast call method RegisterMainContent");

            var t = new TabItem {Content = element, Header = title};
            MainContent.Items.Add(t);

            //Select new tab:
            MainContent.SelectedItem = t;
        }

        public static void RemoveActiveTab(string tabName = "")
        {
            try
            {
                if (!string.IsNullOrEmpty(tabName))
                    TabControlCollection.Remove(tabName);

                try
                {
                    dynamic obj = MainContent.SelectedItem;
                    //Execute custom close method:
                    obj.Content.Release();

                }
                finally
                {
                    //Remove GUI Tab:
                    MainContent.Items.Remove(MainContent.SelectedItem);
                    MainContent.Items.MoveCurrentToLast();
                }
            }
            catch (Exception ex)
            {
                var logger = NLog.LogManager.GetCurrentClassLogger();
                logger.ErrorException(ex.Message, ex);
            }
        }

        public static void CloseAllTab()
        {
            if (!IsRegisterMainContent())
                return;

            while (MainContent.Items.MoveCurrentToLast())
            {
                MainContent.Items.Remove(MainContent.SelectedItem);
            }
        }

        #endregion
    }
}
