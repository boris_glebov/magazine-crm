﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace Hqub.MagazineCrm.Core
{
    public static class SerializeHelper
    {
        public static MemoryStream Serialize(object entity)
        {
            var formatter = new BinaryFormatter();
            var stream = new MemoryStream();

            // Serialize the customer object graph.
            formatter.Serialize(stream, entity);

            return stream;
        }

        public static object Deserialize(MemoryStream entity)
        {
            var formatter = new BinaryFormatter();

            // Read from the begining of the stream.
            entity.Seek(0, SeekOrigin.Begin);

            return formatter.Deserialize(entity);
        }
    }
}
