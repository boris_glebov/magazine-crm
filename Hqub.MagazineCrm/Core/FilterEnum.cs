﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hqub.MagazineCrm.Core
{
    public enum FilterEnum
    {
        None,
        NotInvoice,
        NotPaid
    }
}
