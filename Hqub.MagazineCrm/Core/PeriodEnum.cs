﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hqub.MagazineCrm.Core
{
    public enum PeriodEnum
    {
        EveryMonth = 101, // Каждый месяц
        EveryTwoMonths = 102, // Каждые 2 месяца
        EveryThreeMonths = 103, // Каждые 3 месяца
        EverySixMonth = 106, // Каждые 6 месяцев
        OneHalfYear = 201, //Раз в полгода
        OneYear = 202, //Раз в год
    }
}
