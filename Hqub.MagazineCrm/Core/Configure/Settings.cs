﻿using System;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using Hqub.MagazineCrm.Model;

namespace Hqub.MagazineCrm.Core.Configure
{
    public static class Settings
    {
        private static SettingsEntity _settingsEntity;

        public static void ConfigSave()
        {
            var configPath = AppDomain.CurrentDomain.BaseDirectory + "config.xml";
            if(!File.Exists(configPath))
                CreateDefaultConfig(configPath);

            var serializer = new XmlSerializer(typeof(SettingsEntity));
            TextWriter fileStream = new StreamWriter(configPath);
            serializer.Serialize(fileStream, _settingsEntity);
        }

        private static void CreateDefaultConfig(string path)
        {
            var content = new StringBuilder();
            content.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
            content.AppendLine("<config>");
            content.AppendLine("\t" + "<database name=\"magazine-db.db3\"/>");
            content.AppendLine("\t" + "<language>en</language>");
            content.AppendLine("</config>");

            File.WriteAllText(path, content.ToString());
        }

        public static SettingsEntity Instance
        {
            get
            {
                if (_settingsEntity == null)
                {
                    var configPath = AppDomain.CurrentDomain.BaseDirectory + "config.xml";
                    if (!File.Exists(configPath))
                        CreateDefaultConfig(configPath);

                    var serializer = new XmlSerializer(typeof(SettingsEntity));
                    var readStream = File.OpenRead(configPath);
                    _settingsEntity = (SettingsEntity)serializer.Deserialize(readStream);
                    readStream.Close();
                }

                return _settingsEntity;
            }
        }

        public static SettingsEntity.Database DatabaseSettings
        {
            get { return Instance.DatabaseEntity; }
        }
    }
}
