﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace Hqub.MagazineCrm.Core
{
    public static class Utils
    {
        public static byte[] StreamFile(string filename)
        {
            var fs = new FileStream(filename, FileMode.Open, FileAccess.Read);

            // Create a byte array of file stream length
            var fileData = new byte[fs.Length];

            //Read block of bytes from stream into the byte array
            fs.Read(fileData, 0, Convert.ToInt32(fs.Length));

            //Close the File Stream
            fs.Close();
            return fileData; //return the byte data
        }

        public static decimal NumberCultureParse(string num)
        {
            decimal f;
            decimal result = 0;
            if (decimal.TryParse(num, NumberStyles.Float,
                    CultureInfo.InvariantCulture, out f))
                result = f;
            else if (decimal.TryParse(num, out f))
                result = f;

            return result;
        }

        public static string DecimalToString(decimal d)
        {
            var culture = (CultureInfo) CultureInfo.CurrentCulture.Clone();
            culture.NumberFormat.NumberDecimalSeparator =
                CultureInfo.CurrentCulture.NumberFormat.CurrencyGroupSeparator;
            return d.ToString("#.00", culture);
        }

        public static decimal GetDecimal(string value)
        {
            decimal number;
            CultureInfo culture = null;

            // Return if string is empty
            if (String.IsNullOrEmpty(value))
                throw new ArgumentNullException("The input string is invalid.");

            // Determine if value can be parsed using current culture.
            try
            {
                culture = CultureInfo.CurrentCulture;
                number = decimal.Parse(value.Replace(",", culture.NumberFormat.CurrencyDecimalSeparator), culture);
                return number;
            }
            catch { }
            // If Parse operation fails, see if there's a neutral culture.
            try
            {
                culture = culture.Parent;
                number = decimal.Parse(value, culture);
                return number;
            }
            catch { }
            // If there is no neutral culture or if parse operation fails, use
            // the invariant culture.
            culture = CultureInfo.InvariantCulture;
            try
            {
                number = decimal.Parse(value, culture);
                return number;
            }
            // All attempts to parse the string have failed; rethrow the exception.
            catch (FormatException e)
            {
                throw new FormatException(String.Format("Unable to parse '{0}'.", value),
                                          e);
            }
        }

        public static string GetStringDate(DateTime dateTime, string culture = "")
        {
            return dateTime.ToString("d",
                                     string.IsNullOrEmpty(culture)
                                         ? CultureInfo.CurrentCulture
                                         : new CultureInfo(culture));
        }
   
        public static void PdfPrint(string inPath, string outPath, string outFileName)
        {
            var scriptPath = AppDomain.CurrentDomain.BaseDirectory + "Scripts\\" + "Convert2PDF.vbs";

            var scriptProc = new Process();
            scriptProc.StartInfo.FileName = @"cscript";
            // 
            scriptProc.StartInfo.Arguments = string.Format("//B //Nologo \"{3}\" \"{0}\" \"{1}\"  \"{2}\" \"{4}\"",
                                                           inPath, outPath, outFileName, scriptPath,
                                                           Configure.Settings.Instance.PdfPrinter);
            scriptProc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden; //prevent console window from popping up
            scriptProc.StartInfo.RedirectStandardOutput = true;
            scriptProc.StartInfo.UseShellExecute = false;
            scriptProc.Start();

            scriptProc.WaitForExit();

            var output = scriptProc.StandardOutput.ReadToEnd();
            Debug.WriteLine(output);
            scriptProc.Close();
        }
    }
}
