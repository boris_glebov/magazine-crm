﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using Hqub.MagazineCrm.Core.Configure;

namespace Hqub.MagazineCrm.Database
{
    public class Engine
    {
        /*
         * Initialize database and tools
         */
        static Engine()
        {
            var dbPath = AppDomain.CurrentDomain.BaseDirectory + Settings.DatabaseSettings.Name;

            /*
             * Create database
             */
            if(!System.IO.File.Exists(dbPath))
            {
                try
                {
                    SQLiteConnection.CreateFile(dbPath);
                }
                catch (Exception exception)
                {
                    System.Diagnostics.Debug.WriteLine(string.Format("static Tools: {0}", exception.Message));
                }
            }
        }
    }
}
