
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 07/22/2012 13:40:40
-- Generated from EDMX file: C:\git\magazine-crm\Hqub.MagazineCrm\Database\ModelDatabase.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_CustomerOrders]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OrdersSet] DROP CONSTRAINT [FK_CustomerOrders];
GO
IF OBJECT_ID(N'[dbo].[FK_OrdersLayouts]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[LayoutsSet] DROP CONSTRAINT [FK_OrdersLayouts];
GO
IF OBJECT_ID(N'[dbo].[FK_CustomersJournalOrders]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JournalOrdersSet] DROP CONSTRAINT [FK_CustomersJournalOrders];
GO
IF OBJECT_ID(N'[dbo].[FK_JournalOrdersJournalLayouts]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JournalLayoutsSet] DROP CONSTRAINT [FK_JournalOrdersJournalLayouts];
GO
IF OBJECT_ID(N'[dbo].[FK_JournalsJournalOrders]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JournalOrdersSet] DROP CONSTRAINT [FK_JournalsJournalOrders];
GO
IF OBJECT_ID(N'[dbo].[FK_CustomersPayments]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PaymentsSet] DROP CONSTRAINT [FK_CustomersPayments];
GO
IF OBJECT_ID(N'[dbo].[FK_JournalsBills]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[BillsSet] DROP CONSTRAINT [FK_JournalsBills];
GO
IF OBJECT_ID(N'[dbo].[FK_BillsNotifications]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[NotificationsSet] DROP CONSTRAINT [FK_BillsNotifications];
GO
IF OBJECT_ID(N'[dbo].[FK_CustomersBills]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[BillsSet] DROP CONSTRAINT [FK_CustomersBills];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[CustomersSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CustomersSet];
GO
IF OBJECT_ID(N'[dbo].[OrdersSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[OrdersSet];
GO
IF OBJECT_ID(N'[dbo].[HistorySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[HistorySet];
GO
IF OBJECT_ID(N'[dbo].[LayoutsSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[LayoutsSet];
GO
IF OBJECT_ID(N'[dbo].[JournalsSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[JournalsSet];
GO
IF OBJECT_ID(N'[dbo].[JournalOrdersSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[JournalOrdersSet];
GO
IF OBJECT_ID(N'[dbo].[JournalLayoutsSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[JournalLayoutsSet];
GO
IF OBJECT_ID(N'[dbo].[PaymentsSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PaymentsSet];
GO
IF OBJECT_ID(N'[dbo].[BillsSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[BillsSet];
GO
IF OBJECT_ID(N'[dbo].[NotificationsSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[NotificationsSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'CustomersSet'
CREATE TABLE [dbo].[CustomersSet] (
    [Id] integer  NOT NULL,
    [Name] nvarchar(2147483647)  NOT NULL,
    [Zip] nvarchar(2147483647)  NULL,
    [Phone] nvarchar(2147483647)  NULL,
    [Cell] nvarchar(2147483647)  NULL,
    [Contact] nvarchar(2147483647)  NULL,
    [Note] nvarchar(2147483647)  NULL,
    [Email] nvarchar(2147483647)  NULL,
    [HomePage] nvarchar(2147483647)  NULL,
    [Balance] decimal(18,0)  NOT NULL,
    [Fax] nvarchar(2147483647)  NULL,
    [City] nvarchar(2147483647)  NULL,
    [Country] nvarchar(2147483647)  NULL,
    [FirstName] nvarchar(2147483647)  NULL,
    [LastName] nvarchar(2147483647)  NULL,
    [Salutation] int  NULL,
    [House] nvarchar(2147483647)  NULL,
    [Street] nvarchar(2147483647)  NULL,
    [PaymentMadeByDirectDebit] bit  NOT NULL
);
GO

-- Creating table 'OrdersSet'
CREATE TABLE [dbo].[OrdersSet] (
    [Id] integer  NOT NULL,
    [IsPrepay] bit  NOT NULL,
    [IsSetInvoce] bit  NOT NULL,
    [IsPayable] bit  NOT NULL,
    [Amount] decimal(18,0)  NOT NULL,
    [CustomerId] integer  NOT NULL,
    [Notes] nvarchar(2147483647)  NOT NULL,
    [Price] decimal(18,0)  NOT NULL,
    [Period] int  NOT NULL,
    [Size] nvarchar(2147483647)  NOT NULL,
    [Description] nvarchar(2147483647)  NULL,
    [Prepay] decimal(18,0)  NOT NULL,
    [PriceBrutto] decimal(18,0)  NOT NULL,
    [StartMonth] int  NOT NULL,
    [StartYear] int  NOT NULL,
    [EndMonth] int  NOT NULL,
    [EndYear] int  NOT NULL,
    [ExtraNotes] nvarchar(2147483647)  NULL,
    [IsAutomatic] bit  NULL
);
GO

-- Creating table 'HistorySet'
CREATE TABLE [dbo].[HistorySet] (
    [Id] integer  NOT NULL,
    [TypeName] nvarchar(2147483647)  NOT NULL,
    [Data] blob  NOT NULL,
    [EntityId] integer  NOT NULL,
    [DateStamp] datetime  NOT NULL
);
GO

-- Creating table 'LayoutsSet'
CREATE TABLE [dbo].[LayoutsSet] (
    [Id] integer  NOT NULL,
    [Data] blob  NOT NULL,
    [OrderId] integer  NOT NULL,
    [Title] nvarchar(2147483647)  NOT NULL,
    [Extension] nvarchar(2147483647)  NOT NULL
);
GO

-- Creating table 'JournalsSet'
CREATE TABLE [dbo].[JournalsSet] (
    [Id] integer  NOT NULL,
    [Title] nvarchar(2147483647)  NOT NULL,
    [PublishDate] datetime  NOT NULL,
    [IsFix] bit  NOT NULL
);
GO

-- Creating table 'JournalOrdersSet'
CREATE TABLE [dbo].[JournalOrdersSet] (
    [Id] integer  NOT NULL,
    [IsPrepay] bit  NOT NULL,
    [IsSetInvoce] bit  NOT NULL,
    [IsPayable] bit  NOT NULL,
    [Amount] decimal(18,0)  NOT NULL,
    [CustomerId] integer  NOT NULL,
    [Notes] nvarchar(2147483647)  NOT NULL,
    [Price] decimal(18,0)  NOT NULL,
    [Period] int  NOT NULL,
    [Size] nvarchar(2147483647)  NOT NULL,
    [JournalId] integer  NOT NULL,
    [RealOrderId] integer  NOT NULL,
    [Prepay] decimal(18,0)  NOT NULL,
    [Description] nvarchar(2147483647)  NULL,
    [PriceBrutto] decimal(18,0)  NOT NULL,
    [PublishDate] datetime  NULL,
    [PayableDate] datetime  NULL,
    [StartMonth] int  NOT NULL,
    [StartYear] int  NOT NULL,
    [EndMonth] int  NOT NULL,
    [EndYear] int  NOT NULL,
    [ExtraNotes] nvarchar(2147483647)  NULL,
    [InvoiceDate] datetime  NULL
);
GO

-- Creating table 'JournalLayoutsSet'
CREATE TABLE [dbo].[JournalLayoutsSet] (
    [Id] integer  NOT NULL,
    [Data] blob  NOT NULL,
    [OrderId] integer  NOT NULL,
    [Title] nvarchar(2147483647)  NOT NULL,
    [Extension] nvarchar(2147483647)  NOT NULL
);
GO

-- Creating table 'PaymentsSet'
CREATE TABLE [dbo].[PaymentsSet] (
    [Id] integer  NOT NULL,
    [CustomerId] integer  NOT NULL,
    [Amount] decimal(18,0)  NOT NULL,
    [PayDate] datetime  NOT NULL,
    [OrderId] integer  NOT NULL
);
GO

-- Creating table 'BillsSet'
CREATE TABLE [dbo].[BillsSet] (
    [Id] integer  NOT NULL,
    [IsPayable] bit  NOT NULL,
    [PublishDate] datetime  NOT NULL,
    [InvoiceDate] datetime  NOT NULL,
    [JournalId] integer  NOT NULL,
    [Orders] nvarchar(2147483647)  NOT NULL,
    [CustomerId] integer  NOT NULL
);
GO

-- Creating table 'NotificationsSet'
CREATE TABLE [dbo].[NotificationsSet] (
    [Id] integer  NOT NULL,
    [BillId] integer  NOT NULL,
    [Number] int  NOT NULL,
    [CreateDate] datetime  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'CustomersSet'
ALTER TABLE [dbo].[CustomersSet]
ADD CONSTRAINT [PK_CustomersSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'OrdersSet'
ALTER TABLE [dbo].[OrdersSet]
ADD CONSTRAINT [PK_OrdersSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'HistorySet'
ALTER TABLE [dbo].[HistorySet]
ADD CONSTRAINT [PK_HistorySet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'LayoutsSet'
ALTER TABLE [dbo].[LayoutsSet]
ADD CONSTRAINT [PK_LayoutsSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'JournalsSet'
ALTER TABLE [dbo].[JournalsSet]
ADD CONSTRAINT [PK_JournalsSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'JournalOrdersSet'
ALTER TABLE [dbo].[JournalOrdersSet]
ADD CONSTRAINT [PK_JournalOrdersSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'JournalLayoutsSet'
ALTER TABLE [dbo].[JournalLayoutsSet]
ADD CONSTRAINT [PK_JournalLayoutsSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PaymentsSet'
ALTER TABLE [dbo].[PaymentsSet]
ADD CONSTRAINT [PK_PaymentsSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'BillsSet'
ALTER TABLE [dbo].[BillsSet]
ADD CONSTRAINT [PK_BillsSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'NotificationsSet'
ALTER TABLE [dbo].[NotificationsSet]
ADD CONSTRAINT [PK_NotificationsSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [CustomerId] in table 'OrdersSet'
ALTER TABLE [dbo].[OrdersSet]
ADD CONSTRAINT [FK_CustomerOrders]
    FOREIGN KEY ([CustomerId])
    REFERENCES [dbo].[CustomersSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_CustomerOrders'
CREATE INDEX [IX_FK_CustomerOrders]
ON [dbo].[OrdersSet]
    ([CustomerId]);
GO

-- Creating foreign key on [OrderId] in table 'LayoutsSet'
ALTER TABLE [dbo].[LayoutsSet]
ADD CONSTRAINT [FK_OrdersLayouts]
    FOREIGN KEY ([OrderId])
    REFERENCES [dbo].[OrdersSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_OrdersLayouts'
CREATE INDEX [IX_FK_OrdersLayouts]
ON [dbo].[LayoutsSet]
    ([OrderId]);
GO

-- Creating foreign key on [CustomerId] in table 'JournalOrdersSet'
ALTER TABLE [dbo].[JournalOrdersSet]
ADD CONSTRAINT [FK_CustomersJournalOrders]
    FOREIGN KEY ([CustomerId])
    REFERENCES [dbo].[CustomersSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_CustomersJournalOrders'
CREATE INDEX [IX_FK_CustomersJournalOrders]
ON [dbo].[JournalOrdersSet]
    ([CustomerId]);
GO

-- Creating foreign key on [OrderId] in table 'JournalLayoutsSet'
ALTER TABLE [dbo].[JournalLayoutsSet]
ADD CONSTRAINT [FK_JournalOrdersJournalLayouts]
    FOREIGN KEY ([OrderId])
    REFERENCES [dbo].[JournalOrdersSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_JournalOrdersJournalLayouts'
CREATE INDEX [IX_FK_JournalOrdersJournalLayouts]
ON [dbo].[JournalLayoutsSet]
    ([OrderId]);
GO

-- Creating foreign key on [JournalId] in table 'JournalOrdersSet'
ALTER TABLE [dbo].[JournalOrdersSet]
ADD CONSTRAINT [FK_JournalsJournalOrders]
    FOREIGN KEY ([JournalId])
    REFERENCES [dbo].[JournalsSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_JournalsJournalOrders'
CREATE INDEX [IX_FK_JournalsJournalOrders]
ON [dbo].[JournalOrdersSet]
    ([JournalId]);
GO

-- Creating foreign key on [CustomerId] in table 'PaymentsSet'
ALTER TABLE [dbo].[PaymentsSet]
ADD CONSTRAINT [FK_CustomersPayments]
    FOREIGN KEY ([CustomerId])
    REFERENCES [dbo].[CustomersSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_CustomersPayments'
CREATE INDEX [IX_FK_CustomersPayments]
ON [dbo].[PaymentsSet]
    ([CustomerId]);
GO

-- Creating foreign key on [JournalId] in table 'BillsSet'
ALTER TABLE [dbo].[BillsSet]
ADD CONSTRAINT [FK_JournalsBills]
    FOREIGN KEY ([JournalId])
    REFERENCES [dbo].[JournalsSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_JournalsBills'
CREATE INDEX [IX_FK_JournalsBills]
ON [dbo].[BillsSet]
    ([JournalId]);
GO

-- Creating foreign key on [BillId] in table 'NotificationsSet'
ALTER TABLE [dbo].[NotificationsSet]
ADD CONSTRAINT [FK_BillsNotifications]
    FOREIGN KEY ([BillId])
    REFERENCES [dbo].[BillsSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_BillsNotifications'
CREATE INDEX [IX_FK_BillsNotifications]
ON [dbo].[NotificationsSet]
    ([BillId]);
GO

-- Creating foreign key on [CustomerId] in table 'BillsSet'
ALTER TABLE [dbo].[BillsSet]
ADD CONSTRAINT [FK_CustomersBills]
    FOREIGN KEY ([CustomerId])
    REFERENCES [dbo].[CustomersSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_CustomersBills'
CREATE INDEX [IX_FK_CustomersBills]
ON [dbo].[BillsSet]
    ([CustomerId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------