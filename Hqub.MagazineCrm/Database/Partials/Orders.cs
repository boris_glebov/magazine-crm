﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Hqub.MagazineCrm.Core;

namespace Hqub.MagazineCrm.Database
{
    public partial class Orders : IDataErrorInfo, ICloneable
    {
        private readonly Dictionary<string, string> _collectionErrors = new Dictionary<string, string>();

        public Orders()
        {
            Period = (int)PeriodEnum.EveryMonth;
        }

        /// <summary>
        /// Validation
        /// </summary>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public string this[string propertyName]
        {
            get
            {
                switch(propertyName)
                {
                    case "Period":
                        const string propertyPeriod = "Period";

                        if(Period == 0)
                        {
                            if (!_collectionErrors.ContainsKey(propertyPeriod))
                                _collectionErrors.Add(propertyPeriod, Properties.Errors.NameRequiredToFill);

                            return Properties.Errors.NameRequiredToFill;
                        }

                        _collectionErrors.Remove(propertyPeriod);
                        break;

                }

                return null;
            }
        }

        /// <summary>
        /// Check the rules of the periodicity
        /// </summary>
        public bool CheckOrderPeriod(DateTime currentDate)
        {
            var journalService = NinjectBootstrap.GetJournalService(false, false);

            var dateNow = currentDate;
            var countMonth = (Math.Abs(StartYear - dateNow.Year) * 12 - StartMonth) + dateNow.Month + 1;

            if ((countMonth <= 0 && ((PeriodEnum)Period) != PeriodEnum.EveryMonth) || EndMonth < dateNow.Month)
                return false;

            switch ((PeriodEnum)Period)
            {
                case PeriodEnum.EveryMonth:
                    return true;
                case PeriodEnum.EveryTwoMonths:
                    return (countMonth % 2) == 0;
                case PeriodEnum.EveryThreeMonths:
                    return (countMonth % 3) == 0;
                case PeriodEnum.EverySixMonth:
                    return (countMonth % 6) == 0;
                case PeriodEnum.OneHalfYear:
                    if (StartYear <= dateNow.Year && EndYear >= dateNow.Year)
                        return journalService.CheckPublishOrderHalfYear(Id, StartMonth, StartYear);
                    return false;
                case PeriodEnum.OneYear:
                    var year = dateNow.Year;
                    if (StartYear <= year && EndYear >= year)
                        return journalService.CheckPublishOrder(Id, dateNow.Year);

                    return false;
            }
            return true;
        }

        /// <summary>
        /// Convert to JournalOrder
        /// </summary>
        /// <returns></returns>
        public JournalOrders ToJournalOrder()
        {
            return new JournalOrders
                       {
                           RealOrderId = Id,
                           Amount = Amount,
                           CustomerId = CustomerId,
                           Customers = Customer,
                           Description = Description,
                           StartYear = StartYear,
                           EndYear = EndYear,
                           StartMonth = StartMonth,
                           EndMonth = EndMonth,
                           IsPrepay = IsPrepay,
                           Price = Price,
                           PriceBrutto = PriceBrutto,
                           Size = Size,
                           Period = Period,
                           ExtraNotes = ExtraNotes
                       };
        }

        /// <summary>
        /// 
        /// </summary>
        private bool _isChecked;
        public bool IsChecked
        {
            get { return _isChecked; }
            set
            {
                _isChecked = value;
                OnPropertyChanged("IsChecked");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private bool _isInclude = true;
        public bool IsInclude
        {
            get { return _isInclude; }
            set
            {
                _isInclude = value;
                OnPropertyChanged("IsInclude");
            }
        }

        /// <summary>
        /// Last error, alfter validation
        /// </summary>
        public string Error { get; set; }
        public bool IsError { get { return _collectionErrors.Count > 0; } }

        /// <summary>
        /// Memberwise clone order
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}
