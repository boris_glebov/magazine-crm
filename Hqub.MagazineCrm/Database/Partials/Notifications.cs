﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hqub.MagazineCrm.Database
{
    public partial class Notifications
    {
        private string _title;
        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                OnPropertyChanged("Title");
            }
        }

        private Journals _journal;
        public Journals Journal
        {
            get { return _journal; }
            set
            {
                _journal = value;
                OnPropertyChanged("Journal");
            }
        }

    }
}
