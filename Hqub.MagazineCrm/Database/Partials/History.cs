﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Hqub.MagazineCrm.Database
{
    public partial class History
    {
        private object _entity;
        public object Entity
        {
            get { return _entity = _entity ?? Core.SerializeHelper.Deserialize(new MemoryStream(Data)); }
        }
    }
}
