﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hqub.MagazineCrm.Database
{
    public partial class JournalOrders
    {
        private static List<Group> Groups = new List<Group>();
        public static Group CreateGroup(long id, string name)
        {
            var group = Groups.Where(x => x.Id == id).FirstOrDefault();
            if (group != null)
                return group;

            group = new Group(id, name);
            Groups.Add(group);

            return group;
        }

        public class Group
        {
            public Group(long id, string name)
            {
                Id = id;
                Name = name;
            }

            public long Id
            {
                get;
                set;
            }

            public string Name { get; set; }

            public override string ToString()
            {
                return string.Format("{0:0000} {1}", Id, Name);
            }
        }

        private readonly Dictionary<string, string> _collectionErrors = new Dictionary<string, string>();

        /// <summary>
        /// Validation
        /// </summary>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public string this[string propertyName]
        {
            get
            {
                switch (propertyName)
                {
                    case "Period":
                        const string propertyPeriod = "Period";

                        if (Period == 0)
                        {
                            if (!_collectionErrors.ContainsKey(propertyPeriod))
                                _collectionErrors.Add(propertyPeriod, Properties.Errors.NameRequiredToFill);

                            return Properties.Errors.NameRequiredToFill;
                        }

                        _collectionErrors.Remove(propertyPeriod);
                        break;

                    case "Notes":
                        const string propertyNotes = "Notes";

                        if (string.IsNullOrEmpty(Notes) && JournalLayouts.Count == 0)
                        {
                            if (!_collectionErrors.ContainsKey(propertyNotes))
                                _collectionErrors.Add(propertyNotes, "Добавьте шаблоны обьявлений, либо заполните поле Notes");

                            return "Добавьте шаблоны обьявлений, либо заполните поле Notes";
                        }

                        _collectionErrors.Remove(propertyNotes);

                        break;
                }

                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private bool _isChecked;
        public bool IsChecked
        {
            get { return _isChecked; }
            set
            {
                _isChecked = value;
                OnPropertyChanged("IsChecked");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private bool _isInclude = true;
        public bool IsInclude
        {
            get { return _isInclude; }
            set
            {
                _isInclude = value;
                OnPropertyChanged("IsInclude");
            }
        }

        /// <summary>
        /// Last error, alfter validation
        /// </summary>
        public string Error { get; set; }
        public bool IsError { get { return _collectionErrors.Count > 0; } }

        /// <summary>
        /// Property for grouping
        /// </summary>
        private Group _groupEntity;
        public Group GroupEntity
        {
            get
            {
                if (_groupEntity == null && this.Customers != null)
                    return _groupEntity = CreateGroup(Customers.Id, Customers.Name);

                return _groupEntity;
            }
            set
            {
                _groupEntity = value;
                OnPropertyChanged("GroupEntity");
            }
        }

        /// <summary>
        /// Memberwise clone order
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}
