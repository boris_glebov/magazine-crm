﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace Hqub.MagazineCrm.Database
{
    public partial class Customers : IDataErrorInfo
    {
        public decimal RecalBalance()
        {
            return NinjectBootstrap.GetPaymentService().GetBalance(Id);
        }

        public string this[string propertyName]
        {
            get
            {
                switch(propertyName)
                {
                    case "Name":
                        const string propertyCompanyName = "Name";
                        
                        if(string.IsNullOrEmpty(Name))
                        {
                            if (!_collectionErrors.ContainsKey(propertyCompanyName))
                                _collectionErrors.Add(propertyCompanyName, Properties.Errors.NameRequiredToFill);

                            return Properties.Errors.NameRequiredToFill;
                        }

                        _collectionErrors.Remove(propertyCompanyName);

                        break;

//                    case "Adresse":
//                        const string propertyAdress = "Adress";
//
//                        if(string.IsNullOrEmpty(Address))
//                        {
//                            if(!_collectionErrors.ContainsKey(propertyAdress))
//                                _collectionErrors.Add(propertyAdress, Properties.Errors.AddresseRequiredToFill);
//
//                            return Properties.Errors.AddresseRequiredToFill;
//                        }
//
//                        _collectionErrors.Remove(propertyAdress);
//
//                        break;

//                    case "Phone":
//                        const string propertyPhone = "Phone";
//
//                        if(string.IsNullOrEmpty(Phone))
//                        {
//                            if(!_collectionErrors.ContainsKey(propertyPhone))
//                                _collectionErrors.Add(propertyPhone, Properties.Errors.PhoneRequiredToFill);
//
//                            return Properties.Errors.PhoneRequiredToFill;
//                        }
//
//                        _collectionErrors.Remove(propertyName);
//
//                        break;
                }

                return null;
            }
        }

        private readonly Dictionary<string, string> _collectionErrors = new Dictionary<string, string>();

        public string Error { get; set; }
        public bool IsError { get { return _collectionErrors.Count > 0; } }

        public SolidColorBrush Foreground
        {
            get
            {
                if (RecalBalance() < 0)
                    return Brushes.Red;

                if (Orders.Count == 0)
                    return Brushes.Gray;

                return Brushes.Black;
            }
        }

        public int IntegerForeground
        {
            get
            {
                if (RecalBalance() < 0)
                    return -1;

                if (Orders.Count == 0)
                    return 0;

                return 1;
            }

        }

        public string FullName
        {
            get { return ToString(); }
        }

        public override string ToString()
        {
            return string.Format("{0:0000} {1}", Id, Name);
        }
    }
}
