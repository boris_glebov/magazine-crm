﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace Hqub.MagazineCrm.Database
{
    public class CustomSort : IComparer
    {
        public CustomSort(ListSortDirection direction, DataGridColumn column)
        {
            Direction = direction;
            Column = column;
        }

        public ListSortDirection Direction { get; private set; }

        public DataGridColumn Column { get; private set; }

        private int StringCompare(string s1, string s2)
        {
            if (Direction == ListSortDirection.Ascending)
                return s1.CompareTo(s2);
            return s2.CompareTo(s1);
        }

        private int IntCompare(int int1, int int2)
        {
            if (Direction == ListSortDirection.Ascending)
                return int1.CompareTo(int2);
            return int2.CompareTo(int1);
        }

        private int LongCompare(long l1, long l2)
        {
            if (Direction == ListSortDirection.Ascending)
                return l1.CompareTo(l2);
            return l2.CompareTo(l1);
        }

        private int FloatCompare(float f1, float f2)
        {
            if (Direction == ListSortDirection.Ascending)
                return f1.CompareTo(f2);
            return f2.CompareTo(f1);
        }

        private int DoubleCompare(double d1, double d2)
        {
            if (Direction == ListSortDirection.Ascending)
                return d1.CompareTo(d2);
            return d2.CompareTo(d1);
        }

        private int DecimalCompare(decimal d1, decimal d2)
        {
            if (Direction == ListSortDirection.Ascending)
                return d1.CompareTo(d2);
            return d2.CompareTo(d1);
        }

        int IComparer.Compare(object X, object Y)
        {
            var property = X.GetType().GetProperty(Column.SortMemberPath);

            var valueX = property.GetValue(X, null);
            var valueY = property.GetValue(Y, null);

            if (valueX == null || valueY == null)
                return 0;

            if (valueX is int)
            {
                return IntCompare((int)valueX, (int)valueY);
            }

            if(valueX is long)
            {
                return LongCompare((long) valueX, (long) valueY);
            }

            if(valueX is float)
            {
                return FloatCompare((float)valueX, (float)valueY);
            }

            if(valueX is decimal)
            {
                return DecimalCompare((decimal) valueX, (decimal) valueY);
            }

            if(valueX is double)
            {
                return DoubleCompare((double) valueX, (double) valueY);
            }

            if (valueX is string)
            {
                return StringCompare((string) valueX, (string) valueY);
            }

            return 0;
        }
    }
}
