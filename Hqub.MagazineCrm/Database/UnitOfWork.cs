﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hqub.MagazineCrm.Database
{
    public class UnitOfWork
    {
        private readonly ModelDatabaseContainer _globalContext = new ModelDatabaseContainer();
        private readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        public ModelDatabaseContainer Context { get { return _globalContext; } }

        public bool Commit()
        {
            try
            {
                _globalContext.SaveChanges();
                return true;
            }
            catch (Exception exception)
            {
                _logger.ErrorException(exception.Message, exception);
                if (exception.InnerException != null)
                    _logger.ErrorException(exception.InnerException.Message, exception.InnerException);
                
                return false;
            }
        }
    }
}
