﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Hqub.MagazineCrm.Model
{
    public class ReportEntity : Model
    {
        public ReportEntity(string name,string title, SelectReportAction action , string notes = "")
        {
            Name = name;
            Title = title;
            Notes = notes;
            Action = action;
        }

        #region Properties

        public delegate void SelectReportAction();

        public SelectReportAction Action { get; set; }
        #endregion

        #region Binding Properties

        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                RaisePropertyChanged("Name");
            }
        }

        private string _title;
        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                RaisePropertyChanged("Title");
            }
        }

        private string _notes;
        public string Notes
        {
            get { return _notes; }
            set
            {
                _notes = value;
                RaisePropertyChanged("Notes");
            }
        }

        #endregion
    }
}
