﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Hqub.MagazineCrm.Model
{
    public class NotificationEntity : Model
    {
        private int _num;
        public int Num
        {
            get { return _num; }
            set
            {
                _num = value;
                RaisePropertyChanged("Num");
            }
        }

        private ObservableCollection<Database.JournalOrders> _journalOrders;
        public ObservableCollection<Database.JournalOrders> JournalOrders
        {
            get { return _journalOrders; }
            set
            {
                _journalOrders = value;
                RaisePropertyChanged("JournalOrders");
            }
        } 
    }
}
