﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Hqub.MagazineCrm.Model
{
    public class OrderPeriodCollection : ObservableCollection<OrderPeriod>
    {

        public OrderPeriodCollection()
        {
            Add(new OrderPeriod(Properties.Resources.EveryMonth, Core.PeriodEnum.EveryMonth));
            Add(new OrderPeriod(Properties.Resources.EveryTwoMonths, Core.PeriodEnum.EveryTwoMonths));
            Add(new OrderPeriod(Properties.Resources.EveryThreeMonths, Core.PeriodEnum.EveryThreeMonths));
            Add(new OrderPeriod(Properties.Resources.EverySixMonth, Core.PeriodEnum.EverySixMonth));
            Add(new OrderPeriod(Properties.Resources.OneHalfYear, Core.PeriodEnum.OneHalfYear));
            Add(new OrderPeriod(Properties.Resources.OneYear, Core.PeriodEnum.OneYear));
        }

        private static OrderPeriodCollection _instance;
        public static OrderPeriodCollection Instance
        {
            get { return _instance = _instance ?? new OrderPeriodCollection(); }
        }
    }
}
