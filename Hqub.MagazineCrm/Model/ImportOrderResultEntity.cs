﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hqub.MagazineCrm.Model
{
    public class ImportOrderResultEntity : Model
    {
        private long _id;
        public long Num
        {
            get { return _id; }
            set
            {
                _id = value;
                RaisePropertyChanged("Num");
            }
        }

        private string _customerName;
        public string Customer
        {
            get { return _customerName; }
            set
            {
                _customerName = value;
                RaisePropertyChanged("Customer");
            }
        }

        private string _description;
        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                RaisePropertyChanged("Description");
            }
        }

        private string _status;
        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                RaisePropertyChanged("Status");
            }
        }
    }
}
