﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hqub.MagazineCrm.Model
{
    public class OrderPeriod : Model
    {
        public OrderPeriod(string title, Core.PeriodEnum periodEnum)
        {
            Title = title;
            PeriodEnum = periodEnum;
        }

        private string _title;
        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                RaisePropertyChanged("Title");
            }
        }


        private Core.PeriodEnum _periodEnum;
        public Core.PeriodEnum PeriodEnum
        {
            get { return _periodEnum; }
            set
            {
                _periodEnum = value;
                RaisePropertyChanged("PeriodEnum");
            }
        }
    }
}
