﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Xml.Serialization;

namespace Hqub.MagazineCrm.Model
{
    [XmlRoot("")]
    public class WindowSettingsEntity
    {
        public double Height { get; set; }

        public double Width { get; set; }

        public double Top { get; set; }

        public double Left { get; set; }

        public WindowState WindowState { get; set; }
    }
}
