﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Hqub.MagazineCrm.Model
{
    public class MonthsCollection : ObservableCollection<int>
    {
        public MonthsCollection()
        {
            //Init month:
            for(var i=1; i<=12; ++i)
                Add(i);
        }
    }
}
