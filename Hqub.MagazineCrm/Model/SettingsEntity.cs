﻿using System;
using System.Xml.Serialization;

namespace Hqub.MagazineCrm.Model
{
    [XmlRoot("config")]
    public class SettingsEntity
    {
        [XmlRoot("database")]
        public class Database
        {
            [XmlAttribute("name")]
            public string Name { get; set; }
        }

        [XmlRoot("paths")]
        public class PathSettings
        {
            private string _journalPath;
            private string _tempPath;
            private string _templatesPath;

            [XmlElement("journal")]
            public string JournalsPath
            {
                get
                {
                    return string.IsNullOrEmpty(_journalPath)
                               ? AppDomain.CurrentDomain.BaseDirectory + "Journals\\"
                               : _journalPath;
                }
                set { _journalPath = value; }
            }

            [XmlElement("temp")]
            public string TempPath
            {
                get { return string.IsNullOrEmpty(_tempPath) ? AppDomain.CurrentDomain.BaseDirectory + "Temp\\" : _tempPath; }
                set { _tempPath = value; }
            }

            [XmlElement("templates")]
            public string TemplatesPath
            {
                get
                {
                    return string.IsNullOrEmpty(_templatesPath)
                               ? AppDomain.CurrentDomain.BaseDirectory + "Templates\\"
                               : _templatesPath;
                }
                set { _templatesPath = value; }
            }
        }

        [XmlElement("database")]
        public Database DatabaseEntity { get; set; }

        [XmlElement("language")]
        public string Language { get; set; }

        [XmlElement("errinerung")]
        public int Errinerung { get; set; }

        [XmlElement("mahnung")]
        public int Mahnung { get; set; }

        [XmlElement("tax")]
        public decimal Tax { get; set; }

        [XmlElement("printer")]
        public string Printer { get; set; }

        [XmlElement("pdf_printer")]
        public string PdfPrinter { get; set; }

        [XmlElement("auto_print")]
        public bool AutoPrint { get; set; }

        [XmlElement("active_printer")]
        public string ActivePrinter { get; set; }

        [XmlElement("paths")]
        public PathSettings Paths { get; set; }
    }
}
