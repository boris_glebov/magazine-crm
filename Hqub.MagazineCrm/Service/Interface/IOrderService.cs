﻿using System.Collections.Generic;

namespace Hqub.MagazineCrm.Service.Interface
{
    public interface IOrderService : IService
    {
        IEnumerable<Database.Orders> List();

        Database.Layouts AddLayout(Database.Layouts layout, Database.Orders order);

        bool Add(Database.Orders order);
    }
}
