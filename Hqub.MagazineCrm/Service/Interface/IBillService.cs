﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hqub.MagazineCrm.Service.Interface
{
    public interface IBillService : IService
    {
        bool Add(Database.Bills bill);
        List<Database.Bills> GetBills(bool onlyUnpaid);
    }
}
