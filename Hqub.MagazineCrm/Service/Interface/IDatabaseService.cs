﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hqub.MagazineCrm.Database;

namespace Hqub.MagazineCrm.Service.Interface
{
    public interface IDatabaseService : IService
    {
        void Clear();

        bool InsertCustomer(Customers customer);
        bool InsertOrder(Orders order);
        bool InsertJournalOrder(Orders order, DateTime publishDate, DateTime? payableDate);
    }
}
