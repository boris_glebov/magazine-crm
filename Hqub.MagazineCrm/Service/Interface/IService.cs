﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hqub.MagazineCrm.Service.Interface
{
    public interface IService
    {
        bool SaveChanges();
    }
}
