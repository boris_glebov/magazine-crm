﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hqub.MagazineCrm.Database;

namespace Hqub.MagazineCrm.Service.Interface
{
    public interface IJournalService : IService
    {
        IEnumerable<JournalOrders> GetJournalOrders(long customerId, long journalId);
        IEnumerable<JournalOrders> GetClosedOrders(DateTime currentDateTime);
        IEnumerable<JournalOrders> GetNewOrders(DateTime currentDateTime);
        IEnumerable<JournalOrders> GetCurrentOrders();
        IEnumerable<JournalOrders> GetPaidOrders();
        IEnumerable<JournalOrders> GetUnPaidOrders();
        DateTime? GetLastJournalDate();

        JournalOrders GetJournalOrder(long orderId);

        IEnumerable<Journals> List();
        Journals GetJournal(int journalId);

        bool CheckPublishOrderHalfYear(long orderId, int month, int year);
        bool CheckPublishOrder(long orderId, int year);
        bool Save(Journals journal);
    }
}
