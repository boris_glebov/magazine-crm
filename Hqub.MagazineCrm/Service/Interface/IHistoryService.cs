﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using Hqub.MagazineCrm.Database;

namespace Hqub.MagazineCrm.Service.Interface
{
    public interface IHistoryService : IService
    {
        IEnumerable<History> CustomerCollection(long customerId);
        bool AddCustomer(long entityId, Binary customer);
    }
}
