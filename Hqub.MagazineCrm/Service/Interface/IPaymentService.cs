﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hqub.MagazineCrm.Service.Interface
{
    public interface IPaymentService : IService
    {
        bool Add(Database.Payments payment);
        decimal GetBalance(long customerId);
        IEnumerable<Database.Payments> List(long customerId);
    }
}
