﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hqub.MagazineCrm.Service.Interface
{
    public interface ICustomerService : IService
    {
        IEnumerable<Database.Customers> List();

        Database.Customers Get(long id);
        IEnumerable<Database.Customers> Get(string partName);

        bool Add(Database.Customers customer);
        bool Exists(int id);
        bool Exists(string name);
    }
}
