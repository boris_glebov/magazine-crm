﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using Hqub.MagazineCrm.Database;

namespace Hqub.MagazineCrm.Service.Implementation
{
    public class HistoryService : Service.Interface.IHistoryService
    {
        private readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();

        private const string CustomerType = "Customers";
        private const string OrdersType = "Orders";

        public bool AddCustomer(long entityId, Binary customer)
        {
            _unitOfWork.Context.HistorySet.AddObject(new History
                                                         {
                                                             TypeName = CustomerType,
                                                             Data = customer.ToArray(),
                                                             DateStamp = DateTime.Now,
                                                             EntityId = entityId
                                                         });

            return SaveChanges();
        }

        public IEnumerable<History> CustomerCollection(long customerId)
        {
            return _unitOfWork.Context.HistorySet.Where(x => x.TypeName == CustomerType && x.EntityId == customerId);
        }

        public bool SaveChanges()
        {
            return _unitOfWork.Commit();
        }
    }
}
