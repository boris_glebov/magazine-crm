﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hqub.MagazineCrm.Database;

namespace Hqub.MagazineCrm.Service.Implementation
{
    public class JournalService : Interface.IJournalService
    {
        private readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();

        public IEnumerable<JournalOrders> GetJournalOrders(long customerId, long journalId)
        {
            return
                _unitOfWork.Context.JournalOrdersSet.Where(
                    order => order.JournalId == journalId && order.CustomerId == customerId);
        }

        public JournalOrders GetJournalOrder(long orderId)
        {
            return _unitOfWork.Context.JournalOrdersSet.FirstOrDefault(x => x.RealOrderId == orderId);
        }

        #region Reports

        public IEnumerable<JournalOrders> GetClosedOrders(DateTime currentDateTime)
        {
            var prevDate = currentDateTime.AddMonths(-1);

            var orders = _unitOfWork.Context.OrdersSet.ToList();

            var currentOrders = GetOrdersByDate(currentDateTime, orders);
            var prevOrders = GetOrdersByDate(prevDate, orders);

            return prevOrders.Where(x=>!currentOrders.Contains(x)).Select(order => order.ToJournalOrder());
        }

        public IEnumerable<JournalOrders> GetNewOrders(DateTime currentDateTime)
        {
            var prevDate = currentDateTime.AddMonths(-1);

            var orders = _unitOfWork.Context.OrdersSet.ToList();

            var currentOrders = GetOrdersByDate(currentDateTime, orders);
            var prevOrders = GetOrdersByDate(prevDate, orders);

            return currentOrders.Where(x => !prevOrders.Contains(x)).Select(order => order.ToJournalOrder());
        }

        public IEnumerable<JournalOrders> GetCurrentOrders()
        {
            var orders = GetOrdersByDate(DateTime.Now, _unitOfWork.Context.OrdersSet.ToList());

            return orders.Select(order => order.ToJournalOrder());
        }

        private IEnumerable<Orders> GetOrdersByDate(DateTime date, IList<Orders> orders)
        {
            return from order in orders
                                where
                                    (order.StartYear <= date.Year &&
                                     order.StartMonth <= date.Month) &&
                                    (order.EndYear >= date.Year && order.EndMonth <= date.Year) &&
                                    order.CheckOrderPeriod(date)
                                select order;
        }

        public IEnumerable<JournalOrders> GetPaidOrders()
        {
            return _unitOfWork.Context.JournalOrdersSet.Where(x => x.IsPayable);
        }

        public IEnumerable<JournalOrders> GetUnPaidOrders()
        {
            return _unitOfWork.Context.JournalOrdersSet.Where(x => !x.IsPayable);
        }
        #endregion

        public IEnumerable<Journals> List()
        {
            return _unitOfWork.Context.JournalsSet;
        }

        public Journals GetJournal(int journalId)
        {
            return _unitOfWork.Context.JournalsSet.Where(x => x.Id == journalId).FirstOrDefault();
        }

        public DateTime? GetLastJournalDate()
        {
           var journal = _unitOfWork.Context.JournalsSet.OrderByDescending(x => x.Id).FirstOrDefault();

            if(journal == null)
                return null;

            return new DateTime(journal.PublishDate.Year, journal.PublishDate.Month, journal.PublishDate.Day);
        }

        public bool CheckPublishOrderHalfYear(long orderId, int month, int year)
        {
            var last =
                _unitOfWork.Context.JournalOrdersSet.Where(x => x.RealOrderId == orderId).OrderBy(x => x.PublishDate).
                    LastOrDefault();

            if (last == null || last.PublishDate == null)
                return true;

            var publishDate = (DateTime) last.PublishDate;

            var m = (publishDate - new DateTime(year, month, (publishDate.Day))).Days / 30;

            return Math.Abs(m) >= 6;
        }

        public bool CheckPublishOrder(long orderId, int year)
        {
            return
                !_unitOfWork.Context.JournalOrdersSet.Any(
                    x => x.RealOrderId == orderId && x.PublishDate != null && ((DateTime) x.PublishDate).Year == year);
        }

        public bool Save(Journals journal)
        {
            _unitOfWork.Context.JournalsSet.AddObject(journal);
            return SaveChanges();
        }

        public bool SaveChanges()
        {
            return _unitOfWork.Commit();
        }
    }
}
