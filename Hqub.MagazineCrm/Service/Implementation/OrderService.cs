﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hqub.MagazineCrm.Database;

namespace Hqub.MagazineCrm.Service.Implementation
{
    public class OrderService : Interface.IOrderService
    {
        private readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();


        public bool SaveChanges()
        {
            return _unitOfWork.Commit();
        }

        public IEnumerable<Orders> List()
        {
            try
            {
                return _unitOfWork.Context.OrdersSet;
            }
            catch (Exception exception)
            {
                _logger.ErrorException(exception.Message, exception);
                return new List<Orders>();
            }
        }

        public Layouts AddLayout(Layouts layout, Orders order)
        {
            order.Layouts.Add(layout);

            _unitOfWork.Context.LayoutsSet.AddObject(layout);

            SaveChanges();

            return layout;
        }

        public bool Add(Orders order)
        {
            _unitOfWork.Context.OrdersSet.AddObject(order);

            return SaveChanges();
        }
    }
}
