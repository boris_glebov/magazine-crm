﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hqub.MagazineCrm.Database;

namespace Hqub.MagazineCrm.Service.Implementation
{
    public class PaymentService : Interface.IPaymentService
    {
        private readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();

        public bool Add(Payments payment)
        {
            _unitOfWork.Context.PaymentsSet.AddObject(payment);
            return SaveChanges();
        }

        public decimal GetBalance(long customerId)
        {
            var customer = _unitOfWork.Context.CustomersSet.Where(c => c.Id == customerId).FirstOrDefault();
            if (customer == null)
                return 0;

            return customer.Payments.Select(p => p.Amount).Sum();
        }

        public IEnumerable<Payments> List(long customerId)
        {
            var c = _unitOfWork.Context.CustomersSet.FirstOrDefault(x => x.Id == customerId);

            return c != null ? c.Payments : new List<Payments>().AsEnumerable();
        }

        public bool SaveChanges()
        {
            return _unitOfWork.Commit();
        }
    }
}
