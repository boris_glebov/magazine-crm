﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hqub.MagazineCrm.Core.Configure;
using Hqub.MagazineCrm.Database;
using System.Data.SQLite;
using System.Data.Common;
using System.Data;

namespace Hqub.MagazineCrm.Service.Implementation
{
    public class DatabaseService : Interface.IDatabaseService
    {
        private readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();

        public void Clear()
        {
            ClearTable("CustomersSet");
            ClearTable("OrdersSet");
            ClearTable("JournalOrdersSet");
            ClearTable("JournalsSet");
            ClearTable("PaymentsSet");
        }

        private void ClearTable(string tableName)
        {
            var dbPath = AppDomain.CurrentDomain.BaseDirectory + Settings.DatabaseSettings.Name;

            try
            {
                using (var connection = new SQLiteConnection(string.Format("Data Source={0};", dbPath)))
                {
                    connection.Open();

                    var command = connection.CreateCommand();

                    //Clear data from CustomerSet

                    command.CommandText = string.Format("DELETE FROM {0}", tableName);
                    command.ExecuteNonQuery();

                    //Reset sequence CusomerSet
                    command.CommandText = string.Format("delete from sqlite_sequence where name='{0}'", tableName);
                    command.ExecuteNonQuery();

                    connection.Close();
                }
            }
            catch (Exception exception)
            {
                _logger.ErrorException(exception.Message, exception);
                if (exception.InnerException != null)
                    _logger.ErrorException(exception.InnerException.Message, exception.InnerException);
            }
        }

        public bool InsertCustomer(Customers customer)
        {
            var dbPath = AppDomain.CurrentDomain.BaseDirectory + Settings.DatabaseSettings.Name;

            try
            {
                using (var connection = new SQLiteConnection(string.Format("Data Source={0};", dbPath)))
                {
                    connection.Open();

                    var command = connection.CreateCommand();

                    command.CommandText = string.Format(@"Insert INTO CustomersSet('Id', 'Name',  'Zip', 'Phone', 'Cell', 'Contact', 'Note', 'Email', 'HomePage', 'Balance', 'Fax', 'City', 'Country', 'FirstName', 'LastName', 'Salutation', 'House', 'Street', 'PaymentMadeByDirectDebit') 
                                                                 VALUES(@Id, @Name, @Zip, @Phone, @Cell, @Contact, @Note, @Email, @HomePage, @Balance, @Fax, @City, @Country, @FirstName, @LastName, @Salutation, @House, @Street, @PaymentMadeByDirectDebit)
                        ");
                    command.Parameters.AddRange(new []
                                                    {
                                                        new SQLiteParameter("@Id", customer.Id),
                                                        new SQLiteParameter("@Name", customer.Name),
                                                        new SQLiteParameter(@"Zip", customer.Zip),
                                                        new SQLiteParameter(@"Phone", customer.Phone),
                                                        new SQLiteParameter("@Cell", customer.Cell),
                                                        new SQLiteParameter("@Contact", customer.Contact),
                                                        new SQLiteParameter("@Note", customer.Note),
                                                        new SQLiteParameter("@Email", customer.Email),
                                                        new SQLiteParameter("@HomePage", customer.HomePage),
                                                        new SQLiteParameter("@Balance", customer.Balance),
                                                        new SQLiteParameter("@Fax", customer.Fax),
                                                        new SQLiteParameter("@City", customer.City),
                                                        new SQLiteParameter("@Country", customer.Country), 
                                                        new SQLiteParameter("@FirstName", customer.FirstName),
                                                        new SQLiteParameter("@LastName", customer.LastName),
                                                        new SQLiteParameter("@Salutation", customer.Salutation),
                                                        new SQLiteParameter("@House", customer.House),
                                                        new SQLiteParameter("@Street", customer.Street),
                                                        new SQLiteParameter(@"PaymentMadeByDirectDebit", customer.PaymentMadeByDirectDebit), 
                                                    });
                    var result = command.ExecuteNonQuery();

                    connection.Close();

                    return result == 1;
                }
            }
            catch (Exception exception)
            {
                _logger.ErrorException(exception.Message, exception);
                if (exception.InnerException != null)
                    _logger.ErrorException(exception.InnerException.Message, exception.InnerException);

                return false;
            }
        }
        
        public bool InsertOrder(Orders order)
        {
            var dbPath = AppDomain.CurrentDomain.BaseDirectory + Settings.DatabaseSettings.Name;

            try
            {
                using (var connection = new SQLiteConnection(string.Format("Data Source={0};", dbPath)))
                {
                    connection.Open();

                    var command = connection.CreateCommand();

                    command.CommandText = string.Format(@"Insert INTO OrdersSet('Id', 
                                                                                    'IsPrepay', 
                                                                                    'IsSetInvoce',
                                                                                    'IsPayable', 
                                                                                    'Amount', 
                                                                                    'CustomerId', 
                                                                                    'Notes', 
                                                                                    'Price', 
                                                                                    'Period', 
                                                                                    'Size', 
                                                                                    'Prepay', 
                                                                                    'Description', 
                                                                                    'PriceBrutto', 
                                                                                    'StartMonth', 
                                                                                    'StartYear', 
                                                                                    'EndMonth', 
                                                                                    'EndYear', 
                                                                                    'ExtraNotes',
                                                                                    'IsAutomatic'    
                                                                                ) 

                                                                       VALUES(@Id, 
                                                                                    @IsPrepay, 
                                                                                    @IsSetInvoce, 
                                                                                    @IsPayable, 
                                                                                    @Amount, 
                                                                                    @CustomerId, 
                                                                                    @Notes,
                                                                                    @Price,
                                                                                    @Period, 
                                                                                    @Size,
                                                                                    @Prepay, 
                                                                                    @Description,
                                                                                    @PriceBrutto,
                                                                                    @StartMonth, 
                                                                                    @StartYear,
                                                                                    @EndMonth,
                                                                                    @EndYear, 
                                                                                    @ExtraNotes, 
                                                                                    @IsAutomatic)
                        ");
                    command.Parameters.AddRange(new[]
                                                    {
                                                        new SQLiteParameter("@Id", order.Id),
                                                        new SQLiteParameter("@IsPrepay", order.IsPrepay),
                                                        new SQLiteParameter("@IsSetInvoce", order.IsSetInvoce),
                                                        new SQLiteParameter("@IsPayable", order.IsPayable),
                                                        new SQLiteParameter("@Amount", order.Amount),
                                                        new SQLiteParameter("@CustomerId", order.CustomerId),
                                                        new SQLiteParameter("@Notes", order.Notes),
                                                        new SQLiteParameter("@Price", order.Price),
                                                        new SQLiteParameter("@Period", order.Period),
                                                        new SQLiteParameter("@Size", order.Size),
                                                        new SQLiteParameter("@Prepay", order.Prepay),
                                                        new SQLiteParameter("@Description", order.Description),
                                                        new SQLiteParameter("@PriceBrutto", order.PriceBrutto),
                                                        new SQLiteParameter("@StartMonth", order.StartMonth),
                                                        new SQLiteParameter("@StartYear", order.StartYear),
                                                        new SQLiteParameter("@EndMonth", order.EndMonth),
                                                        new SQLiteParameter("@EndYear", order.EndYear), 
                                                        new SQLiteParameter("@ExtraNotes", order.ExtraNotes), 
                                                        new SQLiteParameter("@IsAutomatic", order.IsAutomatic), 
                                                    });
                    var result = command.ExecuteNonQuery();

                    connection.Close();

                    return result == 1;
                }
            }
            catch (Exception exception)
            {
                _logger.ErrorException(exception.Message, exception);
                if (exception.InnerException != null)
                    _logger.ErrorException(exception.InnerException.Message, exception.InnerException);

                return false;
            }
        }

        public bool InsertJournalOrder(Orders order, DateTime publishDate, DateTime? payableDate)
        {
            try
            {
                var journal = _unitOfWork.Context.JournalsSet.FirstOrDefault(
                    x => x.PublishDate == publishDate);

                if(journal == null)
                {
                    journal = new Journals
                    {
                        PublishDate = publishDate,
                        IsFix = true,
                        Title = string.Format("{0:00}", publishDate.Month)
                    };

                    _unitOfWork.Context.JournalsSet.AddObject(journal);
                }

                journal.JournalOrders.Add(new JournalOrders
                                              {
                                                  IsPrepay = order.IsPrepay,
                                                  IsSetInvoce = true,
                                                  IsPayable = payableDate != null,
                                                  Amount = payableDate != null ? order.PriceBrutto : 0,
                                                  CustomerId = order.CustomerId,
                                                  Notes = order.Notes,
                                                  Price = order.Price,
                                                  Period = order.Period,
                                                  Size = order.Size,
                                                  RealOrderId = order.Id,
                                                  Prepay = order.Prepay,
                                                  Description = order.Description,
                                                  PriceBrutto = order.PriceBrutto,
                                                  PublishDate = publishDate,
                                                  PayableDate = payableDate,
                                                  StartMonth = order.StartMonth,
                                                  StartYear = order.StartYear,
                                                  EndMonth = order.EndMonth,
                                                  EndYear = order.EndYear,
                                                  ExtraNotes = order.ExtraNotes,
                                                  InvoiceDate = publishDate
                                              });

                // debit
                _unitOfWork.Context.PaymentsSet.AddObject(new Payments
                                                              {
                                                                  CustomerId = order.CustomerId,
                                                                  Amount = - order.PriceBrutto,
                                                                  PayDate = publishDate,
                                                                  OrderId = order.Id
                                                              });

                // credit
                if (payableDate != null)
                    _unitOfWork.Context.PaymentsSet.AddObject(new Payments
                                                                  {
                                                                      CustomerId = order.CustomerId,
                                                                      Amount = order.PriceBrutto,
                                                                      PayDate = (DateTime) payableDate,
                                                                      OrderId = order.Id
                                                                  });
                return _unitOfWork.Commit();

            }
            catch (Exception exception)
            {
                _logger.ErrorException(exception.Message, exception);
                if (exception.InnerException != null)
                    _logger.ErrorException(exception.InnerException.Message, exception.InnerException);

                return false;
            }
        }

        public bool SaveChanges()
        {
            return _unitOfWork.Commit();
        }
    }
}
