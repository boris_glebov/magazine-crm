﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hqub.MagazineCrm.Database;

namespace Hqub.MagazineCrm.Service.Implementation
{
    public class CustomerService : Interface.ICustomerService
    {
        private readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();

        public IEnumerable<Customers> List()
        {
            try
            {
                return _unitOfWork.Context.CustomersSet;
            }
            catch (Exception exception)
            {
               _logger.ErrorException(exception.Message, exception);
                return new List<Customers>();
            }
        }

        public Customers Get(long id)
        {
            try
            {
                return _unitOfWork.Context.CustomersSet.Where(x => x.Id == id).FirstOrDefault();
            }
            catch (Exception exception)
            {
                _logger.ErrorException(exception.Message, exception);
                return null;
            }
        }

        public IEnumerable<Customers> Get(string partName)
        {
            throw new NotImplementedException();
        }

        public bool Add(Customers customer)
        {
            _unitOfWork.Context.CustomersSet.AddObject(customer);
            return _unitOfWork.Commit();
        }

        public bool Exists(int id)
        {
           return _unitOfWork.Context.CustomersSet.Any(x => x.Id == id);
        }

        public bool Exists(string name)
        {
            return _unitOfWork.Context.CustomersSet.Any(x => x.Name == name);
        }

        public bool SaveChanges()
        {
            return _unitOfWork.Commit();
        }
    }
}
