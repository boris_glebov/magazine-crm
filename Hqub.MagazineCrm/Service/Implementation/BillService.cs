﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hqub.MagazineCrm.Database;
using Hqub.MagazineCrm.Service.Interface;

namespace Hqub.MagazineCrm.Service.Implementation
{
    public class BillService : Interface.IBillService
    {
        private readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();

        public bool Add(Bills bill)
        {
            _unitOfWork.Context.BillsSet.AddObject(bill);
            return SaveChanges();
        }

        public List<Bills> GetBills(bool onlyUnpaid)
        {
            return onlyUnpaid
                       ? _unitOfWork.Context.BillsSet.Where(x => !x.IsPayable).ToList()
                       : _unitOfWork.Context.BillsSet.ToList();
        }

        public bool SaveChanges()
        {
            return _unitOfWork.Commit();
        }
    }
}
