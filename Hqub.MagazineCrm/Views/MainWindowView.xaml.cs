﻿using System;
using System.Threading;
using System.Windows;
using Hqub.MagazineCrm.ViewModel;

namespace Hqub.MagazineCrm.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindowView : Window
    {
        private readonly MainWindowViewModel _mainWindowViewModel;

        public MainWindowView()
        {
            InitializeComponent();

            _mainWindowViewModel = new MainWindowViewModel();
            DataContext = _mainWindowViewModel;

            Core.GuiManager.RegisterMainContent(MainContent);
        }
    }
}
