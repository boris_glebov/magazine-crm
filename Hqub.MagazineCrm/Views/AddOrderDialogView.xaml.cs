﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Hqub.MagazineCrm.ViewModel;

namespace Hqub.MagazineCrm.Views
{
    /// <summary>
    /// Interaction logic for AddOrderDialogView.xaml
    /// </summary>
    public partial class AddOrderDialogView : Window
    {
        private readonly AddOrderDialogViewModel _addOrderDialogViewModel;
        public AddOrderDialogView(Database.Customers customerEntity, AddOrderDialogViewModel.State state = AddOrderDialogViewModel.State.Create)
        {
            InitializeComponent();
            
            _addOrderDialogViewModel = new AddOrderDialogViewModel(customerEntity, state) {Close = Close, DialogResult = DialogResult};
            DataContext = _addOrderDialogViewModel;

            SetPositionSettings();
        }

        public AddOrderDialogView(Database.Customers customerEntity, Database.Orders orderEntity, AddOrderDialogViewModel.State state = AddOrderDialogViewModel.State.CreateWithoutSave)
        {
            InitializeComponent();

            _addOrderDialogViewModel = new AddOrderDialogViewModel(customerEntity, orderEntity, state) { Close = Close, DialogResult = DialogResult };
            DataContext = _addOrderDialogViewModel;

            SetPositionSettings();
        }

        public AddOrderDialogView(Database.Orders orderEntity)
        {
            InitializeComponent();

            _addOrderDialogViewModel = new AddOrderDialogViewModel(orderEntity) { Close = Close, DialogResult = DialogResult};
            DataContext = _addOrderDialogViewModel;

            SetPositionSettings();
        }

        public AddOrderDialogView(Database.JournalOrders journalOrder)
        {
            InitializeComponent();

            _addOrderDialogViewModel = new AddOrderDialogViewModel(journalOrder) { Close = Close, DialogResult = DialogResult };
            DataContext = _addOrderDialogViewModel;

            SetPositionSettings();
        }

        private void SetPositionSettings()
        {
            var settings = Properties.Settings.Default;

            Closing += (sender, args) =>
            {
                settings.AddOrderDialogViewHeight = Height;
                settings.AddOrderDialogViewWidth = Width;
                settings.AddOrderDialogViewTop = Top;
                settings.AddOrderDialogViewLeft = Left;

                settings.Save();
            };


            // ReSharper disable CompareOfFloatsByEqualityOperator

            if (settings.AddOrderDialogViewHeight == 0 ||
                settings.AddOrderDialogViewLeft == 0 ||
                settings.AddOrderDialogViewTop == 0 ||
                settings.AddOrderDialogViewWidth == 0)
                return;

            // ReSharper restore CompareOfFloatsByEqualityOperator

            Height = settings.AddOrderDialogViewHeight;
            Width = settings.AddOrderDialogViewWidth;
            Top = settings.AddOrderDialogViewTop;
            Left = settings.AddOrderDialogViewLeft;
        }
    }
}
