﻿using System.Windows;
using Hqub.MagazineCrm.ViewModel;

namespace Hqub.MagazineCrm.Views
{
    /// <summary>
    /// Interaction logic for AddUserDialogView.xaml
    /// </summary>
    public partial class AddUserDialogView : Window
    {
        private readonly AddUserDialogViewModel _addUserDialogViewModel;
        public AddUserDialogView()
        {
            InitializeComponent();

            _addUserDialogViewModel = new AddUserDialogViewModel {Close = Close};

            DataContext = _addUserDialogViewModel;
            SetPositionSettings();
        }

        public AddUserDialogView(Database.Customers cutomerEntity, Service.Interface.ICustomerService customerService)
        {
            InitializeComponent();

            _addUserDialogViewModel = new AddUserDialogViewModel(cutomerEntity, customerService)
                                          {Close = Close};
            DataContext = _addUserDialogViewModel;
            SetPositionSettings();
        }

        public bool? Result
        {
            get { return _addUserDialogViewModel.DialogResult; }
        }

        private void SetPositionSettings()
        {
            var settings = Properties.Settings.Default;

            Closing += (sender, args) =>
            {
                settings.AddUserDialogViewHeight = Height;
                settings.AddUserDialogViewWidth = Width;
                settings.AddUserDialogViewTop = Top;
                settings.AddUserDialogViewLeft = Left;

                settings.Save();
            };


            // ReSharper disable CompareOfFloatsByEqualityOperator

            if (settings.AddUserDialogViewHeight == 0 ||
                settings.AddUserDialogViewLeft == 0 ||
                settings.AddUserDialogViewTop == 0 ||
                settings.AddUserDialogViewWidth == 0)
                return;

            // ReSharper restore CompareOfFloatsByEqualityOperator

            Height = settings.AddUserDialogViewHeight;
            Width = settings.AddUserDialogViewWidth;
            Top = settings.AddUserDialogViewTop;
            Left = settings.AddUserDialogViewLeft;
        }
    }
}
