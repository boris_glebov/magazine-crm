﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Hqub.MagazineCrm.ViewModel;

namespace Hqub.MagazineCrm.Views
{
    /// <summary>
    /// Interaction logic for OrdersDialogView.xaml
    /// </summary>
    public partial class OrdersDialogView : Window
    {
        private readonly ViewModel.OrdersDialogViewModel _ordersDialogViewModel;
        public OrdersDialogView()
        {
            InitializeComponent();
            SetPositionSettings();

            _ordersDialogViewModel = new OrdersDialogViewModel {Close = Close};

            DataContext = _ordersDialogViewModel;
        }

        public IEnumerable<Database.Orders> SelectOrders()
        {
            ShowDialog();
            return _ordersDialogViewModel.SelectedOrders;
        }

        private void SetPositionSettings()
        {
            var settings = Properties.Settings.Default;

            Closing += (sender, args) =>
            {
                settings.OrdersDialogViewHeight = Height;
                settings.OrdersDialogViewWidth = Width;
                settings.OrdersDialogViewTop = Top;
                settings.OrdersDialogViewLeft = Left;

                settings.Save();
            };


            // ReSharper disable CompareOfFloatsByEqualityOperator

            if (settings.OrdersDialogViewHeight == 0 ||
                settings.OrdersDialogViewLeft == 0 ||
                settings.OrdersDialogViewTop == 0 ||
                settings.OrdersDialogViewWidth == 0)
                return;

            // ReSharper restore CompareOfFloatsByEqualityOperator

            Height = settings.OrdersDialogViewHeight;
            Width = settings.OrdersDialogViewWidth;
            Top = settings.OrdersDialogViewTop;
            Left = settings.OrdersDialogViewLeft;


        }

        private void DataGrid_Sorting(object sender, DataGridSortingEventArgs e)
        {
            e.Handled = true;
            var column = e.Column;

            ListSortDirection direction = (column.SortDirection != ListSortDirection.Ascending) ?
                                ListSortDirection.Ascending : ListSortDirection.Descending;
            column.SortDirection = direction;
            var lcv = (ListCollectionView)CollectionViewSource.GetDefaultView(Grid.ItemsSource);
            var mySort = new Database.CustomSort(direction, column);
            lcv.CustomSort = mySort;  // provide our own sort
        }
    }
}
