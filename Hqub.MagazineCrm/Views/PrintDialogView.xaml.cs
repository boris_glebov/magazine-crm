﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Hqub.MagazineCrm.ViewModel;

namespace Hqub.MagazineCrm.Views
{
    /// <summary>
    /// Interaction logic for PrintDialogView.xaml
    /// </summary>
    public partial class PrintDialogView : Window
    {
        private ViewModel.PrintDialogViewModel _printDialogViewModel;

        public PrintDialogView(bool isPdf=true)
        {
            InitializeComponent();

            _printDialogViewModel = new PrintDialogViewModel(isPdf)
                                        {
                                            Close = Close
                                        };

            DataContext = _printDialogViewModel;
            SetPositionSettings();
        }

        private void SetPositionSettings()
        {
            var settings = Properties.Settings.Default;

            Closing += (sender, args) =>
            {
                settings.PrintDialogViewHeight = Height;
                settings.PrintDialogViewWidth = Width;
                settings.PrintDialogViewTop = Top;
                settings.PrintDialogViewLeft = Left;

                settings.Save();
            };


// ReSharper disable CompareOfFloatsByEqualityOperator

            if (settings.PrintDialogViewHeight == 0 ||
                settings.PrintDialogViewLeft == 0 ||
                settings.PrintDialogViewTop == 0 ||
                settings.PrintDialogViewWidth == 0)
                return;

// ReSharper restore CompareOfFloatsByEqualityOperator

            Height = settings.PrintDialogViewHeight;
            Width = settings.PrintDialogViewWidth;
            Top = settings.PrintDialogViewTop;
            Left = settings.PrintDialogViewLeft;
            

        }
    }
}
