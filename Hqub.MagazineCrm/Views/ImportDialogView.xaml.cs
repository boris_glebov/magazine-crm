﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Hqub.MagazineCrm.ViewModel;

namespace Hqub.MagazineCrm.Views
{
    /// <summary>
    /// Interaction logic for ImportView.xaml
    /// </summary>
    public partial class ImportView : Window
    {
        private readonly ImportDialogViewModel _importDialogViewModel;

        public ImportView()
        {
            InitializeComponent();

            _importDialogViewModel = new ImportDialogViewModel {Close = Close, Dispatcher=Dispatcher, MainGrid = mainGrid};
            DataContext = _importDialogViewModel;
        }
    }
}
