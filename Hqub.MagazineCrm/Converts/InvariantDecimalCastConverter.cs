﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;
using Hqub.MagazineCrm.Core;

namespace Hqub.MagazineCrm.Converts
{
    public class InvariantDecimalCastConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Utils.GetDecimal(value.ToString());
        }
    }
}
