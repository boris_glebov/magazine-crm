﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace Hqub.MagazineCrm.Converts
{
    public class BinaryToImageConvert : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var imageArr = (byte[])value;

            try
            {
                var bi = new BitmapImage();
                bi.BeginInit();
                bi.CreateOptions = BitmapCreateOptions.None;
                bi.CacheOption = BitmapCacheOption.Default;
                bi.StreamSource = new MemoryStream(imageArr);
                bi.EndInit();

                return bi;
            }
            catch (NotSupportedException)
            {
                var bi = new BitmapImage();

                bi.BeginInit();
                bi.UriSource = new Uri("pack://application:,,,/Hqub.MagazineCrm;component/Assets/binary.png");
                bi.EndInit();

                return bi;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
