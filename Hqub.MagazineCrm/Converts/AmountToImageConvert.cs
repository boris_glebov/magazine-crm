﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace Hqub.MagazineCrm.Converts
{
    public class AmountToImageConvert : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var amount = (decimal) value;

            var source = amount >= 0 ? "pack://application:,,,/Hqub.MagazineCrm;component/Assets/add.png" :
                                     "pack://application:,,,/Hqub.MagazineCrm;component/Assets/remove.png";

            var bi = new BitmapImage();

            bi.BeginInit();
            bi.UriSource = new Uri(source);
            bi.EndInit();

            return bi;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
