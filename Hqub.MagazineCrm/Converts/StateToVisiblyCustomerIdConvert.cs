﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using Hqub.MagazineCrm.ViewModel;

namespace Hqub.MagazineCrm.Converts
{
    public class StateToVisiblyCustomerIdConvert : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var state = (AddUserDialogViewModel.State) value;

            return state == AddUserDialogViewModel.State.Create ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
