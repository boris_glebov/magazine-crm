﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using Hqub.MagazineCrm.ViewModel;

namespace Hqub.MagazineCrm.Converts
{
    public class OrderStateToVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var state = (AddOrderDialogViewModel.State) value;

            return state == AddOrderDialogViewModel.State.Create ||
                   state == AddOrderDialogViewModel.State.CreateWithoutSave
                       ? Visibility.Collapsed
                       : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
