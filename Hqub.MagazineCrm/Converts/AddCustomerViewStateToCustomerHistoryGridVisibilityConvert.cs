﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;

namespace Hqub.MagazineCrm.Converts
{
    public class AddCustomerViewStateToCustomerHistoryGridVisibilityConvert : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var state = (ViewModel.AddUserDialogViewModel.State) value;

            return state != ViewModel.AddUserDialogViewModel.State.Create ? Visibility.Visible : Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
