﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace Hqub.MagazineCrm.Converts
{
    public class IsFixJournalToImageConvert : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var isFix = (bool)value;

            var source = isFix  ? "pack://application:,,,/Hqub.MagazineCrm;component/Assets/lock.png" :
                                     "pack://application:,,,/Hqub.MagazineCrm;component/Assets/newspaper.png";

            var bi = new BitmapImage();

            bi.BeginInit();
            bi.UriSource = new Uri(source);
            bi.EndInit();

            return bi;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
