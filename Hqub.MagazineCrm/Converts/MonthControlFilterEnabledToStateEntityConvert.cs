﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;
using Hqub.MagazineCrm.Controls.ViewModel;

namespace Hqub.MagazineCrm.Converts
{
    public class MonthControlFilterEnabledToStateEntityConvert : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var convertValue = (MonthControlViewModel.State) value;

            return convertValue != MonthControlViewModel.State.Fix;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
