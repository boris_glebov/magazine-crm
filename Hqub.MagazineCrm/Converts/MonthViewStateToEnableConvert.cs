﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using Hqub.MagazineCrm.Controls.ViewModel;

namespace Hqub.MagazineCrm.Converts
{
    public class MonthViewStateToEnableConvert : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var state = (MonthControlViewModel.State) value;
            return (state != MonthControlViewModel.State.Fix && state != MonthControlViewModel.State.Compare);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
