﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;
using Hqub.MagazineCrm.ViewModel;

namespace Hqub.MagazineCrm.Converts
{
    public class StateOrderViewToEnabledConvert : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var viewModel = (AddOrderDialogViewModel) value;
            var state = viewModel.StateEntity;

            return state == AddOrderDialogViewModel.State.Create ||
                   state == AddOrderDialogViewModel.State.CreateWithoutSave ||
                   viewModel.OrderEntity.IsAutomatic == true;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
