﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace Hqub.MagazineCrm.Converts
{
    public class FilterTextToBorderBrushConvert : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var convertValue = (string) value;
            return string.IsNullOrEmpty(convertValue)
                       ? System.Windows.Media.Brushes.Blue
                       : System.Windows.Media.Brushes.Green;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
