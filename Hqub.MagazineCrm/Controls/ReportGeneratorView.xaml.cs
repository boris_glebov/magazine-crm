﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Hqub.MagazineCrm.Controls.ViewModel;

namespace Hqub.MagazineCrm.Controls
{
    /// <summary>
    /// Interaction logic for ReportGeneratorView.xaml
    /// </summary>
    public partial class ReportGeneratorView : UserControl
    {
        private readonly ReportGeneratorViewModel _reportGeneratorViewModel;

        public ReportGeneratorView()
        {
            InitializeComponent();

            _reportGeneratorViewModel = new ReportGeneratorViewModel()
                                            {
                                                Hash = reportTabControl.GetHashCode(),
                                                Dispatcher = Dispatcher
                                            };

            DataContext = _reportGeneratorViewModel;

            Core.GuiManager.RegisterTabControl(Core.Constants.ReportTabControl, reportTabControl);
        }
    }
}
