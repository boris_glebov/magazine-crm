﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GalaSoft.MvvmLight.Command;

namespace Hqub.MagazineCrm.Controls
{
    /// <summary>
    /// Interaction logic for AttachmentControlView.xaml
    /// </summary>
    public partial class AttachmentControlView : UserControl
    {
        public static readonly DependencyProperty LayoutEntityProperty = DependencyProperty.Register("LayoutEntity",
                                                                                              typeof (Database.Layouts),
                                                                                              typeof (AttachmentControlView));

        public static readonly DependencyProperty JournalLayoutEntityProperty = DependencyProperty.Register("JournalLayouts",
                                                                                             typeof(Database.JournalLayouts),
                                                                                             typeof(AttachmentControlView));
        private readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        public AttachmentControlView()
        {
            InitializeComponent();
        }

        public Database.Layouts LayoutEntity
        {
            get { return (Database.Layouts) GetValue(LayoutEntityProperty); }
            set
            {
                SetValue(LayoutEntityProperty, value);
            }
        }

        public Database.JournalLayouts JournalLayouts
        {
            get { return (Database.JournalLayouts) GetValue(JournalLayoutEntityProperty); }
            set
            {
                SetValue(JournalLayoutEntityProperty, value);
            }
        }

        private void UserControl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                //It is Duck :)
                dynamic layout;

                if(LayoutEntity == null)
                    layout = JournalLayouts;
                else
                    layout = LayoutEntity;


                var tempDirectoryName = Core.Configure.Settings.Instance.Paths.TempPath;
                if (!System.IO.Directory.Exists(tempDirectoryName))
                    System.IO.Directory.CreateDirectory(tempDirectoryName);


                var path = string.Format("{0}{1}{2}", tempDirectoryName,
                                         Guid.NewGuid().ToString(), layout.Extension);

                var file = System.IO.File.Create(path, layout.Data.Length);
                file.Write(layout.Data, 0, layout.Data.Length);

                file.Close();

                Process.Start(path);
            }
            catch (Exception ex)
            {
                _logger.ErrorException(ex.Message, ex);
                
                if(ex.InnerException != null)
                {
                    _logger.ErrorException(ex.InnerException.Message, ex.InnerException);
                }
            }
        }
    }
}
