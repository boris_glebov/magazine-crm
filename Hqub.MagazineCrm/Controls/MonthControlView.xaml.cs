﻿using System.ComponentModel;
using System.Windows.Data;
using Hqub.MagazineCrm.Controls.ViewModel;

namespace Hqub.MagazineCrm.Controls
{
    /// <summary>
    /// Interaction logic for MonthControlView.xaml
    /// </summary>
    public partial class MonthControlView
    {
        private readonly MonthControlViewModel _monthControlViewModel;

        public MonthControlView()
        {
            InitializeComponent();

            _monthControlViewModel = new MonthControlViewModel
                                         {
                                             DispatcherInstance = Dispatcher
                                         };

            DataContext = _monthControlViewModel;
        }

        public MonthControlView(Database.Journals journal)
        {
            InitializeComponent();

            _monthControlViewModel = new MonthControlViewModel(journal)
            {
                DispatcherInstance = Dispatcher
            };

            DataContext = _monthControlViewModel;
        }

        public void Release()
        {
            _monthControlViewModel.Release();
        }

        private void DataGrid_Sorting(object sender, System.Windows.Controls.DataGridSortingEventArgs e)
        {
            e.Handled = true;
            var column = e.Column;
            
            ListSortDirection direction = (column.SortDirection != ListSortDirection.Ascending) ?
                                ListSortDirection.Ascending : ListSortDirection.Descending;
            column.SortDirection = direction;
            var lcv = (ListCollectionView)CollectionViewSource.GetDefaultView(Grid.ItemsSource);
            var mySort = new Database.CustomSort(direction, column);
            lcv.CustomSort = mySort;  // provide our own sort
        }
    }
}
