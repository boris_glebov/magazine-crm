﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Hqub.MagazineCrm.Controls.ViewModel;

namespace Hqub.MagazineCrm.Controls.Reports
{
    /// <summary>
    /// Interaction logic for OrderReportsView.xaml
    /// </summary>
    public partial class OrderReportsView : UserControl
    {
        private readonly Controls.Reports.OrderReportsViewModel _orderReportsViewModel;

        public OrderReportsView(IEnumerable<Database.JournalOrders> orders )
        {
            InitializeComponent();

            _orderReportsViewModel = new OrderReportsViewModel(orders)
                                         {
                                             Dispatcher = Dispatcher
                                         };
            DataContext = _orderReportsViewModel;
        }
    }
}
