﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight.Command;
using Hqub.MagazineCrm.Database;

namespace Hqub.MagazineCrm.Controls.Reports
{
    public class OrderReportsViewModel : GalaSoft.MvvmLight.ViewModelBase
    {
        private readonly Service.Interface.IOrderService _orderService = NinjectBootstrap.GetOrderService(false, false);
        private readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        public OrderReportsViewModel(IEnumerable<JournalOrders> orders)
        {
            _orders = new List<JournalOrders>(orders);
            UpdateOrdersView(_orders);
        }

        private void UpdateOrdersView(List<JournalOrders> orders)
        {
            OrdersCollectionView = new ListCollectionView(orders);

            TotalPrice = orders.Sum(x => x.Price);
            TotalPriceBrutto = orders.Sum(x => x.PriceBrutto);

            if (OrdersCollectionView.GroupDescriptions != null)
                OrdersCollectionView.GroupDescriptions.Add(new PropertyGroupDescription("GroupEntity"));
        }

        #region Commands

        public ICommand SaveCommand { get { return new RelayCommand(SaveCommandExecute); } }
        private void SaveCommandExecute()
        {
            SaveReport();
        }

        public ICommand PrintCommand {get{return new RelayCommand(PrintCommandExecute);}}
        private void PrintCommandExecute()
        {
            try
            {
                var printerDialog = new Views.PrintDialogView(false);
                printerDialog.ShowDialog();

                PreparePrint();
                var fileName = string.Format("{0}temp\\report_{1}.docx", AppDomain.CurrentDomain.BaseDirectory,
                                             Guid.NewGuid());
                SaveReport(fileName);

                dynamic app = new Microsoft.Office.Interop.Word.Application
                                  {
                                      Visible = false
                                  };

                app.Documents.Open(fileName);
                app.ActivePrinter = Core.Configure.Settings.Instance.ActivePrinter;
                app.ActiveDocument.PrintOut();

                app.Quit();
            }
            catch (Exception ex)
            {
                _logger.ErrorException(ex.Message, ex);
            }
        }

        private void PreparePrint()
        {
            var path = AppDomain.CurrentDomain.BaseDirectory + "temp";

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
        }

        private void SaveReport(string fileName = "")
        {
            var orderReportTemplatePath = AppDomain.CurrentDomain.BaseDirectory + "Templates\\ReportOrders.docx";
            if(!File.Exists(orderReportTemplatePath))
            {
                _logger.ErrorException(string.Format("File '{0}' not found.", orderReportTemplatePath),
                                       new FileNotFoundException("File not found.", orderReportTemplatePath));
                return;
            }

            var path = fileName;
            if (string.IsNullOrEmpty(path))
            {
                var dlg = new Microsoft.Win32.SaveFileDialog();
                dlg.Filter = "MS Word (*.docx) | *.docx";
                if (dlg.ShowDialog() != true)
                    return;

                path = dlg.FileName;
            }

            var ds = new DataSet();
            //Generate table of orders:
            var table = new DataTable("Orders");
            table.Columns.AddRange(new[]
                                       {
                                           new DataColumn("Number"),
                                           new DataColumn("Description"),
                                           new DataColumn("PriceNetto"),
                                           new DataColumn("PriceBrutto"),
                                           new DataColumn("Total")
                                       });

            var number = 1;
            foreach (var journalOrder in _orders)
            {
                table.Rows.Add(number, journalOrder.Description, journalOrder.Price, journalOrder.PriceBrutto,
                               journalOrder.PriceBrutto);
            }

            ds.Tables.Add(table);
            var dict = new Dictionary<string, string>
                           {
                               {"PriceTotal", TotalPriceBrutto.ToString(CultureInfo.CurrentCulture.NumberFormat)}
                           };

            try
            {
                File.WriteAllBytes(path, TRIS.FormFill.Lib.FormFiller.GetWordReport(orderReportTemplatePath, ds, dict));

            }catch(Exception ex)
            {
                _logger.ErrorException(ex.Message, ex);
            }
        }
        #endregion

        #region Properties

        public  Dispatcher Dispatcher{ get; set; }

        #endregion

        #region Binding Properties

        private readonly List<JournalOrders> _orders;
        private ListCollectionView _ordersCollectionView;

        public ListCollectionView OrdersCollectionView
        {
            get { return _ordersCollectionView; }
            set
            {
                _ordersCollectionView = value;
                RaisePropertyChanged("OrdersCollectionView");
            }
        }

        private decimal _totalPrice;

        public decimal TotalPrice
        {
            get { return _totalPrice; }
            set
            {
                _totalPrice = value;
                RaisePropertyChanged("TotalPrice");
            }
        }

        private decimal _totalPriceBrutto;

        public decimal TotalPriceBrutto
        {
            get { return _totalPriceBrutto; }
            set
            {
                _totalPriceBrutto = value;
                RaisePropertyChanged("TotalPriceBrutto");
            }
        }

        #endregion
    }
}
