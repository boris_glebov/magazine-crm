﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Hqub.MagazineCrm.Controls.ViewModel;

namespace Hqub.MagazineCrm.Controls
{
    /// <summary>
    /// Interaction logic for DateInputView.xaml
    /// </summary>
    public partial class DateInputView : Window
    {
        private readonly DateInputDialogViewModel _dateInputDialogViewModel;

        public DateInputView()
        {
            InitializeComponent();

            _dateInputDialogViewModel = new DateInputDialogViewModel();
            _dateInputDialogViewModel.Close = Close;

            DataContext = _dateInputDialogViewModel;
            SetPositionSettings();
        }

        public DateTime SelectDate
        {
            get { return _dateInputDialogViewModel.SelectedDate; }
        }

        private void SetPositionSettings()
        {
            var settings = Properties.Settings.Default;

            Closing += (sender, args) =>
            {
                settings.DateInputViewHeight = Height;
                settings.DateInputViewWidth = Width;
                settings.DateInputViewTop = Top;
                settings.DateInputViewLeft = Left;

                settings.Save();
            };


            // ReSharper disable CompareOfFloatsByEqualityOperator

            if (settings.DateInputViewHeight == 0 ||
                settings.DateInputViewLeft == 0 ||
                settings.DateInputViewTop == 0 ||
                settings.DateInputViewWidth == 0)
                return;

            // ReSharper restore CompareOfFloatsByEqualityOperator

            Height = settings.DateInputViewHeight;
            Width = settings.DateInputViewWidth;
            Top = settings.DateInputViewTop;
            Left = settings.DateInputViewLeft;
        }
    }
}
