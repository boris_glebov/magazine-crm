﻿using System;
using System.Globalization;
using System.Threading;
using System.Windows;
using Hqub.MagazineCrm.Controls.ViewModel;
using Infralution.Localization.Wpf;

namespace Hqub.MagazineCrm.Controls
{
    /// <summary>
    /// Interaction logic for InputDialog.xaml
    /// </summary>
    public partial class InputDialog : Window
    {
        private readonly ViewModel.InputDialogViewModel _inputDialogViewModel;


        public InputDialog(string title, string label)
        {
            InitializeComponent();

            _inputDialogViewModel = new InputDialogViewModel
                                        {
                                            Title = title,
                                            Label = label,
                                            Close = Close
                                        };

            DataContext = _inputDialogViewModel;
        }

        public decimal Value
        {
            get { return _inputDialogViewModel.Value; }
        }

        public bool Result { get { return _inputDialogViewModel.DialogResult; } }
 

        public DateTime SelectDate
        {
            get { return _inputDialogViewModel.SelectedDate; }
        }
    }
}
