﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Data.Objects.DataClasses;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight.Command;
using Hqub.MagazineCrm.Core;
using Hqub.MagazineCrm.Database;
using System.Windows.Data;
using Microsoft.Office.Interop.Word;
using DataTable = System.Data.DataTable;

namespace Hqub.MagazineCrm.Controls.ViewModel
{
    public class MonthControlViewModel : GalaSoft.MvvmLight.ViewModelBase
    {
        private readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        public enum State
        {
            Create,
            Modify,
            Fix,
            Compare
        }

        private const double CountDayInMonth = 30.0;
        private readonly Service.Interface.IJournalService _journalService = NinjectBootstrap.GetJournalService();
        private readonly Service.Interface.IPaymentService _paymentService  = NinjectBootstrap.GetPaymentService();
        private readonly Service.Interface.IBillService _billService =NinjectBootstrap.GetBillService();

        /// <summary>
        /// Generate new journal
        /// </summary>
        public MonthControlViewModel()
        {
            StateEntity = State.Create;

            JournalEntity = new Journals
                                {
                                    Title = string.Format("{0:00}", DateTime.Now.Month),
                                    PublishDate = DateTime.Now
                                };

            InitOrderCollection();
//            var bg = new BackgroundWorker();
//            bg.DoWork += (sender, args) => DispatcherInstance.Invoke(new Action(InitOrderCollection), DispatcherPriority.Background);
//
//            bg.RunWorkerAsync();

            //Subscribe on filter event:
            SubscribeOnFiltirngEvent();
        }

        /// <summary>
        /// Show selected journal
        /// </summary>
        public MonthControlViewModel(Journals journal)
        {
            StateEntity = journal.IsFix ? State.Fix : State.Modify;
            JournalEntity = journal;

            _journalOrders = new List<JournalOrders>(JournalEntity.JournalOrders);
            UpdateOrderView();

            //Subscribe on filter event:
            SubscribeOnFiltirngEvent();
        }


        #region Field

        private List<Orders> _orders = new List<Orders>();
        private List<JournalOrders> _journalOrders = new List<JournalOrders>();
        #endregion

        #region Methods

        public void Release()
        {
            if (StateEntity != State.Fix)
                SaveJournal();
        }

        /// <summary>
        /// Simple init oreder collection. Invoke AutoGenerateJournal
        /// </summary>
        private void InitOrderCollection()
        {
            var service = NinjectBootstrap.GetOrderService();
            
            _orders.Clear();
            _journalOrders.Clear();

            _orders.AddRange(AutoGenerateJournal(service.List()));

            foreach (var order in _orders)
            {
                _journalOrders.Add(OrderClone(order));
            }

            UpdateOrderView();
        }

        /// <summary>
        /// Update order presents
        /// </summary>
        private void UpdateOrderView()
        {
            Orders = new ListCollectionView(_journalOrders);
            if (Orders.GroupDescriptions != null)
                Orders.GroupDescriptions.Add(new PropertyGroupDescription("GroupEntity"));
        }

        /// <summary>
        /// Add orders in journal by rules
        /// </summary>
        private IEnumerable<Orders> AutoGenerateJournal(IEnumerable<Orders> orders)
        {
            var dateNow = DateTime.Now;
            var actualOrders = new List<Orders>();

            foreach (var order in orders)
            {
                //Если дата начала заказа больше текущей, значит заказ еще не началася:
                if(  order.StartYear > dateNow.Year || (order.StartYear == dateNow.Year && order.StartMonth > dateNow.Month))
                {
                    continue;
                }

                //Если дата окончания заказа меньше текущей даты, то не берем
                if(order.EndYear < dateNow.Year || (order.EndYear == dateNow.Year && order.EndMonth < dateNow.Month))
                {
                    continue;
                }

                if (order.CheckOrderPeriod(dateNow))
                    actualOrders.Add(order);
            }

            return actualOrders;
        }

        /// <summary>
        /// Handle event MouseLeftButtonDown. Close journal panel
        /// </summary>
        public ICommand CloseCommand
        {
            get { return new RelayCommand<MouseEventArgs>(CloseCommandExecute); }
        }

        private void CloseCommandExecute(MouseEventArgs obj)
        {
            var now = DateTime.Now;
            JournalManager.Instance.CloseJournal(now.Month, now.Year);

            GuiManager.RemoveActiveTab();
        }

        /// <summary>
        /// Reload orders
        /// </summary>
        public ICommand RefreshCommand
        {
            get { return new RelayCommand(RefreshCommandExecute); }
        }

        private void RefreshCommandExecute()
        {
            if (StateEntity == State.Fix || StateEntity == State.Compare)
                return;

            if (
                MessageBox.Show(Properties.Resources.ChangeDiscard, Properties.Resources.RefreshToolTip,
                                MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                return;

            switch (StateEntity)
            {
                case State.Create:
                    InitOrderCollection();
                    break;

                case State.Modify:
                    _journalOrders = new List<JournalOrders>(JournalEntity.JournalOrders);
                    UpdateOrderView();
                    break;
            }
        }

        private bool Filtering(object de)
        {
            var order = de as JournalOrders;

            var checkResult = true;

            if(!string.IsNullOrEmpty(FilterText))
                if(IsCaseSensitive)
                {
                    checkResult &= order.RealOrderId.ToString().Contains(FilterText) || order.Description.Contains(FilterText);
                }
                else
                {
                    checkResult &= order.RealOrderId.ToString().Contains(FilterText) || order.Description.ToLower().Contains(FilterText.ToLower());
                }

            switch (SelectedFilter)
            {
                case FilterEnum.NotInvoice:
                    checkResult &= !order.IsSetInvoce; 
                    break;
                case FilterEnum.NotPaid:
                    checkResult &= !order.IsPayable;
                    break;
            }

            return checkResult;
        }

        private JournalOrders OrderClone(Orders order)
        {
            var clone = new JournalOrders
            {
                Id = order.Id,
                RealOrderId = order.Id,
                GroupEntity = JournalOrders.CreateGroup(order.CustomerId, order.Customer.Name),
                Notes = order.Notes,
                Amount = order.Amount,
                CustomerId = order.CustomerId,
                Price = order.Price,
                IsPayable = order.IsPayable,
                IsSetInvoce = order.IsSetInvoce,
                IsPrepay = order.IsPrepay,
                Period = order.Period,
                Size = order.Size,
                StartYear = order.StartYear,
                StartMonth = order.StartMonth,
                EndMonth = order.EndMonth,
                EndYear = order.EndYear,
                PriceBrutto = order.PriceBrutto,
                Description = order.Description,
                ExtraNotes = order.ExtraNotes,
                JournalLayouts  = new EntityCollection<JournalLayouts>()
            };

            foreach (var layout in order.Layouts)
            {
                clone.JournalLayouts.Add(new JournalLayouts
                                             {
                                                 Id = layout.Id,
                                                 OrderId = layout.OrderId,
                                                 Data = layout.Data,
                                                 Title = layout.Title,
                                                 Extension = layout.Extension
                                             });
            }

            return clone;
        }

        private bool CustomValidate()
        {
            if(string.IsNullOrEmpty(JournalEntity.Title))
            {
                ErrorMessage = "Title not be empty";
                return false;
            }

            return true;
        }

        private void SubscribeOnFiltirngEvent()
        {
            GlobalFunction.OnFiltering += (text, filter) =>
                                              {
                                                  _isCaseSensitive = filter;
                                                  FilterText = text;
                                              };
        }

        /// <summary>
        /// Save journal 
        /// </summary>
        private bool SaveJournal(bool isFix = false)
        {
            if (!CustomValidate())
            {
                MessageBox.Show(ErrorMessage, Properties.Resources.Error,
                                MessageBoxButton.OK,
                                MessageBoxImage.Error);

                return false;
            }

            foreach (var journalOrder in _journalOrders.Where(journalOrder => !JournalEntity.JournalOrders.Any(x => x.Id == journalOrder.Id)))
            {
                JournalEntity.JournalOrders.Add(journalOrder);
            }

            JournalEntity.IsFix = isFix;

            switch (StateEntity)
            {
                case State.Create:
                    if (_journalService.Save(JournalEntity))
                    {
//                        MessageBox.Show(Properties.Resources.JournalSaveSuccess, Properties.Resources.Journals,
//                                        MessageBoxButton.OK,
//                                        MessageBoxImage.Information);

                        StateEntity = isFix ? State.Fix : State.Modify;
                    }
                    else
                    {
                        JournalEntity.JournalOrders.Clear();
                        MessageBox.Show("Inner exception");
                        return false;
                    }

                    break;

                case State.Modify:
                    if (_journalService.SaveChanges())
                    {
//                        MessageBox.Show(Properties.Resources.JournalSaveSuccess, Properties.Resources.Journals,
//                                        MessageBoxButton.OK,
//                                        MessageBoxImage.Information);

                        if(isFix)
                            StateEntity = State.Fix;
                    }
                    else
                    {
                        _logger.ErrorException("Journal don't save.", new Exception("Journal don't save."));
                        return false;
                    }
                    break;

                case State.Fix:

                    MessageBox.Show("Journal is fixed!");
                    return  false;
            }

            return true;
        }
        
        #endregion

        #region Commands

        public ICommand SearchTextKeyDownCommand
        {
            get { return new RelayCommand<KeyEventArgs>(SearchTextKeyDownExecute); }
        }

        private void SearchTextKeyDownExecute(KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                FilterText = string.Empty;
            }
        }

        public ICommand SearchTextChangedCommand { get { return new RelayCommand<TextChangedEventArgs>(SearchTextChangedCommandExecute); } }
        private void SearchTextChangedCommandExecute(TextChangedEventArgs args)
        {
            FilterText = ((TextBox)args.Source).Text;
        }

        /// <summary>
        /// Include order in journal
        /// </summary>
        public ICommand IncludeOrderCommand
        {
            get { return new RelayCommand(IncludeOrderCommandExecute); }
        }
        private void IncludeOrderCommandExecute()
        {
            if(StateEntity == State.Fix || StateEntity == State.Compare)
            {
                MessageBox.Show(Properties.Resources.JournalFixed, Properties.Resources.Error, MessageBoxButton.OK,
                                MessageBoxImage.Error);

                return;
            }

            var dialog = new Views.OrdersDialogView();
            var selectedOrders = dialog.SelectOrders();

            foreach (var selectedOrder in selectedOrders)
            {
                if(_journalOrders.Any(o=>o.Id == selectedOrder.Id))
                    continue;

                _journalOrders.Add(OrderClone(selectedOrder));
            }
        
            UpdateOrderView();
        }

        public ICommand ClearFilterCommand
        {
            get { return new RelayCommand(ClearFilterCommandExecute); }
        }

        private void ClearFilterCommandExecute()
        {
            FilterText = string.Empty;
            SelectedFilter = FilterEnum.None;
        }

        /// <summary>
        /// Exclude order from journal
        /// </summary>
        public ICommand ExcludeOrderCommand
        {
            get { return new RelayCommand(ExcludeOrderCommandExecute); }
        }

        private void ExcludeOrderCommandExecute()
        {
            if (StateEntity == State.Fix || StateEntity == State.Compare)
            {
                MessageBox.Show(Properties.Resources.JournalFixed, Properties.Resources.Error, MessageBoxButton.OK,
                                MessageBoxImage.Error);

                return;
            }

            if (SelectedOrder == null)
                return;

            _journalOrders.Remove(SelectedOrder);
            SelectedOrder = null;

            UpdateOrderView();
        }

        /// <summary>
        /// Save journal
        /// </summary>
        public ICommand SaveCommand{get{return new RelayCommand(SaveCommandExecute);}}
        private void SaveCommandExecute()
        {
            SaveJournal();
        }

        /// <summary>
        /// Fix journal
        /// </summary>
        public ICommand FixCommand
        {
            get { return new RelayCommand(FixCommandExecute); }
        }

        private void FixCommandExecute()
        {
            if (
                MessageBox.Show(Properties.Resources.FixJournalConfirm, Properties.Resources.Warning,
                                MessageBoxButton.OKCancel, MessageBoxImage.Warning) == MessageBoxResult.Cancel)
                return;

            if (SaveJournal(true))
                SetInvoice(true);
        }

        /// <summary>
        /// Show order info (readonly)
        /// </summary>
        public ICommand OrderViewCommand{get{return new RelayCommand(OrderViewCommandExecute);}}
        private void OrderViewCommandExecute()
        {
            if(SelectedOrder == null)
            {
                return;
            }

            var dialog = new Views.AddOrderDialogView(SelectedOrder);
            dialog.ShowDialog();
        }

        /// <summary>
        /// Invoice for payment to the client 
        /// </summary>
        public ICommand SetInvoiceCommand{get{return new RelayCommand(SetInvoiceCommandExecute);}}
        private void SetInvoiceCommandExecute()
        {
            if(Core.Configure.Settings.Instance.AutoPrint)
            {
                var printerDialog = new Views.PrintDialogView(false);
                printerDialog.ShowDialog();
            }

            SetInvoice();
        }

        private void SetInvoice(bool forAll=false)
        {
            if (_journal.Id == 0)
            {
                SaveJournal();
            }

            DateTime invoiceDate;

            var prepareOrders = new List<JournalOrders>();

            var anotherOrders = _journalOrders.Where(o => !o.IsSetInvoce).ToList();

            if (!forAll)
            {
                if (SelectedOrder == null)
                {
                    return;
                }

                var orders = _journalOrders.Where(x => x.CustomerId == SelectedOrder.CustomerId).ToList();
    
                if (SelectedOrder.IsSetInvoce)
                {
                     prepareOrders.Add(SelectedOrder);
                    CreateBill(SelectedOrder.CustomerId, prepareOrders);
                    return;
                }

                if (orders.Count() > 1 && MessageBox.Show(Properties.Resources.OrderInclude, "", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    prepareOrders.AddRange(orders);
                }else
                {
                    prepareOrders.Add(SelectedOrder);
                }

                var dialog = new DateInputView();
                dialog.ShowDialog();
                invoiceDate = dialog.SelectDate;
            }
            else 
            {
                prepareOrders.AddRange(anotherOrders);
                invoiceDate = _journal.PublishDate;
            }

            foreach (var journalOrder in prepareOrders)
            {
                //provide payment
                _paymentService.Add(new Payments
                {
                    CustomerId = journalOrder.CustomerId,
                    Amount = -journalOrder.PriceBrutto,
                    PayDate = JournalEntity.PublishDate
                });
            }
          
            if (_paymentService.SaveChanges())
            {
                //Grouping
                var groupByUser = from x in prepareOrders
                                  group x by x.CustomerId
                                  into g
                                  select new
                                             {
                                                 CustomerId = g.Key,
                                                 Orders = g
                                             };

                var customerService = NinjectBootstrap.GetCustomerService(false, false);
                //Generate bill
                foreach (var user in groupByUser)
                {
                    //Check balance. If positive then continue:
                    var customer = customerService.Get(user.CustomerId);
                    if(customer == null || customer.RecalBalance() > 0)
                        continue;
                    
                    //Generate bill
                    CreateBill(user.CustomerId, user.Orders);
                }

                //Set invoice and date:
                foreach (var prepareOrder in prepareOrders)
                {
                    prepareOrder.IsSetInvoce = true;
                    prepareOrder.InvoiceDate = invoiceDate;
                }

                _journalService.SaveChanges();
            }
        }

        private void CreateBill(long customerId, IEnumerable<JournalOrders> journalOrders)
        {
            PrepareCreateBill();

            var customer = NinjectBootstrap.GetCustomerService().Get(customerId);

            var billTemplatePath = Core.Configure.Settings.Instance.Paths.TemplatesPath + "Bill.docx";
            var billWordFileName = string.Format("Bill_id{0}.docx", customerId);
            var billPdfFileName = string.Format("Bill_id{0}.pdf", customerId);

            var billPath = string.Format("{0}{1}\\", Core.Configure.Settings.Instance.Paths.JournalsPath,
                                         string.Format("{0}.{1}", JournalEntity.PublishDate.Month,
                                                       JournalEntity.PublishDate.Year));

            var billWordPath = string.Format("{0}{1}", billPath, billWordFileName);

            if (!File.Exists(billTemplatePath))
                throw new FileNotFoundException(string.Format("Template '{0}' not found.", billTemplatePath));


            //Generate table of orders:
            var table = new DataTable("Orders");
            table.Columns.AddRange(new[]
                                       {
                                           new DataColumn("Number"),
                                           new DataColumn("Description"),
                                           new DataColumn("PriceNetto"),
                                           new DataColumn("PriceBrutto"),
                                           new DataColumn("Total")
                                       });

            var invoiceNum = new StringBuilder();
            var number = 1; //Anzahl
            decimal totalPrice = 0;
            //Fill table
            foreach (var journalOrder in journalOrders)
            {
                table.Rows.Add(number, journalOrder.Description, 
                                Utils.DecimalToString(journalOrder.Price),
                                Utils.DecimalToString(journalOrder.PriceBrutto),
                                Utils.DecimalToString(journalOrder.PriceBrutto));

                invoiceNum.AppendFormat("{0}-", journalOrder.RealOrderId);

                totalPrice += journalOrder.PriceBrutto;
            }

            invoiceNum = invoiceNum.Remove(invoiceNum.Length - 1, 1);

            var dict = new Dictionary<string, string>
                           {
                               {"CompanyName", customer.Name},
                               {"Street", customer.Street},
                               {"House", customer.House},
                               {"City", customer.City},
                               {"Zip", string.Format("{0}", customer.Zip)},
                               {"PublishDate", Utils.GetStringDate(_journal.PublishDate)},
                               {"InvoiceNum", invoiceNum.ToString()},
                               {"Month", JournalEntity.PublishDate.Month.ToString()},
                               {"Year", JournalEntity.PublishDate.Year.ToString()}
                           };


            //Add bill to database:
            _billService.Add(new Bills
                                 {
                                     IsPayable = false,
                                     PublishDate = _journal.PublishDate,
                                     InvoiceDate = _journal.PublishDate,
                                     JournalId = _journal.Id,
                                     CustomerId = customerId,
                                     Orders = invoiceNum.ToString()
                                 });

            //Total
//            var balance = customer.RecalBalance();
            dict.Add("PriceTotal", Utils.DecimalToString(totalPrice));

            var ds = new DataSet();
            ds.Tables.Add(table);
            File.WriteAllBytes(billWordPath, TRIS.FormFill.Lib.FormFiller.GetWordReport(billTemplatePath, ds, dict));

            //Print:
            try
            {
                Utils.PdfPrint(billWordPath, billPath, billPdfFileName);

                if (Core.Configure.Settings.Instance.AutoPrint)
                {
                    dynamic app = new Microsoft.Office.Interop.Word.Application
                    {
                        Visible = false
                    };

                    app.ActivePrinter = Core.Configure.Settings.Instance.ActivePrinter;
                    app.ActiveDocument.PrintOut();

                    app.Quit();
                }
            }
            catch (Exception exception)
            {
                _logger.ErrorException(exception.Message, exception);
            }

            //Enjoy! ;)
        }

        private void PrepareCreateBill()
        {
            var pathJournalsDir = Core.Configure.Settings.Instance.Paths.JournalsPath;
            var pathJournalDir = string.Format("{0}{1}", pathJournalsDir, 
                                               string.Format("{0}.{1}", JournalEntity.PublishDate.Month,
                                                             JournalEntity.PublishDate.Year));

            if (!Directory.Exists(pathJournalsDir))
                Directory.CreateDirectory(pathJournalsDir);

            if (!Directory.Exists(pathJournalDir))
                Directory.CreateDirectory(pathJournalDir);
        }

        private void CheckFilteredContent()
        {
            if(!string.IsNullOrEmpty(FilterText) || (SelectedFilter != FilterEnum.None))
            {
                FilteredTextVisibility = Visibility.Visible;
            }
            else
            {
                FilteredTextVisibility = Visibility.Hidden;
            }
        }

        /// <summary>
        /// Make a payment
        /// </summary>
        public ICommand MakePaymentCommand{get{return new RelayCommand(MakePaymentCommandExecute);}}
        private void MakePaymentCommandExecute()
        {
            if (!SelectedOrder.IsSetInvoce)
            {
                MessageBox.Show(Properties.Resources.RequiredBill, Properties.Resources.Error, MessageBoxButton.OK,
                                MessageBoxImage.Error);
                return;
            }

            var dialog = new InputDialog(string.Format("{0} № {1}", Properties.Resources.Order, SelectedOrder.RealOrderId),
                                         Properties.Resources.LabelInputBalanceDialog);
            dialog.ShowDialog();

            if (dialog.Value <= 0)
                return;

            _paymentService.Add(new Payments
                                    {
                                        CustomerId = SelectedOrder.CustomerId,
                                        Amount = dialog.Value,
                                        PayDate = JournalEntity.PublishDate
                                    });

            if (!_paymentService.SaveChanges()) return;

            SelectedOrder.Amount = dialog.Value;
            SelectedOrder.IsPayable = true;

            _journalService.SaveChanges();
        }

        public ICommand OpenFolderCommand{get{return new RelayCommand(OpenFolderCommandExecute);}}
        private void OpenFolderCommandExecute()
        {
            var billPath = string.Format("{0}{1}", Core.Configure.Settings.Instance.Paths.JournalsPath,
                                        string.Format("{0}.{1}", JournalEntity.PublishDate.Month, JournalEntity.PublishDate.Year));

            if (Directory.Exists(billPath))
                System.Diagnostics.Process.Start(billPath);
        }

        #endregion

        #region Properties

        public Dispatcher DispatcherInstance { get; set; }

        #endregion

        #region Properties Binding

        /// <summary>
        /// Order collection
        /// </summary>
        private ListCollectionView _ordersView;
        public ListCollectionView Orders
        {
            get { return _ordersView; }
            set
            {
                _ordersView = value;
                RaisePropertyChanged("Orders");
            }
        }

        /// <summary>
        /// Selected order
        /// </summary>
        private JournalOrders _selectedOrder;
        public JournalOrders SelectedOrder
        {
            get { return _selectedOrder; }
            set
            {
                _selectedOrder = value;
                RaisePropertyChanged("SelectedOrder");
            }
        }

        /// <summary>
        /// Journal
        /// </summary>
        private Journals _journal;
        public Journals JournalEntity
        {
            get { return _journal; }
            set
            {
                _journal = value;
                RaisePropertyChanged("JournalEntity");
            }
        }

        /// <summary>
        /// Filter
        /// </summary>
        private string _filterText;
        public string FilterText
        {
            get { return _filterText; }
            set
            {
                _filterText = value;

                if (string.IsNullOrEmpty(value))
                    Orders.Filter = null;
                else
                    Orders.Filter = Filtering;


                CheckFilteredContent();
                RaisePropertyChanged("FilterText");
            }
        }

        /// <summary>
        /// Stores error message, after call CustomValidation method
        /// </summary>
        private string _errorMessage;
        public string ErrorMessage
        {
            get { return _errorMessage; }
            set
            {
                _errorMessage = value;
                RaisePropertyChanged("ErrorMessage");
            }
        }

        /// <summary>
        /// State this view
        /// </summary>
        private State _stateEntity;
        public State StateEntity
        {
            get { return _stateEntity; }
            set
            {
                _stateEntity = value;
                RaisePropertyChanged("StateEntity");
            }
        }

        /// <summary>
        /// Filter by
        /// </summary>
        private FilterEnum _selectedFilter = FilterEnum.None;
        public FilterEnum SelectedFilter
        {
            get { return _selectedFilter; }
            set
            {
                _selectedFilter = value;
                Orders.Filter = Filtering;
                CheckFilteredContent();

                RaisePropertyChanged("SelectedFilter");
            }
        }

        /// <summary>
        /// Filtered text
        /// </summary>
        private Visibility _filteredTextVisibility = Visibility.Hidden;
        public Visibility FilteredTextVisibility
        {
            get { return _filteredTextVisibility; }
            set
            {
                _filteredTextVisibility = value;
                RaisePropertyChanged("FilteredTextVisibility");
            }
        }

        /// <summary>
        /// Activate filter case sensitive
        /// </summary>
        private bool _isCaseSensitive;
        public bool IsCaseSensitive
        {
            get { return _isCaseSensitive; }
            set
            {
                _isCaseSensitive = value;
                Orders.Filter = Filtering;
                RaisePropertyChanged("IsCaseSensitive");
                
            }
        }

        /// <summary>
        /// Busy indicator
        /// </summary>
        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                RaisePropertyChanged("IsBusy");
            }
        }

        #endregion
    }
}
