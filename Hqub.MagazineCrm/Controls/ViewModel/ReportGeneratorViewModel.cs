﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight.Command;
using Hqub.MagazineCrm.Core;
using Hqub.MagazineCrm.Model;

namespace Hqub.MagazineCrm.Controls.ViewModel
{
    public class ReportGeneratorViewModel : GalaSoft.MvvmLight.ViewModelBase
    {
        public ReportGeneratorViewModel()
        {
            Reports = new ObservableCollection<ReportEntity>(InitReportCollection());
        }

        #region Fields

        private readonly Service.Interface.IJournalService _journalService = NinjectBootstrap.GetJournalService(true,
                                                                                                                false);
        #endregion

        #region Method

        private List<ReportEntity> InitReportCollection()
        {
            var collection = new List<ReportEntity>
                                 {
                                     new ReportEntity("OldOrdersReport", Properties.Resources.CompletedOrders,
                                                      GenerateReportOnCompleatedOrders,
                                                      Properties.Resources.CompletedOrdersNotes),
                                     new ReportEntity("NewOrdersReport", Properties.Resources.NewOrders,
                                                      GenerateReportOnNewOrders, Properties.Resources.NewOrdersNotes),
                                     new ReportEntity("CurrentOrdersReport", Properties.Resources.CurrentOrders,
                                                      GenerateReportOnCurrentOrders,
                                                      Properties.Resources.CurrentOrdersNotes),
                                     new ReportEntity("UnpaidOrdersReport", Properties.Resources.UnpaidOrders,
                                                      GenerateReportOnUnpaidOrders,
                                                      Properties.Resources.UnpaidOrdersNote),
                                     new ReportEntity("PaidOrdersReport", Properties.Resources.PaidOrders,
                                                      GenerateReportPaidOrders, Properties.Resources.PaidOrdersNote)
                                 };

            return collection;
        }

        private void GenerateReportOnCompleatedOrders()
        {
            var tab = new Reports.OrderReportsView(_journalService.GetClosedOrders(DateTime.Now));

            GuiManager.AddPage(ReportTabControl, SelectedReportEntity.Name, SelectedReportEntity.Title, tab);
        }

        private void GenerateReportOnNewOrders()
        {
            var tab = new Reports.OrderReportsView(_journalService.GetNewOrders(DateTime.Now));

            GuiManager.AddPage(ReportTabControl, SelectedReportEntity.Name, SelectedReportEntity.Title, tab);
        }

        private void GenerateReportOnCurrentOrders()
        {
            var tab = new Reports.OrderReportsView(_journalService.GetCurrentOrders());

            GuiManager.AddPage(ReportTabControl, SelectedReportEntity.Name, SelectedReportEntity.Title, tab);
        }

        private void GenerateReportOnUnpaidOrders()
        {
            var tab = new Reports.OrderReportsView(_journalService.GetUnPaidOrders());

            GuiManager.AddPage(ReportTabControl, SelectedReportEntity.Name, SelectedReportEntity.Title, tab);
        }

        private void GenerateReportPaidOrders()
        {
            var tab = new Reports.OrderReportsView(_journalService.GetPaidOrders());

            GuiManager.AddPage(ReportTabControl, SelectedReportEntity.Name, SelectedReportEntity.Title, tab);
        }

        #endregion

        #region Commands

        public ICommand OpenReportCommand
        {
            get { return new RelayCommand(OpenReportCommandExecute); }
        }

        private void OpenReportCommandExecute()
        {
            if (SelectedReportEntity == null || SelectedReportEntity.Action == null)
                return;

            IsBusy = true;
            var bg = new BackgroundWorker();
            bg.DoWork += (s, a) => Dispatcher.Invoke(DispatcherPriority.Background,new Action(() => SelectedReportEntity.Action()));
            bg.RunWorkerCompleted += (s, a) => IsBusy = false;
            bg.RunWorkerAsync();
        }

        public ICommand CloseCommand
        {
            get { return new RelayCommand(CloseCommandExecute); }
        }

        private void CloseCommandExecute()
        {
            GuiManager.RemoveActiveTab(ReportTabControl);
        }

        #endregion

        #region Properties

        public int Hash { get; set; }

        public string ReportTabControl
        {
            get { return string.Format("{0}_{1}", Constants.ReportTabControl, Hash); }
        }

        public Dispatcher Dispatcher { get; set; }
        #endregion

        #region Properties Binding

        private ObservableCollection<ReportEntity> _reports;

        public ObservableCollection<ReportEntity> Reports
        {
            get { return _reports; }
            set
            {
                _reports = value;
                RaisePropertyChanged("Reports");
            }
        }

        private ReportEntity _selectedReportEntity;

        public ReportEntity SelectedReportEntity
        {
            get { return _selectedReportEntity; }
            set
            {
                _selectedReportEntity = value;
                RaisePropertyChanged("SelectedReportEntity");
            }
        }

        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                RaisePropertyChanged("IsBusy");
            }
        }

        #endregion
    }
}
