﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;

namespace Hqub.MagazineCrm.Controls.ViewModel
{
    public class InputDialogViewModel : GalaSoft.MvvmLight.ViewModelBase, Core.UserInterface.IDialog
    {
        public InputDialogViewModel()
        {
            SelectedDate = DateTime.Now;
        }

        #region Commands

        public ICommand OkCommand { get { return new RelayCommand(OkCommandExecute); } }
        private void OkCommandExecute()
        {
            DialogResult = true;
            Close();
        }

        #endregion

        #region Properties Binding

        /// <summary>
        /// Title dialog
        /// </summary>
        private string _title;
        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                RaisePropertyChanged("Title");
            }
        }

        /// <summary>
        /// Message
        /// </summary>
        private string _label;
        public string Label
        {
            get { return _label; }
            set
            {
                _label = value;
                RaisePropertyChanged("Label");
            }
        }

        /// <summary>
        /// Value
        /// </summary>
        private decimal _value;
        public decimal Value
        {
            get { return _value; }
            set
            {
                _value = value;
                RaisePropertyChanged("Value");
            }
        }

        public decimal? ValueNullable
        {
            get { return _valueNullable; }
            set
            {
                _valueNullable = value;

                if (value != null)
                {
                    Value = (decimal)value;
                }
            }
        }

        /// <summary>
        /// Selected date
        /// </summary>
        private DateTime _selectedDate;

        private decimal? _valueNullable;

        public DateTime SelectedDate
        {
            get { return _selectedDate; }
            set
            {
                _selectedDate = value;
                RaisePropertyChanged("SelectedDate");
            }
        }

        #endregion

        public bool DialogResult { get; set; }

        public Action Close { get; set; }
    }
}
