﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;

namespace Hqub.MagazineCrm.Controls.ViewModel
{
    public class DateInputDialogViewModel : GalaSoft.MvvmLight.ViewModelBase, Core.UserInterface.IDialog
    {
        public DateInputDialogViewModel()
        {
            SelectedDate = DateTime.Now;
        }

        #region Commands

        public ICommand OkCommand{get{return new RelayCommand(OkCommandExecute);}}

        private void OkCommandExecute()
        {
            Close();
        }

        #endregion

        #region Binding Properties

        private DateTime _selectedDate;

        public DateTime SelectedDate
        {
            get { return _selectedDate; }
            set
            {
                _selectedDate = value;
                RaisePropertyChanged("SelectedDate");
            }
        }

        #endregion

        public Action Close { get; set; }
    }
}
