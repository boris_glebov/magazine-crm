﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Objects.DataClasses;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using Hqub.MagazineCrm.Core;
using Hqub.MagazineCrm.Database;
using Microsoft.Win32;

namespace Hqub.MagazineCrm.ViewModel
{
    public class AddOrderDialogViewModel : GalaSoft.MvvmLight.ViewModelBase, Core.UserInterface.IDialog
    {
        public enum State
        {
            Create,
            CreateWithoutSave,
            Modify,
            JournalOrderView
        }

        /// <summary>
        /// New
        /// </summary>
        public AddOrderDialogViewModel(Customers customer, State state = State.Create)
        {
            if (customer == null)
                throw new NullReferenceException(
                    "'customer' not be equal null.");

            CustomerEntity = customer;
            StateEntity = state;
            OrderEntity = new Orders
                              {
                                  StartMonth = DateTime.Now.Month,
                                  EndMonth = DateTime.Now.Month,
                                  StartYear = DateTime.Now.Year,
                                  EndYear = DateTime.Now.Year,
                                  Notes = string.Empty,
                                  IsAutomatic = false
                              };

            OrderEntity.PropertyChanged += (sender, e) =>
                                               {
                                                   if (e.PropertyName == "Price")
                                                       RecalcPriceBrutto();
                                               };
        }

        /// <summary>
        /// Clone
        /// </summary>
        public AddOrderDialogViewModel(Customers customer, Orders order, State state = State.CreateWithoutSave)
        {
            if (customer == null)
                throw new NullReferenceException(
                    "'customer' not be equal null.");

            CustomerEntity = customer;
            StateEntity = state;
            OrderEntity = OrderClone(order);

        }

        /// <summary>
        /// Modify
        /// </summary>
        public AddOrderDialogViewModel(Orders order)
        {
            CustomerEntity = order.Customer;
            OrderEntity = order;

            StateEntity = State.Modify;

            CloneOrderEntity = OrderClone(OrderEntity);
        }

        /// <summary>
        /// Journal order view
        /// </summary>
        public AddOrderDialogViewModel(JournalOrders order)
        {
            AcceptButtonVisibility = Visibility.Hidden;
            StateEntity = State.JournalOrderView;
            OrderEntity = ConvertJournalOrderToOrder(order);
        }

        #region Fields

        private readonly List<Layouts> _newAttachments = new List<Layouts>();
        private readonly Service.Interface.IPaymentService _paymentService = NinjectBootstrap.GetPaymentService();
        #endregion

        #region Methods

        private Orders OrderClone(Orders order)
        {
            var clone = new Orders {
//                Id = order.Id,
                Notes = order.Notes,
                Amount = order.Amount,
                CustomerId = order.CustomerId,
                Price = order.Price,
                IsPayable = order.IsPayable,
                IsSetInvoce = order.IsSetInvoce,
                IsPrepay = order.IsPrepay,
                Period = order.Period,
                Size = order.Size,
                Description = order.Description,
                Prepay =  order.Prepay,
                PriceBrutto = order.PriceBrutto,
                StartMonth = order.StartMonth,
                StartYear = order.StartYear,
                EndMonth = order.EndMonth,
                EndYear = order.EndYear,
                ExtraNotes = order.ExtraNotes,
                IsAutomatic = order.IsAutomatic,
                Layouts = new EntityCollection<Layouts>()
            };

            return clone;
        }

        private Orders ConvertJournalOrderToOrder(JournalOrders journalOrder)
        {
            var clone = new Orders
                            {
                                Id = journalOrder.Id,
                                Notes = journalOrder.Notes,
                                Amount = journalOrder.Amount,
                                CustomerId = journalOrder.CustomerId,
                                Price = journalOrder.Price,
                                IsPayable = journalOrder.IsPayable,
                                IsSetInvoce = journalOrder.IsSetInvoce,
                                IsPrepay = journalOrder.IsPrepay,
                                Period = journalOrder.Period,
                                Size = journalOrder.Size,
                                Description = journalOrder.Description,
                                Prepay = journalOrder.Prepay,
                                PriceBrutto = journalOrder.PriceBrutto,
                                StartMonth = journalOrder.StartMonth,
                                StartYear = journalOrder.StartYear,
                                EndMonth = journalOrder.EndMonth,
                                EndYear = journalOrder.EndYear,
                                ExtraNotes = journalOrder.ExtraNotes,
                                Layouts = new EntityCollection<Layouts>()
                            };

            foreach (var journalLayoute in journalOrder.JournalLayouts)
            {
                clone.Layouts.Add(new Layouts
                                      {
                                          Id = journalLayoute.Id,
                                          Data = journalLayoute.Data,
                                          Extension = journalLayoute.Extension,
                                          Title = journalLayoute.Title
                                      });
            }

            return clone;
        }

        private void RevertChanges(Orders order, Orders clone)
        {
//            order.Id = clone.Id;
            order.Amount = clone.Amount;
            order.CustomerId = clone.CustomerId;
            order.Price = clone.Price;
            order.IsPayable = clone.IsPayable;
            order.IsSetInvoce = clone.IsSetInvoce;
            order.IsPrepay = clone.IsPrepay;
            order.Period = clone.Period;
            order.Size = clone.Size;
            order.Description = clone.Description;
            order.Prepay = clone.Prepay;
            order.PriceBrutto = clone.PriceBrutto;
            order.StartMonth = clone.StartMonth;
            order.StartYear = clone.StartYear;
            order.EndMonth = clone.EndMonth;
            order.EndYear = clone.EndYear;
            order.Notes = clone.Notes;
            order.ExtraNotes = clone.ExtraNotes;
                 
            //Remove all added layouts:
            foreach (var layout in _newAttachments)
                order.Layouts.Remove(layout);
        }

        private void RecalcPriceBrutto()
        {
            //Take % from settings
            OrderEntity.PriceBrutto = OrderEntity.Price*Core.Configure.Settings.Instance.Tax;
        }

        #endregion

        #region Commands

        public ICommand OkCommand
        {
            get { return new RelayCommand(OkCommandExecute); }
        }

        private void OkCommandExecute()
        {
            var orderService = NinjectBootstrap.GetOrderService();

            ErrorBalonVisibility = false;

            if (!CustomValidation())
            {
                ErrorBalonVisibility = true;
                return;
            }

            switch (StateEntity)
            {
                case State.Create:
                    //Asign order with Customer
                    OrderEntity.CustomerId = CustomerEntity.Id;

                    //Save order and close dialog
                    if (orderService.Add(OrderEntity))
                    {
                        DialogResult = true;
                        Close();
                    }

                    break;

                case State.CreateWithoutSave:
                    CustomerEntity.Orders.Add(OrderEntity);
                    DialogResult = true;
                    Close();
                    break;

                case State.Modify:
                    DialogResult = true;
                    OrderEntity.IsAutomatic = false;
                    Close();
                    break;
            }
        }

        private bool CustomValidation()
        {
            if (string.IsNullOrEmpty(OrderEntity.Size))
            {
                ErrorMessage = Properties.Errors.SizeErrorValue;
                return false;
            }

            if(OrderEntity.StartYear > OrderEntity.EndYear)
            {
                ErrorMessage = Properties.Errors.StartDateError;
                return false;
            }

            if (OrderEntity.StartYear == OrderEntity.EndYear && OrderEntity.StartMonth > OrderEntity.EndMonth)
            {
                ErrorMessage = Properties.Errors.StartDateError;
                return false;
            }

            if (string.IsNullOrEmpty(OrderEntity.Description) && OrderEntity.Layouts.Count == 0)
            {
                ErrorMessage = Properties.Errors.NotesOrAttachRequiredValidate;
                return false;
            }

            if(OrderEntity.Price == 0 || OrderEntity.PriceBrutto == 0)
            {
                ErrorMessage = Properties.Errors.PriceNotBeNull;
                return false;
            }

            if (SelectedPeriod == null)
            {
                ErrorMessage = Properties.Errors.PeriodRequiredToFill;
                return false;
            }

            return true;
        }

        public ICommand CancelCommnad
        {
            get { return new RelayCommand(Close); }
        }

        public ICommand AddAttachmentCommand
        {
            get { return new RelayCommand(AddAttachmentCommandExecute); }
        }

        private void AddAttachmentCommandExecute()
        {
            var dialog = new OpenFileDialog
                             {
                                 Filter =
                                     "Image Files (*.BMP;*.JPG;*.JPEG;*.GIF;*.PNG)|*.BMP;*.JPG;*.JPEG;*.GIF;*.PNG|PDF Files (*.pdf)|*.pdf|Office Files (*.doc;*.docx)|*.doc;*.docx|All Files (*.*)|*.*",
                                 Multiselect = true
                             };

            var userClickedOk = dialog.ShowDialog();
            if (userClickedOk == true)
            {
                foreach (var fileName in dialog.FileNames)
                {
                    if (!File.Exists(fileName))
                        continue;

                    var fileInfo = new FileInfo(fileName);
                    var layout = new Layouts
                                     {
                                         Title = fileInfo.Name,
                                         Data = Utils.StreamFile(fileName),
                                         Extension = fileInfo.Extension,
                                     };

                    //save ref layout:
                    _newAttachments.Add(layout);
                    OrderEntity.Layouts.Add(layout);
                }
            }
        }

        public ICommand PrepayCommand { get { return new RelayCommand(PrepayCommandExecute); } }
        private void PrepayCommandExecute()
        {
            var inputDialog = new Controls.InputDialog("Prepay", Properties.Resources.Prepay);
            inputDialog.ShowDialog();

            if(!inputDialog.Result || inputDialog.Value == 0)
                return;

            OrderEntity.Prepay += inputDialog.Value;


            CustomerEntity.Payments.Add(new Payments
                                            {
                                                Amount = inputDialog.Value,
                                                PayDate = DateTime.Now
                                            });
        }

        public ICommand CalcPriceBruttoCommand { get { return new RelayCommand(CalcPriceBruttoCommandExecute); } }
        private void CalcPriceBruttoCommandExecute()
        {
            RecalcPriceBrutto();
        }

        public ICommand WindowClosingCommand
        {
            get { return new RelayCommand<CancelEventArgs>(WindowClosingCommandExecute); }
        }

        private void WindowClosingCommandExecute(CancelEventArgs e)
        {
            switch (StateEntity)
            {
                case State.Modify:
                    if (DialogResult != true)
                        RevertChanges(OrderEntity, CloneOrderEntity);
                    break;
            }
        }

        #endregion

        #region Properties

        #region Clone

        public Orders CloneOrderEntity { get; set; }

        private List<Layouts> CloneLayouts { get; set; } 

        #endregion

        public bool? DialogResult { get; set; }
        #endregion

        #region Binding Properties

        private Customers _customerEntity;

        public Customers CustomerEntity
        {
            get { return _customerEntity; }
            set
            {
                _customerEntity = value;
                RaisePropertyChanged("CustomerEntity");
            }
        }

        private Orders _orderEntity;
        public Orders OrderEntity
        {
            get { return _orderEntity; }
            set
            {
                _orderEntity = value;
                if (value != null)
                    SelectedPeriod =
                        Model.OrderPeriodCollection.Instance.FirstOrDefault(
                            x => x.PeriodEnum == (PeriodEnum) value.Period);
                RaisePropertyChanged("OrderEntity");
            }
        }

        private bool _errorBalonVisibility;

        public bool ErrorBalonVisibility
        {
            get { return _errorBalonVisibility; }
            set
            {
                _errorBalonVisibility = value;
                RaisePropertyChanged("ErrorBalonVisibility");
            }
        }

        private string _errorMessage;

        public string ErrorMessage
        {
            get { return _errorMessage; }
            set
            {
                _errorMessage = value;

                RaisePropertyChanged("ErrorMessage");
            }
        }

        private string _title;

        public string Title
        {
            get
            {
                switch (StateEntity)
                {
                    case State.Create:
                    case State.CreateWithoutSave:
                        _title = Properties.Resources.OrderNewTitle;
                        break;
                    case State.Modify:
                        _title = string.Format("{0} № {1}", Properties.Resources.Order, OrderEntity.Id);
                        break;
                }

                return _title;
            }
        }

        private Model.OrderPeriod _selectedPeriod;

        public Model.OrderPeriod SelectedPeriod
        {
            get { return _selectedPeriod; }
            set
            {
                _selectedPeriod = value;

                if (value != null)
                    OrderEntity.Period = (int) value.PeriodEnum;

                RaisePropertyChanged("SelectedPeriod");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private State _stateEntity;
        public State StateEntity
        {
            get { return _stateEntity; }
            set
            {
                _stateEntity = value;
                RaisePropertyChanged("StateEntity");
            }
        }

        /// <summary>
        /// Hide or Show Accept button
        /// </summary>
        private Visibility _acceptButtonVisibility = Visibility.Visible;
        public Visibility AcceptButtonVisibility
        {
            get { return _acceptButtonVisibility; }
            set
            {
                _acceptButtonVisibility = value;
                RaisePropertyChanged("AcceptButtonVisibility");
            }
        }

        #endregion

        private Action _close;
        public Action Close
        {
            get
            {
                if (_close == null)
                    throw new NullReferenceException("Метод 'Close' не опредлен.");
                return _close;
            }

            set { _close = value; }
        }
    }
}
