﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight.Command;
using Hqub.MagazineCrm.Core;
using Hqub.MagazineCrm.Database;
using Infralution.Localization.Wpf;
using Microsoft.Win32;

namespace Hqub.MagazineCrm.ViewModel
{
    public class MainWindowViewModel : GalaSoft.MvvmLight.ViewModelBase
    {
        private Service.Interface.ICustomerService _customerService;
        private readonly Service.Interface.IBillService _billService = NinjectBootstrap.GetBillService();

        private readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        public MainWindowViewModel()
        {
            _customerService = NinjectBootstrap.GetCustomerService();
            NotificationsVisibility = Visibility.Collapsed;
            ClientCollectionVisibility = Visibility.Visible;

            var dateTimeFormat = new DateTimeFormatInfo();
            
            //Setting Localization
            var changedCultureUI = new CultureInfo(Core.Configure.Settings.Instance.Language)
                                       {
                                           NumberFormat = CultureInfo.CurrentCulture.NumberFormat,
                                           DateTimeFormat = CultureInfo.CurrentCulture.DateTimeFormat
                                       };

            Thread.CurrentThread.CurrentCulture = changedCultureUI;
            CultureManager.UICulture = changedCultureUI;
            CultureManager.UICultureChanged += CultureManagerUiCultureChanged;
            SelectedLanguage = CultureManager.UICulture.TwoLetterISOLanguageName.ToLower();
        
            LoadCustomerCollection();
            CheckUnpaidBills();

            GlobalFunction.SetBusyIndicator = val=>IsBusy = val;
        }

        #region Methods

        /// <summary>
        /// Load clients from database
        /// </summary>
        private void LoadCustomerCollection()
        {
            _customerService = NinjectBootstrap.GetCustomerService(true);
            //Load customers form DB:
            _clientCollection = new List<Customers>(_customerService.List());
            _clientCollection.Sort(SortCustomers);
            ClientCollection = new ListCollectionView(_clientCollection);

            ClientCollection.Filter = CustomerFiltering;
        }


        /*
         Sort customer. Comparsion<T,T>()
        */
        private int SortCustomers(Customers x, Customers y)
        {
            return -(x.Orders.Count - y.Orders.Count);
        }

        /// <summary>
        /// Event handle  UICultureChanged (object CultureMangaer)
        /// </summary>
        private void CultureManagerUiCultureChanged(object sender, EventArgs e)
        {
            UpdateLanguageMenus();
        }

        /// <summary>
        /// Set current Culture as SelectedLanguage
        /// </summary>
        private void UpdateLanguageMenus()
        {
            SelectedLanguage = CultureManager.UICulture.TwoLetterISOLanguageName.ToLower();
        }

        private void LoadJournalsFromDatabase(Service.Interface.IJournalService service)
        {
            _journalCollection = new List<Journals>(service.List());
            JournalCollectionView = new ListCollectionView(_journalCollection);
            if(JournalCollectionView.GroupDescriptions != null)
                JournalCollectionView.GroupDescriptions.Add(new PropertyGroupDescription("PublishDate.Year"));
        }

        private bool JournalFiltering(object de)
        {
            var journal = de as Journals;

            return journal.Title.Contains(FilterText);
        }

        private bool CustomerFiltering(object de)
        {
            var customer = de as Customers;
            if (customer == null || string.IsNullOrEmpty(FilterText))
                return true;

            return IsCaseSensitive ? customer.Name.Contains(FilterText) : customer.Name.ToLower().Contains(FilterText.ToLower());
        }

        private void CheckUnpaidBills()
        {
            _notificationCollection = new List<Notifications>();
            var bills = _billService.GetBills(true);
            foreach (var bill in bills)
            {
                var days = (DateTime.Now - bill.PublishDate).Days/Core.Configure.Settings.Instance.Mahnung;
                var counter = 1;
                
                while (counter <= days && counter <= 3)
                {
                    var notification = new Notifications
                                           {
                                               Number = counter,
                                               Bills = bill,
                                               Journal = bill.Journals
                                           };

                    _notificationCollection.Add(notification);
                    
                    ++counter;
                }
            }
            
            NotificationCollectionView = new ListCollectionView(_notificationCollection);
            if (NotificationCollectionView.GroupDescriptions != null)
                NotificationCollectionView.GroupDescriptions.Add(new PropertyGroupDescription("Bills.Id"));
        }

        private void GenerateMahnung()
        {

        }

        #endregion

        #region Commands

        /// <summary>
        /// Show dialog for adding new client
        /// </summary>
        public ICommand AddCustomerCommand
        {
            get { return new RelayCommand(AddCustomerCommandExecute); }
        }

        private void AddCustomerCommandExecute()
        {
            var dialog = new Views.AddUserDialogView();
            dialog.ShowDialog();

            //Update customer list
            if (dialog.Result == true)
                LoadCustomerCollection();
        }

        /// <summary>
        /// Show dialog for adding new order
        /// </summary>
        public ICommand AddOrderCommand
        {
            get { return new RelayCommand(AddOrderCommandExecute); }
        }

        private void AddOrderCommandExecute()
        {
            if (SelectedCustomerEntity == null)
            {
                MessageBox.Show(Properties.Resources.RequiredSelectClient, Properties.Resources.OrderNewTitle,
                                MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            var dialog = new Views.AddOrderDialogView(SelectedCustomerEntity);
            dialog.ShowDialog();
        }

        /// <summary>
        /// Syndicate Month by Rules
        /// </summary>
        public ICommand SyndicateMonthCommand
        {
            get { return new RelayCommand(SyndicateMonthCommandExecute); }
        }

        private void SyndicateMonthCommandExecute()
        {
            var now = DateTime.Now;

            //Проверяем случай когда журнал с такой датой еще не опубликован но уже открыт в сессии для создания:
            if (JournalManager.Instance.CheckJournal(now.Month, now.Year))
            {
                MessageBox.Show(string.Format(Properties.Errors.JournalAlreadyOpen, now.Month, now.Year));
                return;
            }

            //проверяем не опубликован ли уже, журнал с текущим месяцем и годом:
            if (!JournalManager.Instance.ExistsJournal(now.Month, now.Year))
                OpenAndRegistrJournal(now);
            else if (MessageBox.Show(
                string.Format(Properties.Errors.JournalAlreadyPublish, now.Month, now.Year),
                Properties.Resources.SyndicateMonth, MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                OpenAndRegistrJournal(now);
            }
        }

        private void OpenAndRegistrJournal(DateTime date)
        {
            var control = new Controls.MonthControlView();
            GuiManager.AddMainContent(control, Properties.Resources.NewJournal);

            //Регистрируем журнал:
            JournalManager.Instance.RegisterJournal(date.Month, date.Year);
        }

        /// <summary>
        /// Setup printer
        /// </summary>
        public ICommand SetupPrintersCommand
        {
            get { return new RelayCommand(SetupPrintersCommandExecute); }
        }

        private void SetupPrintersCommandExecute()
        {
            var dialog = new Views.PrintDialogView();
            dialog.ShowDialog();
        }

        /// <summary>
        /// Terminate App
        /// </summary>
        public ICommand WindowCloseCommand
        {
            get { return new RelayCommand(WindowCloseCommandExecute); }
        }

        private void WindowCloseCommandExecute()
        {
            Application.Current.MainWindow.Close();
        }

        /// <summary>
        /// Handle event WindowClosing
        /// </summary>
        public ICommand WindowClosingCommand
        {
            get { return new RelayCommand<CancelEventArgs>(WindowClosingCommandExecute); }
        }

        private void WindowClosingCommandExecute(CancelEventArgs e)
        {
            if (MessageBoxResult.No ==
                MessageBox.Show(Properties.Resources.AnswerExitFromApp, Properties.Resources.HeaderCompletionApplication,
                                MessageBoxButton.YesNo,
                                MessageBoxImage.Question))
                e.Cancel = true;
        }

        /// <summary>
        /// Show dialog for edit client
        /// </summary>
        public ICommand EditCustomerCommand
        {
            get { return new RelayCommand<MouseEventArgs>(EditCustomerCommandExecute); }
        }

        private void EditCustomerCommandExecute(MouseEventArgs obj)
        {
            if (SelectedCustomerEntity == null)
                return;

            var dialog = new Views.AddUserDialogView(SelectedCustomerEntity, _customerService);
            dialog.ShowDialog();
            
            if (dialog.Result == true)
                LoadCustomerCollection();
        }

        /// <summary>
        /// Open journal for editing
        /// </summary>
        public ICommand EditJournalCommand { get { return new RelayCommand<MouseEventArgs>(EditJournalCommandExecute); } }
        private void EditJournalCommandExecute(MouseEventArgs obj)
        {
            var control = new Controls.MonthControlView(SelectedJournalEntity);
            Core.GuiManager.AddMainContent(control, SelectedJournalEntity.Title);
        }

        /// <summary>
        /// Change language
        /// </summary>
        /// <param name="lang">new select</param>
        public ICommand SelectLanguageCommand
        {
            get { return new RelayCommand<string>(SelectLanguageCommandExecute); }
        }

        private void SelectLanguageCommandExecute(string lang)
        {
            var settingCultureUI = new CultureInfo(lang);
            settingCultureUI.NumberFormat = CultureInfo.CurrentCulture.NumberFormat;
            settingCultureUI.DateTimeFormat = CultureInfo.CurrentCulture.DateTimeFormat;

            CultureManager.UICulture = settingCultureUI;

            Core.Configure.Settings.Instance.Language = lang;
            Core.Configure.Settings.ConfigSave();
        }

        /// <summary>
        /// Close or show notification panel
        /// </summary>
        public ICommand ShowNotificationsCommand
        {
            get { return new RelayCommand(ShowNotificationsCommandExecute); }
        }

        private void ShowNotificationsCommandExecute()
        {
            if(NotificationsVisibility == Visibility.Collapsed || NotificationsVisibility == Visibility.Hidden)
            {
                CheckUnpaidBills();
                NotificationsVisibility = Visibility.Visible;
                return;
            }

            NotificationsVisibility = Visibility.Collapsed;
        }

        /// <summary>
        /// Handle event MouseLeftButtonDown. Close notification panel
        /// </summary>
        public ICommand CloseNotificationsCommand
        {
            get { return new RelayCommand<MouseEventArgs>(CloseNotificationsCommandExecute); }
        }
        private void CloseNotificationsCommandExecute(MouseEventArgs obj)
        {
            ShowNotificationsCommandExecute();
        }

        /// <summary>
        /// Show customer list
        /// </summary>
        public ICommand ShowClientCollection { get { return new RelayCommand(ShowClientCollectionExecute); } }
        private void ShowClientCollectionExecute()
        {
            ClientCollectionVisibility = Visibility.Visible;
            ClearFilterCommandExecute();
        }

        /// <summary>
        /// Show journal list
        /// </summary>
        public ICommand ShowJournalCollection {get {return new RelayCommand(ShowJournalCollectionExecute);}}
        private void ShowJournalCollectionExecute()
        {
            var service = NinjectBootstrap.GetJournalService(GuiManager.IsEmpty());
            LoadJournalsFromDatabase(service);
            ClearFilterCommandExecute();

            JournalCollectionVisibility = Visibility.Visible;
        }

        /// <summary>
        /// Update journal collection
        /// </summary>
        public ICommand UpdateJournalCollectionCommand
        {
            get { return new RelayCommand(UpdateJournalCollectionCommandExecute); }
        }

        private void UpdateJournalCollectionCommandExecute()
        {
            var service = NinjectBootstrap.GetJournalService(true);
            LoadJournalsFromDatabase(service);
        }

        public ICommand FilterTextKeyDownCommand
        {
            get { return new RelayCommand<KeyEventArgs>(FilterTextKeyDownCommandExecute); }
        }

        private void FilterTextKeyDownCommandExecute(KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                FilterText = string.Empty;
            }
        }

        public ICommand FilterTextChangedCommand { get { return new RelayCommand<TextChangedEventArgs>(FilterTextChangedCommandExecute); } }
        private void FilterTextChangedCommandExecute(TextChangedEventArgs args)
        {
            FilterText = ((TextBox)args.Source).Text;
        }

        /// <summary>
        /// Open dialog with orders
        /// </summary>
        public ICommand ShowOrderCollectionCommand { get { return new RelayCommand(ShowOrderCollectionCommandExecute); } }
        private void ShowOrderCollectionCommandExecute()
        {
            var dialog = new Views.OrdersDialogView();
            dialog.ShowDialog();
        }

        /// <summary>
        /// Reports
        /// </summary>
        public ICommand OpenReportGeneratorCommand { get { return new RelayCommand(OpenReportGeneratorCommandExecute); } }
        private void OpenReportGeneratorCommandExecute()
        {
            var control = new Controls.ReportGeneratorView();
            Core.GuiManager.AddMainContent(control, Properties.Resources.Reports);
        }

        /// <summary>
        /// Import data in Database
        /// </summary>
        public ICommand ImportCommand {get {return new RelayCommand(ImportCommandExecute);}}
        private void ImportCommandExecute()
        {
            try
            {
                var dialog = new Views.ImportView();
                dialog.ShowDialog();

                //Update customer list
                LoadCustomerCollection();
            }
            catch (Exception exception)
            {
                _logger.ErrorException(exception.Message, exception);
            }
        }

        public ICommand MahnungPrintCommand{get{return new RelayCommand<Notifications>(MahnungPrintCommandExecute);}}
        private void MahnungPrintCommandExecute(Notifications obj)
        {
            var journalService = NinjectBootstrap.GetJournalService(false, false);

            var dict = new Dictionary<string, string>
                           {
                               {"NumberUnpaidBills", obj.Bills.Orders.Split('-').Length.ToString()},
                               {"UnpaidBillList", obj.Bills.Orders},
                               {"RequiredPaymentDate", Utils.GetStringDate(DateTime.Now.AddDays(10))},
                               {"CurrentDate", Utils.GetStringDate(DateTime.Now)},
                               {"Company", obj.Bills.Customers.Name},
                               {"Street", obj.Bills.Customers.Street},
                               {"House", obj.Bills.Customers.House},
                               {"Zip", obj.Bills.Customers.Zip},
                               {"City", obj.Bills.Customers.City}
                           };

            var balance = obj.Bills.Customers.RecalBalance();
            decimal debt;

            //If balance positive< close mahnung
            if (balance >= 0)
            {
                debt = 0;
                if (
                    MessageBox.Show(Properties.Resources.BalancePositiveCloseMahnungQuestion,
                                    Properties.Resources.Warning, MessageBoxButton.YesNo, MessageBoxImage.Question) ==
                    MessageBoxResult.Yes)
                {
                    obj.Bills.IsPayable = true;
                    _billService.SaveChanges();
                    CheckUnpaidBills();
                    return;
                }
            }
            else
            {
                debt = balance;
            }

            dict.Add("TotalAmountOwing", debt.ToString());

            var tableOrders = new DataTable("Orders");
            tableOrders.Columns.Add("RechnungNr"); //zu Rechnung Nr
            tableOrders.Columns.Add("Number");
            tableOrders.Columns.Add("InvoiceDate");

            var tableTotal = new DataTable("Total");
            tableTotal.Columns.Add("GrossAmount");
            tableTotal.Columns.Add("InvoiceDate");

            decimal totalPriceBrutto = 0;

            foreach (var orderId in obj.Bills.Orders.Split('-'))
            {
                if (string.IsNullOrEmpty(orderId))
                    continue;

                var order = journalService.GetJournalOrder(long.Parse(orderId));
                if (order == null)
                    continue;

                //Calc total price brutto
                totalPriceBrutto += order.PriceBrutto;

                var newOrderRow = tableOrders.NewRow();
                newOrderRow["RechnungNr"] = "zu Rechnung Nr";
                newOrderRow["Number"] = orderId;

                var newTotalRow = tableTotal.NewRow();
                newTotalRow["GrossAmount"] = order.PriceBrutto;

                if (order.InvoiceDate != null)
                {
                    newOrderRow["InvoiceDate"] =
                        newTotalRow["InvoiceDate"] = Utils.GetStringDate((DateTime) order.InvoiceDate);
                }

                tableOrders.Rows.Add(newOrderRow);
                tableTotal.Rows.Add(newTotalRow);
            }

            var ds = new DataSet();
            ds.Tables.Add(tableOrders);
            ds.Tables.Add(tableTotal);

            dict.Add("TotalPriceBrutto", totalPriceBrutto.ToString());

            var billTemplatePath = AppDomain.CurrentDomain.BaseDirectory + "Templates\\Mahnung_GmbH.docx";

            SaveAndOpenMahnung(obj.Bills.JournalId, obj.Bills.Customers.Name, TRIS.FormFill.Lib.FormFiller.GetWordReport(billTemplatePath, ds, dict));
        }

        private void SaveAndOpenMahnung(long journalId, string customerName, byte[] file)
        {
            var mahnungDir = AppDomain.CurrentDomain.BaseDirectory + "Mahnungs";
            var mahnungPath = string.Format("{0}\\Mahnung_Journal_{1}_{2}.docx", mahnungDir, journalId, customerName);
            if (!Directory.Exists(mahnungDir))
                Directory.CreateDirectory(mahnungDir);

            File.WriteAllBytes(mahnungPath, file);

            System.Diagnostics.Process.Start(mahnungPath);
        }

        public ICommand MahnungExplorerDirCommand
        {
            get { return new RelayCommand<Notifications>(MahnungExplorerDirCommandExecute); }
        }

        private void MahnungExplorerDirCommandExecute(Notifications obj)
        {
            var mahnungDir = AppDomain.CurrentDomain.BaseDirectory + "Mahnungs";

            if (!Directory.Exists(mahnungDir))
                Directory.CreateDirectory(mahnungDir);

            System.Diagnostics.Process.Start("explorer.exe", mahnungDir);
        }

        public ICommand MarkAsPaidCommand
        {
            get { return new RelayCommand<Notifications>(MarkAsPaidCommandExecute); }
        }

        private void MarkAsPaidCommandExecute(Notifications notification)
        {
            var paymentService = NinjectBootstrap.GetPaymentService();
            var paymentInputDialog = new Controls.InputDialog(Properties.Resources.TitleInputBalanceDialog,
                                                              Properties.Resources.LabelInputBalanceDialog);

            paymentInputDialog.ShowDialog();
            if (paymentInputDialog.Value <= 0)
                return;

            //Change customer balance:
            paymentService.Add(new Payments
                                   {
                                       CustomerId = notification.Bills.CustomerId,
                                       Amount = paymentInputDialog.Value,
                                       PayDate = paymentInputDialog.SelectDate,
                                   });

            notification.Bills.IsPayable = true;

            //Update journal orders
            var journalService = NinjectBootstrap.GetJournalService(false, false);
            var orders = journalService.GetJournalOrders(notification.Bills.CustomerId, notification.Bills.JournalId);
            foreach (var order in orders)
                order.IsPayable = true;

            if (journalService.SaveChanges() && _billService.SaveChanges() && paymentService.SaveChanges())
                CheckUnpaidBills();

        }

        public ICommand ClearFilterCommand
        {
            get { return new RelayCommand(ClearFilterCommandExecute); }
        }

        private void ClearFilterCommandExecute()
        {
            FilterText = string.Empty;
            IsCaseSensitive = false;
        }

        public ICommand CloseActiveTabCommand
        {
            get { return new RelayCommand(CloseActiveTabCommandExecute); }
        }

        private void CloseActiveTabCommandExecute()
        {
            Core.GuiManager.RemoveActiveTab();
        }

        #endregion

        #region Properties

        

        #endregion

        #region Binding Properties

        /// <summary>
        /// Clients
        /// </summary>
        private List<Customers> _clientCollection;

        private ListCollectionView _clientCollectionView;
        public ListCollectionView ClientCollection
        {
            get
            {
                return _clientCollectionView;
            }
            set
            {
                _clientCollectionView = value;
                RaisePropertyChanged("ClientCollection");
            }
        }

        /// <summary>
        /// Journals
        /// </summary>
        private List<Journals> _journalCollection;
        private ListCollectionView _journalCollectionView;
        public ListCollectionView JournalCollectionView
        {
            get { return _journalCollectionView; }
            set
            {
                _journalCollectionView = value;
                RaisePropertyChanged("JournalCollectionView");
            }
        }

        /// <summary>
        /// Selected Journal
        /// </summary>
        private Journals _selectedJournalEntity;
        public Journals SelectedJournalEntity
        {
            get { return _selectedJournalEntity; }
            set
            {
                _selectedJournalEntity = value;
                RaisePropertyChanged("JournalEntity");
            }
        }

        /// <summary>
        /// Selected client
        /// </summary>
        private Customers _selectedCustomerEntity;
        public Customers SelectedCustomerEntity
        {
            get { return _selectedCustomerEntity; }
            set
            {
                _selectedCustomerEntity = value;
                RaisePropertyChanged("SelectedCustomerEntity");
            }
        }

        /// <summary>
        /// Current language (en, de or ru)
        /// </summary>
        private string _selectedLanguage;

        public string SelectedLanguage
        {
            get { return _selectedLanguage; }
            set
            {
                _selectedLanguage = value;
                RaisePropertyChanged("SelectedLanguage");
            }
        }

        /// <summary>
        /// Is visibly notification panel
        /// </summary>
        private Visibility _notificationsVisibility;

        public Visibility NotificationsVisibility
        {
            get { return _notificationsVisibility; }
            set
            {
                _notificationsVisibility = value;
                QueryRowLength = value == Visibility.Visible
                                     ? new GridLength(200, GridUnitType.Pixel)
                                     : new GridLength(0, GridUnitType.Auto);

                RaisePropertyChanged("NotificationsVisibility");
            }
        }

        /// <summary>
        /// Notification panel WIDTH
        /// </summary>
        private GridLength _queryRowLenght = new GridLength(200, GridUnitType.Auto);

        public GridLength QueryRowLength
        {
            get { return _queryRowLenght; }
            set
            {
                _queryRowLenght = value;
                RaisePropertyChanged("QueryRowLength");
            }
        }

        /// <summary>
        /// Show or collapse customer list
        /// </summary>
        private Visibility _clientCollectionVisibility;
        public Visibility ClientCollectionVisibility
        {
            get { return _clientCollectionVisibility; }
            set
            {
                _clientCollectionVisibility = value;
                if (value == Visibility.Visible)
                {
                    JournalCollectionVisibility = Visibility.Collapsed;
                }
                    

                RaisePropertyChanged("ClientCollectionVisibility");
            }
        }

        /// <summary>
        /// Show or collapse journal
        /// </summary>
        private Visibility _journalCollectionVisibility;
        public Visibility JournalCollectionVisibility
        {
            get { return _journalCollectionVisibility; }
            set
            {
                _journalCollectionVisibility = value;
                if(value == Visibility.Visible)
                {
                    ClientCollectionVisibility = Visibility.Collapsed;
                    SelectedCustomerEntity = null;
                }
                    

                RaisePropertyChanged("JournalCollectionVisibility");
            }
        }

        /// <summary>
        /// Filter
        /// </summary>
        private string _filterText;
        public string FilterText
        {
            get { return _filterText; }
            set
            {
                _filterText = value;
                Core.GlobalFunction.InvokeFiltering(_filterText, IsCaseSensitive);

                if (string.IsNullOrEmpty(value))
                {
                    if (JournalCollectionVisibility == Visibility.Visible)
                        JournalCollectionView.Filter = null;
                    else
                        ClientCollection.Filter = null;
                }
                else if (ActiveTab == null || !(((TabItem)ActiveTab).Content.GetType() == typeof(Controls.MonthControlView)))
                    if (JournalCollectionVisibility == Visibility.Visible)
                        JournalCollectionView.Filter = JournalFiltering;
                    else
                        ClientCollection.Filter = CustomerFiltering;

                RaisePropertyChanged("FilterText");
            }
        }

        private List<Notifications> _notificationCollection;

        private ListCollectionView _notificationCollectionView;
        public ListCollectionView NotificationCollectionView
        {
            get { return _notificationCollectionView; }
            set
            {
                _notificationCollectionView = value;
                RaisePropertyChanged("NotificationCollectionView");
            }
        }

        /// <summary>
        /// Activate filter case sensitive
        /// </summary>
        private bool _isCaseSensitive;
        public bool IsCaseSensitive
        {
            get { return _isCaseSensitive; }
            set
            {
                _isCaseSensitive = value;
                FilterText = FilterText;
                RaisePropertyChanged("IsCaseSensitive");

            }
        }

        /// <summary>
        /// Busy indicator
        /// </summary>
        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                RaisePropertyChanged("IsBusy");
            }
        }

        private object _activeTab;
        public object ActiveTab
        {
            get { return _activeTab; }
            set
            {
                _activeTab = value;
                FilterText = string.Empty;
                RaisePropertyChanged("ActiveTab");
            }
        }

        #endregion
    }
}
