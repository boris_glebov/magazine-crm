﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Linq;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using Hqub.MagazineCrm.Database;

namespace Hqub.MagazineCrm.ViewModel
{
    public class AddUserDialogViewModel : GalaSoft.MvvmLight.ViewModelBase, Core.UserInterface.IDialog
    {
        public enum State
        {
            Create,
            Modify
        }

        private readonly Service.Interface.ICustomerService _customerService;
        private readonly Service.Interface.IHistoryService _historyService;

        public AddUserDialogViewModel()
        {
            //Resolve services
            _customerService = NinjectBootstrap.GetCustomerService();
            _historyService = NinjectBootstrap.GetHistoryService();

            _customerEntity = new Customers();
            StateView = State.Create;
            
            CalcBalanceCommandExecute();
        }

        public AddUserDialogViewModel(Customers customerEntity, Service.Interface.ICustomerService customerService)
        {
            //Resolve services
            _customerService = customerService;
            _historyService = NinjectBootstrap.GetHistoryService();

            _customerEntity = customerEntity;
            StateView = State.Modify;

            CalcBalanceCommandExecute();

            //Clone
            _customerClone = Core.SerializeHelper.Serialize(CustomerEntity).ToArray();

            //Subscribe on PropertyChanged for Serialize and save to history
            CustomerEntity.PropertyChanged += (sender, args) => _customerChangedFlag = true;

            LoadHistory();

            UpdatePayments();
        }

        #region Fields

        private bool _customerChangedFlag;
        private Binary _customerClone;

        #endregion

        #region Methods

        private void LoadHistory()
        {
            var serviceHistory = NinjectBootstrap.GetHistoryService();
            HistoryCustomerCollection = new ObservableCollection<History>(serviceHistory.CustomerCollection(CustomerEntity.Id));
        }

        private void UpdatePayments(bool sortByUnpaid=false)
        {
            if (sortByUnpaid)
            {
                var d = new Dictionary<long, decimal>();
                foreach (var payment in CustomerEntity.Payments)
                {
                    if(d.ContainsKey(payment.OrderId))
                    {
                        d[payment.OrderId] += payment.Amount;
                        continue;
                    }

                    d.Add(payment.OrderId, payment.Amount);
                }

                PaymentsCollectionView = new ListCollectionView(CustomerEntity.Payments
                    .Where(p => d.ContainsKey(p.OrderId) && d[p.OrderId] < 0)
                    .ToList());

                return;
            }

            PaymentsCollectionView = new ListCollectionView(CustomerEntity.Payments.ToList());
        }

        #endregion

        #region Properties

        public bool? DialogResult { get; set; }

        private Action _close;

        public Action Close
        {
            get
            {
                if (_close == null)
                    throw new NullReferenceException("Method 'Close' not defined.");

                return _close;
            }
            set
            {
                _close = value;
                RaisePropertyChanged("Close");
            }
        }

        #endregion

        #region Command

        /// <summary>
        /// Ok button command
        /// </summary>
        public ICommand OkCommand
        {
            get { return new RelayCommand(OkCommandExecute); }
        }

        private void OkCommandExecute()
        {
            ErrorBalonVisibility = false;

            if (CustomerEntity.IsError)
            {
                ErrorBalonVisibility = true;
                return;
            }

            switch (StateView)
            {
                case State.Create:

                    if(_customerService.Exists(CustomerEntity.Name))
                    {
                        MessageBox.Show(string.Format(Properties.Resources.CustomerExists, CustomerEntity.Name));
                        return;
                    }

                    _customerService.Add(CustomerEntity);

                    break;

                case State.Modify:
                    if (_customerChangedFlag)
                        _historyService.AddCustomer(CustomerEntity.Id, _customerClone);

                    _customerService.SaveChanges();
                    break;
            }

            DialogResult = true;
            Close();
        }

        /// <summary>
        /// Cancel button command
        /// </summary>
        public ICommand CancelCommand
        {
            get { return new RelayCommand(Close); }
        }

        /// <summary>
        /// Add order button command
        /// </summary>
        public ICommand AddOrderCommand
        {
            get { return new RelayCommand(AddOrderCommandExecute); }
        }

        private void AddOrderCommandExecute()
        {
            var dialog = new Views.AddOrderDialogView(CustomerEntity, AddOrderDialogViewModel.State.CreateWithoutSave);
            dialog.ShowDialog();
        }

        /// <summary>
        /// Edit order button command
        /// </summary>
        public ICommand EditOrderCommand
        {
            get { return new RelayCommand(EditOrderCommandExecute); }
        }

        private void EditOrderCommandExecute()
        {
            if (SelectedOrder == null)
            {
                MessageBox.Show(Properties.Resources.OrderNotSelected, Properties.Resources.Error, MessageBoxButton.OK,
                                MessageBoxImage.Error);

                return;
            }

            var dialog = new Views.AddOrderDialogView(SelectedOrder);
            dialog.ShowDialog();
        }

        /// <summary>
        /// Copy order button command
        /// </summary>
        public ICommand CopyOrderCommand
        {
            get { return new RelayCommand(CopyOrderCommandExecute); }
        }

        private void CopyOrderCommandExecute()
        {
            if (SelectedOrder == null)
            {
                MessageBox.Show(Properties.Resources.OrderNotSelected, Properties.Resources.Error, MessageBoxButton.OK,
                                MessageBoxImage.Error);

                return;
            }

            var dialog = new Views.AddOrderDialogView(CustomerEntity, SelectedOrder);
            dialog.ShowDialog();
        }

        /// <summary>
        /// Handle event WindowClosing
        /// </summary>
        public ICommand WindowClosingCommand
        {
            get { return new RelayCommand<CancelEventArgs>(WindowClosingCommandExecute); }
        }

        private void WindowClosingCommandExecute(CancelEventArgs e)
        {
            if (DialogResult == true) return;

            //Remove all added orders
            foreach (var order in CustomerEntity.Orders.Where(x => x.Id == 0).ToList())
            {
                CustomerEntity.Orders.Remove(order);
            }
        }

        /// <summary>
        /// Recalc balance
        /// </summary>
        public ICommand CalcBalanceCommand
        {
            get { return new RelayCommand(CalcBalanceCommandExecute); }
        }

        private void CalcBalanceCommandExecute()
        {
            Balance = CustomerEntity.Payments.Select(p => p.Amount).Sum();
        }

        /// <summary>
        /// Analog prepay
        /// </summary>
        public ICommand AddMoneyToAccountCommand { get { return new RelayCommand(AddMoneyToAccountExecute); } }
        private void AddMoneyToAccountExecute()
        {
            var inputDialog = new Controls.InputDialog("Pay", Properties.Resources.CashIn);
            inputDialog.ShowDialog();

            if(!inputDialog.Result)
            {
                return;
            }

            if (inputDialog.Value == 0)
                return;

            CustomerEntity.Payments.Add(new Payments
            {
                Amount = inputDialog.Value,
                PayDate = DateTime.Now
            });

            //Recalc
            CalcBalanceCommandExecute();
        }

        #endregion

        #region Binding Properties

        private Customers _customerEntity;
        public Customers CustomerEntity
        {
            get { return _customerEntity; }
            set
            {
                _customerEntity = value;
                RaisePropertyChanged("CustomerEntity");
            }
        }

        private Orders _selectedOrder;
        public Orders SelectedOrder
        {
            get { return _selectedOrder; }
            set
            {
                _selectedOrder = value;
                RaisePropertyChanged("SelectedOrder");
            }
        }

        private State _stateEntity;
        public State StateView
        {
            get { return _stateEntity; }
            set
            {
                _stateEntity = value;
                RaisePropertyChanged("StateView");
            }
        }

        private ListCollectionView _paymentsCollectionView;

        public ListCollectionView PaymentsCollectionView
        {
            get { return _paymentsCollectionView; }
            set
            {
                _paymentsCollectionView = value;

                RaisePropertyChanged("PaymentsCollectionView");
            }
        }

        private bool _errorBalonVisibility;

        public bool ErrorBalonVisibility
        {
            get { return _errorBalonVisibility; }
            set
            {
                _errorBalonVisibility = value;
                RaisePropertyChanged("ErrorBalonVisibility");
            }
        }

        private bool _isShowOnlyUnpaidOrders;
        public bool IsShowOnlyUnpaidOrders
        {
            get { return _isShowOnlyUnpaidOrders; }
            set
            {
                _isShowOnlyUnpaidOrders = value;
                UpdatePayments(value);
                RaisePropertyChanged("IsShowOnlyUnpaidOrders");
            }
        }

        /// <summary>
        /// Current client balance
        /// </summary>
        private decimal _balance;

        public decimal Balance
        {
            get { return _balance; }
            set
            {
                _balance = value;
                RaisePropertyChanged("Balance");
            }
        }

        private ObservableCollection<History> _historyCustomerCollection;

        public ObservableCollection<History> HistoryCustomerCollection
        {
            get { return _historyCustomerCollection; }
            set
            {
                _historyCustomerCollection = value;
                RaisePropertyChanged("HistoryCustomerCollection");
            }
        }

        #endregion
    }
}
