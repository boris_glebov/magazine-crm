﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using Hqub.MagazineCrm.Model;
using Microsoft.Win32;

namespace Hqub.MagazineCrm.ViewModel
{
    public class ImportDialogViewModel : GalaSoft.MvvmLight.ViewModelBase, Core.UserInterface.IDialog
    {
        #region Fields

        private readonly Action<int> _setMaxEntityValue;
        private readonly Action<ImportCustomerResultEntity> _addImportCustomerResultEntity;
        private readonly Action<ImportOrderResultEntity> _addImportOrderResultEntity;

        private readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        #endregion

        public ImportDialogViewModel()
        {
            _addImportCustomerResultEntity =
                entity =>
                Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
                                  new Action(() =>
                                                 {
                                                     _importCustomerResultEntityCollection.Add(entity);
                                                     ImportResultEntityCollection.Refresh();
                                                     ScrollToLastEntity(entity);
                                                 }));

            _addImportOrderResultEntity =
                entity => Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
                                            new Action(() =>
                                                           {
                                                               _importOrderResultEntityCollection.Add(entity);
                                                               ImportResultEntityCollection.Refresh();
                                                               ScrollToLastEntity(entity);
                                                           }));

            _setMaxEntityValue = value => MaxEntityCount = value;
        }

        #region Methods

        private void Import(string fileName, Action<int> setMaxEntityValue)
        {
            using (ManagedExcel.Application application = ManagedExcel.ExcelApplication.Create())
            {
                application.Visible = false;

                using (ManagedExcel.Workbook workbook = application.Workbooks.Open(fileName))
                {
                    using (ManagedExcel.Worksheets worksheets = workbook.Sheets)
                    {
                        //Import Customers
                        using (var worksheet = worksheets[1])
                        {
                            _importCustomerResultEntityCollection = new List<ImportCustomerResultEntity>();
                            SetView(_importCustomerResultEntityCollection);
                            OperationName = Properties.Resources.CustomerImport;

                            ImportCustomers(worksheet, setMaxEntityValue, _addImportCustomerResultEntity);
                        }

                        //Import Orders
                        using(var worksheet = worksheets[2])
                        {
                            _importOrderResultEntityCollection = new List<ImportOrderResultEntity>();
                            SetView(_importOrderResultEntityCollection);
                            OperationName = Properties.Resources.OrderImport;

                            ImportOrders(worksheet, setMaxEntityValue, _addImportOrderResultEntity);
                        }
                    }
                }

                application.Quit();
            }

            IsRunning = false;
            OperationName = Properties.Resources.ImportFinished;
        }

        private void ImportCustomers(ManagedExcel.Worksheet worksheet, Action<int> setMaxEntityValue,
                            Action<ImportCustomerResultEntity> importResultEntity)
        {
            using (var cells = worksheet.Cells)
            {
                int rowsCount;
                using (var usedRange = worksheet.UsedRange)
                {
                    var value = (object[,])usedRange.Value2;
                    rowsCount = (value).GetUpperBound(0);
                    setMaxEntityValue(rowsCount);
                    System.Diagnostics.Debug.WriteLine(string.Format("!!!! COUNT ROWS = {0}", rowsCount));
                }

                for (var i = 2; i <= rowsCount; ++i)
                {
                    if (!IsRunning)
                        break;

                    var client = new Database.Customers
                                     {
                                         PaymentMadeByDirectDebit = false
                                     };

                    using (var num = cells[i, Core.Import.CustomerColumns.Num])
                    {
                        client.Id = long.Parse(num.Text);
                    }

                    using (var customer = cells[i, Core.Import.CustomerColumns.Kunde])
                    {
                        //If Name is empty, so row reserve
                        if(string.IsNullOrEmpty(customer.Text))
                            continue;

                        client.Name = customer.Text;
                    }

                    using (var street = cells[i, Core.Import.CustomerColumns.Adresse])
                    {
                        try
                        {
                            var address = street.Text;
                            client.House = new string(address.Where(char.IsDigit).ToArray());
                            client.Street = address.Replace(client.House, string.Empty);
                        }catch(Exception)
                        {
                            client.Street = street.Text;
                        }

                    }

                    using (var zip = cells[i, Core.Import.CustomerColumns.PlzOrt])
                    {
                        const string zipcityPattern = @"^(?<zip>([^d]{1,2}-?)?\d{4,5})?\s*(?<city>[^d,]*)(,\s*(?<country>.*))?$";

                        try
                        {
                            var reg = new Regex(zipcityPattern);
                            var zipText = zip.Text;
                            var match = reg.Match(zipText);

                            client.Zip = match.Groups["zip"].Value;
                            client.Country = match.Groups["country"].Value;
                            if (string.IsNullOrEmpty(client.Country))
                                client.Country = "Deutschland";

                            client.City = match.Groups["city"].Value;
                        }
                        catch (Exception ex)
                        {
                            client.Zip = zip.Text;
                            client.Country = client.City = string.Empty;
                        }
                    }

                    using (var phone = cells[i, Core.Import.CustomerColumns.Telefon])
                    {
                        client.Phone = phone.Text;
                    }

                    using (var fax = cells[i, Core.Import.CustomerColumns.Fax])
                    {
                        client.Fax = fax.Text;
                    }

                    using (var mobile = cells[i, Core.Import.CustomerColumns.Handy])
                    {
                        client.Cell = mobile.Text;
                    }

                    using (var contact = cells[i, Core.Import.CustomerColumns.Ansprechpartner])
                    {
                        client.Contact = contact.Text;
                    }

                    using (var notes = cells[i, Core.Import.CustomerColumns.Bemerkungen])
                    {
                        client.Note = notes.Text;
                    }

                    using (var email = cells[i, Core.Import.CustomerColumns.Email])
                    {
                        client.Email = email.Text;
                    }

                    using (var site = cells[i, Core.Import.CustomerColumns.Internetseite])
                    {
                        client.HomePage = site.Text;
                    }

                    var result = new ImportCustomerResultEntity(client.Id, client.Name, "Ok");
                    try
                    {
//                        var customerService = NinjectBootstrap.GetCustomerService(true);
                        var databaseService = NinjectBootstrap.GetDatabaseService();

                        if (!databaseService.InsertCustomer(client))
                            result.Status = "Failed";

                        ++ProceedEntityCount;
                        if (importResultEntity != null)
                            importResultEntity(result);
                    }
                    catch (Exception exception)
                    {
                        result.Status = "Failed";
                        if (importResultEntity != null)
                            importResultEntity(result);

                        _logger.ErrorException(exception.Message, exception);
                        if (exception.InnerException != null)
                            _logger.ErrorException(exception.InnerException.Message,
                                                   exception.InnerException);
                    }
                }
            }
        }

        private void ImportOrders(ManagedExcel.Worksheet worksheet, Action<int> setMaxEntityValue,
                            Action<ImportOrderResultEntity> importResultEntity)
        {
            using (var cells = worksheet.Cells)
            {
                int rowsCount;
                using (var usedRange = worksheet.UsedRange)
                {
                    var value = (object[,])usedRange.Value2;
                    rowsCount = (value).GetUpperBound(0);
                    setMaxEntityValue(rowsCount);
                }

                for (var i = 2; i <= rowsCount; ++i)
                {
                    if (!IsRunning)
                        break;

                    var order = new Database.Orders
                                    {
                                        Size = string.Empty,
                                        IsAutomatic = true
                                    };

                    var orderResultEntity = new ImportOrderResultEntity();

                    using (var cellNum = cells[i, Core.Import.OrderColumns.Num])
                    {
                        var num = cellNum.Text.Trim();
                        if(string.IsNullOrEmpty(num))
                            continue;
                        
                        order.Id = long.Parse(num);
                        orderResultEntity.Num = order.Id;
                    }

                    using(var cellCustomerId = cells[i, Core.Import.OrderColumns.KundeNum])
                    {
                        long customerId;
                        long.TryParse(cellCustomerId.Text, out customerId);

                        if(customerId == 0)
                            continue;

                        order.CustomerId = customerId;
                    }

                    using(var customerName = cells[i, Core.Import.OrderColumns.Kunde])
                    {
                        orderResultEntity.Customer = customerName.Text;
                    }

                    using (var description = cells[i, Core.Import.OrderColumns.Beschreibung])
                    {
                        order.Description = description.Text;
                        orderResultEntity.Description = order.Description;
                    }
           
                    using(var priceNetto = cells[i, Core.Import.OrderColumns.EinnahmeNetto])
                    {
                       order.Price = Core.Utils.NumberCultureParse(priceNetto.Text);
                    }

                    using(var priceBrutto = cells[i, Core.Import.OrderColumns.EinnahmeBrutto])
                    {
                        order.PriceBrutto = Core.Utils.NumberCultureParse(priceBrutto.Text);
                    }

                    try
                    {
                        using (var cellStartMonth = cells[i, Core.Import.OrderColumns.AbWerbungMonat])
                        {
                            int startMonth;
                            int.TryParse(cellStartMonth.Text, out startMonth);

                            order.StartMonth = startMonth;

                            using (var cellStartYear = cells[i, Core.Import.OrderColumns.AbJahr])
                            {
                                int startYear;
                                int.TryParse(cellStartYear.Text, out startYear);
                                order.StartYear = startYear + 2000;
                            }
                        }

                        using(var cellEndMonth = cells[i, Core.Import.OrderColumns.BisWerbungMonat])
                        {
                            int endMonth;
                            int.TryParse(cellEndMonth.Text, out endMonth);

                            order.EndMonth = endMonth;

                            using (var cellEndYear = cells[i, Core.Import.OrderColumns.BisJahr])
                            {
                                int endYear;
                                order.EndYear = !int.TryParse(cellEndYear.Text, out endYear) ? order.StartYear : endYear;
                            }
                        }
                    }catch(FormatException formatException)
                    {
                        _logger.ErrorException(
                            string.Format("[FormatException] OrderId = {0}.Parse start and end dates.", order.Id), formatException);
                        continue;
                    }

                    using(var notes = cells[i, Core.Import.OrderColumns.Bemerkungen])
                    {
                        order.Notes = notes.Text;
                    }

                    DateTime? publishDate = null;
                    DateTime? payableDate = null;
                    DateTime? publishJournalDate = null;

                    using(var cellPublishDate = cells[i, Core.Import.OrderColumns.DatumRechnung])
                    {
                        if (!string.IsNullOrEmpty(cellPublishDate.Text.Trim()))
                        {
                            DateTime tmpDate;
                            if (DateTime.TryParse(cellPublishDate.Text.Trim(), out tmpDate))
                                publishDate = tmpDate;
                        }
                    }

                    using (var cellPayableDate = cells[i, Core.Import.OrderColumns.DatumZahlung])
                    {
                        if (!string.IsNullOrEmpty(cellPayableDate.Text.Trim()))
                        {
                            DateTime tmpDate;
                            if (DateTime.TryParse(cellPayableDate.Text.Trim(), out tmpDate))
                                payableDate = tmpDate;
                        }
                    }

                    using (var cellMonat = cells[i, Core.Import.OrderColumns.Monat])
                    {
                        var textMonat = cellMonat.Text.Trim();
                        if (!string.IsNullOrEmpty(textMonat))
                        {
                            const char separate = '/';
                            try
                            {
                                var m = int.Parse(textMonat.Split(separate)[0]);
                                var y = int.Parse(textMonat.Split(separate)[1]) + 2000;

                                publishJournalDate = new DateTime(y, m, 1);
                            }
                            catch (FormatException exception)
                            {
                                _logger.ErrorException(
                                    string.Format("[FormatException] OrderId = {0}. Parse start and end dates.", order.Id),
                                    exception);
                                continue;
                            }
                        }
                    }

                    try
                    {
                        var databaseService = NinjectBootstrap.GetDatabaseService();
                        orderResultEntity.Status = "Ok";

                        if (!databaseService.InsertOrder(order))
                            orderResultEntity.Status = "Failed";
                        else if (publishJournalDate != null)
                            databaseService.InsertJournalOrder(order, (DateTime)publishJournalDate, payableDate);
                        
                        ++ProceedEntityCount;
                        if (importResultEntity != null)
                            importResultEntity(orderResultEntity);
                    }
                    catch (Exception exception)
                    {
                        orderResultEntity.Status = "Failed";
                        if (importResultEntity != null)
                            importResultEntity(orderResultEntity);

                        _logger.ErrorException(exception.Message, exception);
                        if (exception.InnerException != null)
                            _logger.ErrorException(exception.InnerException.Message,
                                                   exception.InnerException);
                    }
                }
            }
        }

        private bool Filtering(object de)
        {
            var importResultEntity = de as ImportCustomerResultEntity;

            return importResultEntity != null && importResultEntity.Name.Contains(FilterText);
        }
        
        private void ClearDataBase()
        {
            var databaseService = NinjectBootstrap.GetDatabaseService();
            databaseService.Clear();
        }

        private void ClearImportResultViewCollection()
        {
            _importCustomerResultEntityCollection.Clear();
            ImportResultEntityCollection.Refresh();
        }

        private void ScrollToLastEntity(object entity)
        {
            if(MainGrid == null)
                return;

            MainGrid.UpdateLayout();
            MainGrid.ScrollIntoView(entity);
        }

        #endregion

        #region Commands

        /// <summary>
        /// Start load data
        /// </summary>
        public ICommand RunCommand
        {
            get { return new RelayCommand(RunCommandExecute); }
        }

        private void RunCommandExecute()
        {
            if (
                MessageBox.Show(Properties.Resources.AllDataDestroyQuestion, Properties.Resources.Warning, MessageBoxButton.YesNo,
                                MessageBoxImage.Warning) == MessageBoxResult.No)
                return;

            if(string.IsNullOrEmpty(FileName))
            {
                MessageBox.Show(Properties.Resources.SelectExcelDataFile, Properties.Resources.Error, MessageBoxButton.OK,
                                MessageBoxImage.Error);
                return;
            }

            IsRunning = true;

            ClearDataBase();

            var bw = new BackgroundWorker();
            bw.DoWork += (sender, args) => Import(FileName, _setMaxEntityValue);
            bw.RunWorkerCompleted +=
                (sender, args) =>
                MessageBox.Show(Properties.Resources.ImportFinished, Properties.Resources.OperationComplete, MessageBoxButton.OK, MessageBoxImage.Information);
            bw.RunWorkerAsync();
        }

        /// <summary>
        /// Open file with data
        /// </summary>
        public ICommand OpenFileCommand
        {
            get { return new RelayCommand(OpenFileCommandExecute); }
        }

        private void OpenFileCommandExecute()
        {
            var selectXlsDialog = new OpenFileDialog
                                      {
                                          Filter = "Office Files (*.xls)|*.xls",
                                          Multiselect = false
                                      };

            if (selectXlsDialog.ShowDialog() != true)
                return;

            FileName = selectXlsDialog.FileName;
        }

        public ICommand CloseCommand
        {
            get { return new RelayCommand(Close); }
        }

        public ICommand WindowClosingCommand
        {
            get { return new RelayCommand<CancelEventArgs>(WindowClosingCommandExecute); }
        }

        private void WindowClosingCommandExecute(CancelEventArgs e)
        {
            if (IsRunning && MessageBox.Show(Properties.Resources.AbortImportQuestion, Properties.Resources.Warning, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
            {
                e.Cancel = true;
                return;
            }

            IsRunning = false;
        }

        public ICommand FilterTextKeyDownCommand
        {
            get { return new RelayCommand<KeyEventArgs>(FilterTextKeyDownExecute); }
        }

        private void FilterTextKeyDownExecute(KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                FilterText = string.Empty;
            }
        }

        public ICommand FilterTextChangedCommand { get { return new RelayCommand<TextChangedEventArgs>(FilterTextChangedCommandExecute); } }
        private void FilterTextChangedCommandExecute(TextChangedEventArgs args)
        {
            FilterText = ((TextBox)args.Source).Text;
        }

        #endregion

        #region Binding Properties

        /// <summary>
        /// When press run, set True.
        /// </summary>
        private bool _isRunning;

        public bool IsRunning
        {
            get { return _isRunning; }
            set
            {
                _isRunning = value;
                RaisePropertyChanged("IsRunning");
            }
        }

        /// <summary>
        /// Data file 
        /// </summary>
        private string _fileName;
        public string FileName
        {
            get { return _fileName; }
            set
            {
                _fileName = value;
                RaisePropertyChanged("FileName");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private int _proceedEntityCount;
        public int ProceedEntityCount
        {
            get { return _proceedEntityCount; }
            set
            {
                _proceedEntityCount = value;
                RaisePropertyChanged("ProceedEntityCount");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private int _maxEntityCount;
        public int MaxEntityCount
        {
            get { return _maxEntityCount; }
            set
            {
                _maxEntityCount = value;
                RaisePropertyChanged("MaxEntityCount");
            }
        }

        private string _filterText;
        public string FilterText
        {
            get { return _filterText; }
            set
            {
                _filterText = value;

                if (string.IsNullOrEmpty(value))
                    ImportResultEntityCollection.Filter = null;
                else
                    ImportResultEntityCollection.Filter = Filtering;

                RaisePropertyChanged("FilterText");
            }
        }

        private string _operationName;
        public string OperationName
        {
            get { return _operationName; }
            set
            {
                _operationName = value;
                RaisePropertyChanged("OperationName");
            }
        }

        private void SetView(IList collection)
        {
            ImportResultEntityCollection = new ListCollectionView(collection);
            ImportResultEntityCollection.Refresh();
        }

        /// <summary>
        /// 
        /// </summary>
        private List<ImportCustomerResultEntity> _importCustomerResultEntityCollection;
        private List<ImportOrderResultEntity> _importOrderResultEntityCollection;

        private ListCollectionView _importResultEntityCollectionView;
        public ListCollectionView ImportResultEntityCollection
        {
            get { return _importResultEntityCollectionView; }
            set
            {
                _importResultEntityCollectionView = value;
                RaisePropertyChanged("ImportResultEntityCollection");
            }
        }

        #endregion

        #region Properties

        public DataGrid MainGrid { get; set; }
        public Action Close { get; set; }
        public System.Windows.Threading.Dispatcher Dispatcher { get; set; }

        #endregion
    }
}
