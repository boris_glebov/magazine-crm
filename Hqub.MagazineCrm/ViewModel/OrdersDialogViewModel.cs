﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using Hqub.MagazineCrm.Database;

namespace Hqub.MagazineCrm.ViewModel
{
    public class OrdersDialogViewModel : GalaSoft.MvvmLight.ViewModelBase, Core.UserInterface.IDialog
    {
        public OrdersDialogViewModel()
        {
            InitOrderCollection();
        }

        #region Fields

        private List<Orders> _orders; 

        #endregion

        #region Methods

        private void InitOrderCollection()
        {
            var service = NinjectBootstrap.GetOrderService();

            _orders = new List<Orders>(service.List());

            Orders = new ListCollectionView(_orders);

            if (Orders.GroupDescriptions != null)
                Orders.GroupDescriptions.Add(new PropertyGroupDescription("Customer"));

            foreach (var order in _orders)
            {
                order.IsChecked = false;
            }

            SelectedOrders = new List<Orders>();
        }

        private bool Filtering(object de)
        {
            var order = de as Orders;

            return (order.Description.Contains(FilterText));
        }

        public ICommand ClearFilterCommand
        {
            get { return new RelayCommand(ClearFilterCommandExecute); }
        }

        private void ClearFilterCommandExecute()
        {
            FilterText = string.Empty;
        }

        #endregion

        #region Properties

        public List<Orders> SelectedOrders { get; set; }

        public Action Close { get; set; }

        #endregion

        #region Command

        /// <summary>
        /// Ok
        /// </summary>
        public ICommand OkCommand
        {
            get { return new RelayCommand(OkCommandExecute); }
        }

        private void OkCommandExecute()
        {
            SelectedOrders.AddRange(_orders.Where(x => x.IsChecked));
            Close();
        }

        /// <summary>
        /// Close dialog
        /// </summary>
        public ICommand CancelCommand
        {
            get { return new RelayCommand(CancelCommandExecute); }
        }

        private void CancelCommandExecute()
        {
            Close();
        }

        public ICommand SearchTextKeyDownCommand
        {
            get { return new RelayCommand<KeyEventArgs>(SearchTextKeyDownExecute); }
        }

        private void SearchTextKeyDownExecute(KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                FilterText = string.Empty;
            }
        }

        public ICommand SearchTextChangedCommand
        {
            get { return new RelayCommand<TextChangedEventArgs>(SearchTextChangedCommandExecute); }
        }

        private void SearchTextChangedCommandExecute(TextChangedEventArgs args)
        {
            FilterText = ((TextBox) args.Source).Text;
        }

        /// <summary>
        /// Edit order
        /// </summary>
        public ICommand OrderEditCommand
        {
            get { return new RelayCommand(OrderEditCommandExecute); }
        }

        private void OrderEditCommandExecute()
        {
            if (SelectedOrder == null)
            {
                MessageBox.Show(Properties.Resources.OrderNotSelected, Properties.Resources.Error, MessageBoxButton.OK,
                                MessageBoxImage.Error);

                return;
            }

            var dialog = new Views.AddOrderDialogView(SelectedOrder);
            dialog.ShowDialog();
        }

        /// <summary>
        /// Handle windows closing event.
        /// </summary>
        public ICommand WindowClosingCommand
        {
            get { return new RelayCommand<CancelEventArgs>(WindowClosingCommandExecute); }
        }

        private void WindowClosingCommandExecute(CancelEventArgs e)
        {

        }

        /// <summary>
        /// Filter
        /// </summary>
        private string _filterText;

        public string FilterText
        {
            get { return _filterText; }
            set
            {
                _filterText = value;

                if (string.IsNullOrEmpty(value))
                    Orders.Filter = null;
                else
                    Orders.Filter = Filtering;

                RaisePropertyChanged("FilterText");
            }
        }

        /// <summary>
        /// Copy order button command
        /// </summary>
        public ICommand CopyOrderCommand { get { return new RelayCommand(CopyOrderCommandExecute); } }
        private void CopyOrderCommandExecute()
        {
            if (SelectedOrder == null)
            {
                MessageBox.Show(Properties.Resources.OrderNotSelected, Properties.Resources.Error, MessageBoxButton.OK,
                                MessageBoxImage.Error);

                return;
            }

            var dialog = new Views.AddOrderDialogView(SelectedOrder.Customer, SelectedOrder, AddOrderDialogViewModel.State.Create);
            dialog.ShowDialog();

            InitOrderCollection();
        }

        #endregion

        #region Binding Properties

        /// <summary>
        /// Order collection
        /// </summary>
        private ListCollectionView _ordersView;

        public ListCollectionView Orders
        {
            get { return _ordersView; }
            set
            {
                _ordersView = value;
                RaisePropertyChanged("Orders");
            }
        }


        private Orders _selectedOrder;
        public Orders SelectedOrder
        {
            get { return _selectedOrder; }
            set
            {
                _selectedOrder = value;
                RaisePropertyChanged("SelectedOrder");
            }
        }

        #endregion
    }
}
