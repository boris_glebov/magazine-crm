﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Hqub.MagazineCrm.Model;

namespace Hqub.MagazineCrm.ViewModel
{
    public class PrintDialogViewModel : ViewModelBase
    {
        private readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly bool _isPdfChoose;
        public PrintDialogViewModel(bool pdf=true)
        {
            _isPdfChoose = pdf;
            CreateCollection();
        }

        #region Methods

        private void CreateCollection()
        {
            PrintersCollection = new ObservableCollection<PrinterModel>();
            foreach (var installedPrinter in PrinterSettings.InstalledPrinters)
            {
                var printerName = installedPrinter.ToString();
                var printerNameFromConfig = _isPdfChoose
                                                ? Core.Configure.Settings.Instance.PdfPrinter
                                                : Core.Configure.Settings.Instance.ActivePrinter;

                var printer = new PrinterModel(printerName);

                if((string) installedPrinter == printerNameFromConfig)
                    SelectedPrinter = printer;

                PrintersCollection.Add(printer);
            }
        }

        #endregion

        #region Commands

        public ICommand OkCommand
        {
            get { return new RelayCommand(OkCommandExecute); }
        }

        private void OkCommandExecute()
        {
//            var selectPrinter = PrintersCollection.Where(x => x.IsChecked).FirstOrDefault();
            if (SelectedPrinter == null)
            {
                MessageBox.Show(Properties.Resources.RequiredChoosePrinter, Properties.Resources.Error,
                                MessageBoxButton.OK);
                return;
            }

            try
            {
                if (!_isPdfChoose)
                {
                    Core.Configure.Settings.Instance.ActivePrinter = SelectedPrinter.Name;
                    SelectedPrinter = SelectedPrinter;
                }
                else
                {
                    Core.Configure.Settings.Instance.PdfPrinter = SelectedPrinter.Name;
                }
            }
            catch (Exception exception)
            {
                _logger.ErrorException(exception.Message, exception);
            }
            finally
            {
                Core.Configure.Settings.ConfigSave();
                Close();
            }
        }

        #endregion

        #region Binding Properties

        private ObservableCollection<PrinterModel> _printersCollection;
        public ObservableCollection<PrinterModel> PrintersCollection
        {
            get { return _printersCollection; }
            set
            {
                _printersCollection = value;
                RaisePropertyChanged("PrintersCollection");
            }
        }

        private PrinterModel _selectedPrinter;
        public PrinterModel SelectedPrinter
        {
            get { return _selectedPrinter; }
            set
            {
                _selectedPrinter = value;
                RaisePropertyChanged("SelectedPrinter");
            }
        }

        #endregion

        #region Properties

        public Action Close { get; set; }

        #endregion
    }
}
