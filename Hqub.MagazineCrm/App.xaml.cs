﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Markup;

namespace Hqub.MagazineCrm
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        public App()
        {
            Current.Exit += (sender, exitArgs) =>
                                {
                                    var pathTemp = AppDomain.CurrentDomain.BaseDirectory + "temp";
                                    if (!System.IO.Directory.Exists(pathTemp))
                                        return;

                                    foreach (var file in System.IO.Directory.GetFiles(pathTemp))
                                    {
                                        try
                                        {
                                            System.IO.File.Delete(file);
                                        }
                                        catch (Exception ex)
                                        {
                                            _logger.ErrorException(
                                                string.Format("Error remove file '{0}'", file), ex);
                                        }
                                    }
                                };

            Current.DispatcherUnhandledException += (s, e) =>
                                                        {
                                                            _logger.ErrorException(e.Exception.Message, e.Exception);
                                                            var ex = e.Exception;

                                                            _logger.ErrorException(string.Format("Stack trace: {0}", e.Exception.StackTrace), e.Exception);

                                                            if (ex.InnerException != null)
                                                            {
                                                                _logger.ErrorException(
                                                                    string.Format("Inner exception: {0}", ex.InnerException.Message),
                                                                    ex.InnerException);

                                                                _logger.ErrorException(
                                                                    string.Format("Stack trace: {0}", ex.InnerException.StackTrace),
                                                                    ex.InnerException);
                                                            }
                                                        };
        }
    }
}
